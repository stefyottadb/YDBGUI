#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

cp /YDBGUI/certs/ydbgui.pem /repl/backups/melbourne.ydbgui.pem

$ydb_dist/mupip set -replication=on -region "*"
sleep 1

date=`date +%Y%m%d:%H:%M:%S`
mupip replicate -source -start -instsecondary=paris -secondary=paris:3001 -log=/tmp/paris"$date".log
mupip replicate -source -start -instsecondary=santiago -secondary=santiago:3002 -log=/tmp/santiago"$date".log
mupip replicate -source -start -instsecondary=rome -secondary=rome:3003 -log=/tmp/rome"$date".log
mupip replicate -source -start -instsecondary=amsterdam -secondary=amsterdam:3004 -log=/tmp/amsterdam"$date".log
mupip replicate -source -start -instsecondary=london -secondary=london:3005 -log=/tmp/london"$date".log
mupip replicate -source -start -instsecondary=tokio -secondary=tokio:3006 -log=/tmp/tokio"$date".log
mupip replicate -source -start -instsecondary=madrid -secondary=madrid:3007 -log=/tmp/madrid"$date".log
sleep 1
echo '*********************'
echo 'Source checkhealth'
echo '*********************'
$ydb_dist/mupip replicate -source -checkhealth

echo '*********************'
echo 'Paris log'
echo '*********************'
tail -30 /tmp/paris"$date".log

echo '*********************'
echo 'Santiago log'
echo '*********************'
tail -30 /tmp/santiago"$date".log

echo '*********************'
echo 'Rome log'
echo '*********************'
tail -30 /tmp/rome"$date".log

echo '*********************'
echo 'Amsterdam log'
echo '*********************'
tail -30 /tmp/amsterdam"$date".log

echo '*********************'
echo 'London log'
echo '*********************'
tail -30 /tmp/london"$date".log

echo '*********************'
echo 'Tokio log'
echo '*********************'
tail -30 /tmp/tokio"$date".log

echo '*********************'
echo 'Madrid log'
echo '*********************'
tail -30 /tmp/madrid"$date".log

