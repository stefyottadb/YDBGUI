<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## <a href="#" onclick="app.link('replication/modify-topology')">Modifying, saving and recalling the Topology interactive chart</a>

## Toolbars

---

There are two toolbars at the top of the Topology and they are used to:

The <a href="#" onclick="app.link('replication/toolbars/toolbar-design')">Design toolbar</a>, used to modify the current layout:

<img src="md/replication/toolbars/img/toolbar-design.png" width="900"> 

The <a href="#" onclick="app.link('replication/toolbars/toolbar-run')">Run toolbar</a>, used to save, load the layout and control the refresh:

<img src="md/replication/toolbars/img/toolbar-run.png" width="900"> 

You can toggle at any time between the two toolbars by clicking on icon on the left inside:

<img src="md/replication/toolbars/img/toolbar-design-toggle.png" width="600"> 

<img src="md/replication/toolbars/img/toolbar-run-toggle.png" width="600"> 

<br>

> By opening the Topology, the Design toolbar will be selected by default.

> If you save your Topology with the parameter Auto-load set, by opening the Topology it will automatically select the Run toolbar.
