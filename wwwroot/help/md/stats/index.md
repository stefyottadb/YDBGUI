<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# Statistics

---

### <a href="#" onclick="app.link('stats/demo')">See it in action: how to run the demo report</a>

### <a href="#" onclick="app.link('stats/report')">The statistics report</a>

### <a href="#" onclick="app.link('stats/results')">Viewing and exporting the collected data</a>

### <a href="#" onclick="app.link('stats/faq')">FAQ</a>

### <a href="#" onclick="app.link('stats/technical-description')">Technical description</a>

---

### What can you see in the statistics

In the statistics section you can see global (cumulative across all processes) or individual process information about the database internal counters. Currently, there are 70 counters that are maintained by the database engine that we can inspect and run statistics against them. The full list of counters is <a href="https://docs.yottadb.net/ProgrammersGuide/commands.html#zshow" target="blank">here</a>.

For each collected sample can get a delta, an average, a min, or a max that can be visualized on a report.

---

### Do I need to change something in my system in order to collect statistics?

It depends on the Data source.

For 'PEEKBYNAME' and 'FHEAD' you do not need to change anything, if you use 'YGBLSTAT` you need to follow the instructions <a href="#" onclick="app.link('stats/data-connectors')">here</a>.

---

### Steps needed to create and run a report

In order to create a report, you need to follow these basic steps:

1. Create at least one source
1. Add at least one graph or one table to the report
1. Run the report

##### Step 1: Creating one source

The first thing to do is to tell the system WHAT to collect and from WHERE. This is what we call a `source`. You can have as many sources as you wish in a report.

You create a source in the <a href="#" onclick="app.link('stats/sources')">Sources dialog</a> by clicking the icon <img src="md/stats/img/sources-btn.png" width="32"> sources.

##### Step 2: Add at least one graph or one table to the report

In order to display something on the screen, you need to tell the system WHAT sample you want to display and WHERE.

You do that by opening the <a href="#" onclick="app.link('stats/spark-chart/index')">Report definitions dialog</a>, by clicking on the icon <img src="md/stats/img/definition-btn.png" width="32"> Report definition.

##### Step 3: Run the report

You can now run the report by clicking on the icon <img src="md/stats/img/start-btn.png" width="32"> Start.

The server will start collecting data samples and sending them back to the Web browser, where statistics gets computed and displayed on the report screen.

<img src="md/stats/img/running-report.png" width="500">

---

### Saving, loading, exporting and importing reports

Reports are stored in the local storage of the browser. Each report take about between 10 and 50 kb of data, depending on the layout.

Reports can be, additionally, exported and imported, allowing them to be shared across multiple browsers.

In order to Save / Export reports, you can click the icon <img src="md/stats/img/save-btn.png" width="32"> Save report.

In order to Load / Import reports, you can click the icon <img src="md/stats/img/load-btn.png" width="32"> Load report.

--- 

### Viewing and exporting collected data

Data collected can be viewed, filtered and exported to a csv file (options will apply).

Once you STOP the recording, the tab "Results" will be enabled <img src="md/stats/img/results-tab.png" width="150">

By selecting the Results tab, you will have the chance to view, filter and export the collected data from any data source.

---

### Toolbar and status bar

The toolbar and the status bar are explained <a href="#" onclick="app.link('stats/tools')">in this page</a>

---
