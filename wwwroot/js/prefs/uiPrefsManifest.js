/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

// DESCRIPTION
//
//
// The manifest files controls how the data is displayed in the Preferences dialog.
// The tree gets populated by entries in the "children" arrays
// while the items in the table (or the range) gets populated by the "items" arrays
// Also the root in an array, having the same syntax as the "children". Items in the root are always displayed in the tree, while
// the children need to be expanded
//
// Each node can have both "children" and "items" set, meaning that the node will be displayed with a PLUS (to be expanded), but still will display items on the right pane
//
// children description:
//
// name : the text that will be displayed in the tree
// children: an array of childrens (if any)
// items: an array of items
//
// item description
//
// field: the name of the field
// display: the text that will be displayed in the table
// descriptionField: if not omitted or empty string, it is the field name in the array "app.ui.gViewer.settings.params", the description field will be automatically pulled form there
// description: the text that will be displayed in the pop up (help text)
// path: the path relative to the app.systemSettings object
// hive: the local storage hive that contains this setting
// type:
//      numeric     displays a numeric entry
//      color       displays a color entry
//      boolean     displays a Yes / No select
//      select      display a select populated with the given items
//      range       display a range pane (this is NOT in the table)
//
//
// type: numeric
// min:     The minimum value
// max:     The maximum value
// step:    The step when scrolling
// unit:    If missed, nothing will be displayed, if present and not empty-string, text will be displayed in the column
//
// type: color
// no parameters used....
//
// type: boolean
// no parameters used....
//
// type: select
// options: an array of objects, needed to populate the <select> element.
//          The object has two fields:
//          text: The text displayed in the select
//          value The value associated with the text (that will be stored in the userSettings object

app.prefs.manifestBaseline = [
    // REST
    {
        name: 'REST',
        children: [
            {
                name: 'Timeout',
                items: [
                    {
                        field: 'short',
                        display: 'Short',
                        description: 'The response timeout of the REST short-life calls',

                        path: 'rest/timeout/short',
                        hive: 'rest',

                        type: 'numeric',
                        min: 10,
                        max: 3600,
                        step: 1,
                        unit: 'seconds',
                    },
                    {
                        field: 'long',
                        display: 'Long',
                        description: 'The response timeout of REST long-life calls',

                        path: 'rest/timeout/long',
                        hive: 'rest',

                        type: 'numeric',
                        min: 10,
                        max: 3600,
                        step: 1,
                        unit: 'seconds',
                    }
                ]
            }
        ],
        items: [
            {
                field: 'logOnConsole',
                display: 'Log on console',
                description: 'If you want to log on the browser console the request and the response',

                path: 'rest/logOnConsole',
                hive: 'rest',

                type: 'select',
                options: [
                    {text: 'Yes', value: 'yes'},
                    {text: 'No', value: 'no'},
                ]
            }
        ]
    },

    // BACKUP
    {
        name: 'Backup',
        items: [
            {
                field: 'target',
                display: 'Target path',
                description: 'The default path to be used as target',

                path: 'defaults/backup/target',
                hive: 'defaults',

                type: 'path',
            },
            {
                field: 'replTarget',
                display: 'Replication target path',
                description: 'The default path to be used as replication target',

                path: 'defaults/backup/replTarget',
                hive: 'defaults',

                type: 'path',
            },
        ]
    },

    // OCTO
    {
        name: 'Octo',
        children: [
            {
                name: 'Query',
                items: [
                    {
                        field: 'timeout',
                        display: 'Timeout',
                        description: 'The default query timeout',

                        path: 'defaults/octo/defaultTimeout',
                        hive: 'defaults',

                        type: 'numeric',
                        min: 10,
                        max: 3600,
                        step: 1,
                        unit: 'seconds',
                    },
                    {
                        field: 'limit',
                        display: 'Limit',
                        description: 'The default query LIMIT. This value will be overridden by a LIMIT in the SQL statement ',

                        path: 'defaults/octo/defaultLimit',
                        hive: 'defaults',

                        type: 'numeric',
                        min: 1,
                        max: 1000000,
                        step: 1,
                        unit: 'records',
                    },
                ]
            },
            {
                name: 'DragAndDrop',
                items: [
                    {
                        field: 'displayHelp',
                        display: 'Display help on drop',
                        description: 'If TRUE it will provide help when dropping fields, describing the entire functionality',

                        path: 'defaults/octo/displayHelpOnDrop',
                        hive: 'defaults',

                        type: 'boolean',
                    }
                ]
            },
            {
                name: 'Views',
                items: [
                    {
                        field: 'maxDefLength',
                        display: 'Length of definition in tree',
                        description: 'Determine the maximum length of the View definition text in the tree. If the definition is longer, it will cut, 3 dots will be appended and, by hovering, the entire text will be displayed',

                        path: 'defaults/octo/treeViewsDefMaxLength',
                        hive: 'defaults',

                        type: 'numeric',
                        min: 10,
                        max: 200,
                        step: 1,
                        unit: 'characters',
                    }
                ]
            }
        ]
    },

    // REGIONS
    // IMPORTANT: YOU CAN ADD ENTRIES, BUT DO NOT CHANGE THE STRUCTURE OF THE DISK SPACE ALERTS !!!!!!!!!!
    {
        name: 'Regions',
        children: [
            {
                name: 'Database file',
                children: [
                    {
                        name: 'Disk space alert',
                        children: [
                            {
                                name: 'Default',
                                children: [
                                    {
                                        name: 'Auto Extend mode',
                                        items: [
                                            {
                                                field: 'Disk space alert for Auto Extend mode',
                                                display: 'Range',
                                                description: 'The number of extensions available on the device',

                                                path: 'dashboard/autoExtend/@range',
                                                hive: 'dashboard',

                                                type: 'rangeExtension',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Manual Extend mode',
                                        items: [
                                            {
                                                field: 'Disk space alert for Manual Extend mode',
                                                display: 'Range',
                                                description: 'The percent of disk space used on the device',

                                                path: 'dashboard/manualExtend/@range',
                                                hive: 'dashboard',

                                                type: 'rangePercent',
                                            }
                                        ]
                                    },

                                ],
                            },
                            {
                                name: '[regions]',
                                children: [
                                    {
                                        name: 'Auto Extend mode',
                                        items: [
                                            {
                                                field: 'Disk space alert for Auto Extend mode',
                                                display: 'Range',
                                                description: 'The number of extensions available on the device',

                                                path: 'dashboard/regions/{region}/autoExtend',
                                                hive: 'dashboard',

                                                type: 'rangeExtension',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Manual Extend mode',
                                        items: [
                                            {
                                                field: 'Disk space alert for Manual Extend mode',
                                                display: 'Range',
                                                description: 'The percent of disk space used on the device',

                                                path: 'dashboard/regions/{region}/manualExtend',
                                                hive: 'dashboard',

                                                type: 'rangePercent',
                                            }
                                        ]
                                    },

                                ],
                            }
                        ]
                    }
                ]
            },
            {
                name: 'Journal',
                children: [],
                items: [
                    {
                        field: 'Disk space alert for Journal file',
                        display: 'Range',
                        description: 'The percent of disk space available in the current journal file before switching occurs',

                        path: 'dashboard/activeJournalRanges/@range',
                        hive: 'dashboard',

                        type: 'rangePercent',
                    }
                ]
            }
        ]
    },

    // STORAGE
    {
        name: 'Storage',
        children: [
            {
                name: 'Disk space alert',
                items: [
                    {
                        field: 'Disk space alert for File Storage',
                        display: 'Range',
                        description: 'The percent of disk space available on the device',

                        path: 'dashboard/storageRanges/@range',
                        hive: 'dashboard',

                        type: 'rangePercent',
                    }
                ]
            }
        ],
    },

    // Stats
    {
        name: 'Statistics',
        children: [
            {
                name: 'Spark chart',
                children: [
                    {
                        name: 'Report line',
                        children: [
                            {
                                name: 'Header',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'thickness',
                                                display: 'Thickness',
                                                description: 'The thickness of the border',

                                                path: 'stats/sparkLines/sections/reportLine/header/border/thickness',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 0,
                                                max: 10,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLine/header/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                name: 'Region',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'thickness',
                                                display: 'Thickness',
                                                description: 'The thickness of the border',

                                                path: 'stats/sparkLines/sections/reportLine/region/border/thickness',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 0,
                                                max: 10,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLine/region/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'The font decoration type',

                                                        path: 'stats/sparkLines/sections/reportLine/region/font/decoration/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Underline', value: 'underline'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'style',
                                                        display: 'Style',
                                                        description: 'The font decoration style',

                                                        path: 'stats/sparkLines/sections/reportLine/region/font/decoration/style',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Solid', value: 'solid'},
                                                            {text: 'Wavy', value: 'wavy'},
                                                            {text: 'Dotted', value: 'dotted'},
                                                            {text: 'Dashed', value: 'dashed'},
                                                            {text: 'Double', value: 'double'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/region/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                    {
                                                        field: 'thickness',
                                                        display: 'Thickness',
                                                        description: 'The thickness of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/region/font/decoration/thickness',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 0,
                                                        max: 10,
                                                        step: 1,
                                                        unit: 'px',
                                                    },

                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'size',
                                                display: 'Size',
                                                description: 'The size of the font',

                                                path: 'stats/sparkLines/sections/reportLine/region/font/size',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 6,
                                                max: 100,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLine/region/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'fontFamily',
                                                display: 'Family',
                                                description: 'The font family',

                                                path: 'stats/sparkLines/sections/reportLine/region/font/fontFamily',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Inconsolata', value: 'Inconsolata'},
                                                    {text: 'FiraGO', value: 'FiraGO'}
                                                ]
                                            },
                                            {
                                                field: 'weight',
                                                display: 'Weight',
                                                description: 'The font weight',

                                                path: 'stats/sparkLines/sections/reportLine/region/font/weight',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Bold', value: 'bold'}
                                                ]
                                            },
                                            {
                                                field: 'style',
                                                display: 'Style',
                                                description: 'The font style',

                                                path: 'stats/sparkLines/sections/reportLine/region/font/style',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Italic', value: 'italic'},
                                                    {text: 'Oblique', value: 'oblique'},
                                                ]
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the region',

                                        path: 'stats/sparkLines/sections/reportLine/region/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                    {
                                        field: 'align',
                                        display: 'Align',
                                        description: 'The alignment of the region',

                                        path: 'stats/sparkLines/sections/reportLine/region/align',
                                        hive: 'stats',

                                        type: 'select',
                                        options: [
                                            {text: 'Left', value: 'left'},
                                            {text: 'Center', value: 'center'},
                                            {text: 'Right', value: 'right'},
                                        ]
                                    },
                                ]
                            },
                            {
                                name: 'Process',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'thickness',
                                                display: 'Thickness',
                                                description: 'The thickness of the border',

                                                path: 'stats/sparkLines/sections/reportLine/process/border/thickness',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 0,
                                                max: 10,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLine/process/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'The font decoration type',

                                                        path: 'stats/sparkLines/sections/reportLine/process/font/decoration/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Underline', value: 'underline'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'style',
                                                        display: 'Style',
                                                        description: 'The font decoration style',

                                                        path: 'stats/sparkLines/sections/reportLine/process/font/decoration/style',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Solid', value: 'solid'},
                                                            {text: 'Wavy', value: 'wavy'},
                                                            {text: 'Dotted', value: 'dotted'},
                                                            {text: 'Dashed', value: 'dashed'},
                                                            {text: 'Double', value: 'double'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/process/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                    {
                                                        field: 'thickness',
                                                        display: 'Thickness',
                                                        description: 'The thickness of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/process/font/decoration/thickness',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 0,
                                                        max: 10,
                                                        step: 1,
                                                        unit: 'px',
                                                    },

                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'size',
                                                display: 'Size',
                                                description: 'The size of the font',

                                                path: 'stats/sparkLines/sections/reportLine/process/font/size',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 6,
                                                max: 100,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLine/process/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'fontFamily',
                                                display: 'Family',
                                                description: 'The font family',

                                                path: 'stats/sparkLines/sections/reportLine/process/font/fontFamily',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Inconsolata', value: 'Inconsolata'},
                                                    {text: 'FiraGO', value: 'FiraGO'}
                                                ]
                                            },
                                            {
                                                field: 'weight',
                                                display: 'Weight',
                                                description: 'The font weight',

                                                path: 'stats/sparkLines/sections/reportLine/process/font/weight',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Bold', value: 'bold'}
                                                ]
                                            },
                                            {
                                                field: 'style',
                                                display: 'Style',
                                                description: 'The font style',

                                                path: 'stats/sparkLines/sections/reportLine/process/font/style',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Italic', value: 'italic'},
                                                    {text: 'Oblique', value: 'oblique'},
                                                ]
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the process',

                                        path: 'stats/sparkLines/sections/reportLine/process/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                    {
                                        field: 'align',
                                        display: 'Align',
                                        description: 'The alignment of the process',

                                        path: 'stats/sparkLines/sections/reportLine/process/align',
                                        hive: 'stats',

                                        type: 'select',
                                        options: [
                                            {text: 'Left', value: 'left'},
                                            {text: 'Center', value: 'center'},
                                            {text: 'Right', value: 'right'},
                                        ]
                                    },
                                ]
                            },
                            {
                                name: 'Sample name',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'thickness',
                                                display: 'Thickness',
                                                description: 'The thickness of the border',

                                                path: 'stats/sparkLines/sections/reportLine/sample/border/thickness',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 0,
                                                max: 10,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLine/sample/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'The font decoration type',

                                                        path: 'stats/sparkLines/sections/reportLine/sample/font/decoration/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Underline', value: 'underline'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'style',
                                                        display: 'Style',
                                                        description: 'The font decoration style',

                                                        path: 'stats/sparkLines/sections/reportLine/sample/font/decoration/style',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Solid', value: 'solid'},
                                                            {text: 'Wavy', value: 'wavy'},
                                                            {text: 'Dotted', value: 'dotted'},
                                                            {text: 'Dashed', value: 'dashed'},
                                                            {text: 'Double', value: 'double'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/sample/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                    {
                                                        field: 'thickness',
                                                        display: 'Thickness',
                                                        description: 'The thickness of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/sample/font/decoration/thickness',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 0,
                                                        max: 10,
                                                        step: 1,
                                                        unit: 'px',
                                                    },

                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'size',
                                                display: 'Size',
                                                description: 'The size of the font',

                                                path: 'stats/sparkLines/sections/reportLine/sample/font/size',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 6,
                                                max: 100,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLine/sample/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'fontFamily',
                                                display: 'Family',
                                                description: 'The font family',

                                                path: 'stats/sparkLines/sections/reportLine/sample/font/fontFamily',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Inconsolata', value: 'Inconsolata'},
                                                    {text: 'FiraGO', value: 'FiraGO'}
                                                ]
                                            },
                                            {
                                                field: 'weight',
                                                display: 'Weight',
                                                description: 'The font weight',

                                                path: 'stats/sparkLines/sections/reportLine/sample/font/weight',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Bold', value: 'bold'}
                                                ]
                                            },
                                            {
                                                field: 'style',
                                                display: 'Style',
                                                description: 'The font style',

                                                path: 'stats/sparkLines/sections/reportLine/sample/font/style',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Italic', value: 'italic'},
                                                    {text: 'Oblique', value: 'oblique'},
                                                ]
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the sample name',

                                        path: 'stats/sparkLines/sections/reportLine/sample/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                    {
                                        field: 'align',
                                        display: 'Align',
                                        description: 'The alignment of the sample name',

                                        path: 'stats/sparkLines/sections/reportLine/sample/align',
                                        hive: 'stats',

                                        type: 'select',
                                        options: [
                                            {text: 'Left', value: 'left'},
                                            {text: 'Center', value: 'center'},
                                            {text: 'Right', value: 'right'},
                                        ]
                                    },
                                ]
                            },
                            {
                                name: 'Sample type',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'thickness',
                                                display: 'Thickness',
                                                description: 'The thickness of the border',

                                                path: 'stats/sparkLines/sections/reportLine/sampleMap/border/thickness',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 0,
                                                max: 10,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLine/sampleMap/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'The font decoration type',

                                                        path: 'stats/sparkLines/sections/reportLine/sampleMap/font/decoration/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Underline', value: 'underline'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'style',
                                                        display: 'Style',
                                                        description: 'The font decoration style',

                                                        path: 'stats/sparkLines/sections/reportLine/sampleMap/font/decoration/style',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Solid', value: 'solid'},
                                                            {text: 'Wavy', value: 'wavy'},
                                                            {text: 'Dotted', value: 'dotted'},
                                                            {text: 'Dashed', value: 'dashed'},
                                                            {text: 'Double', value: 'double'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/sampleMap/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                    {
                                                        field: 'thickness',
                                                        display: 'Thickness',
                                                        description: 'The thickness of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/sampleMap/font/decoration/thickness',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 0,
                                                        max: 10,
                                                        step: 1,
                                                        unit: 'px',
                                                    },

                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'size',
                                                display: 'Size',
                                                description: 'The size of the font',

                                                path: 'stats/sparkLines/sections/reportLine/sampleMap/font/size',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 6,
                                                max: 100,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLine/sampleMap/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'fontFamily',
                                                display: 'Family',
                                                description: 'The font family',

                                                path: 'stats/sparkLines/sections/reportLine/sampleMap/font/fontFamily',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Inconsolata', value: 'Inconsolata'},
                                                    {text: 'FiraGO', value: 'FiraGO'}
                                                ]
                                            },
                                            {
                                                field: 'weight',
                                                display: 'Weight',
                                                description: 'The font weight',

                                                path: 'stats/sparkLines/sections/reportLine/sampleMap/font/weight',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Bold', value: 'bold'}
                                                ]
                                            },
                                            {
                                                field: 'style',
                                                display: 'Style',
                                                description: 'The font style',

                                                path: 'stats/sparkLines/sections/reportLine/sampleMap/font/style',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Italic', value: 'italic'},
                                                    {text: 'Oblique', value: 'oblique'},
                                                ]
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the sample type',

                                        path: 'stats/sparkLines/sections/reportLine/sampleMap/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                    {
                                        field: 'align',
                                        display: 'Align',
                                        description: 'The alignment of the sample type',

                                        path: 'stats/sparkLines/sections/reportLine/sampleMap/align',
                                        hive: 'stats',

                                        type: 'select',
                                        options: [
                                            {text: 'Left', value: 'left'},
                                            {text: 'Center', value: 'center'},
                                            {text: 'Right', value: 'right'},
                                        ]
                                    },
                                ]
                            },
                            {
                                name: 'Stat value',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'thickness',
                                                display: 'Thickness',
                                                description: 'The thickness of the border',

                                                path: 'stats/sparkLines/sections/reportLine/value/border/thickness',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 0,
                                                max: 10,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLine/value/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'The font decoration type',

                                                        path: 'stats/sparkLines/sections/reportLine/value/font/decoration/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Underline', value: 'underline'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'style',
                                                        display: 'Style',
                                                        description: 'The font decoration style',

                                                        path: 'stats/sparkLines/sections/reportLine/value/font/decoration/style',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Solid', value: 'solid'},
                                                            {text: 'Wavy', value: 'wavy'},
                                                            {text: 'Dotted', value: 'dotted'},
                                                            {text: 'Dashed', value: 'dashed'},
                                                            {text: 'Double', value: 'double'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/value/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                    {
                                                        field: 'thickness',
                                                        display: 'Thickness',
                                                        description: 'The thickness of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLine/value/font/decoration/thickness',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 0,
                                                        max: 10,
                                                        step: 1,
                                                        unit: 'px',
                                                    },
                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'size',
                                                display: 'Size',
                                                description: 'The size of the font',

                                                path: 'stats/sparkLines/sections/reportLine/value/font/size',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 6,
                                                max: 100,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLine/value/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'fontFamily',
                                                display: 'Family',
                                                description: 'The font family',

                                                path: 'stats/sparkLines/sections/reportLine/value/font/fontFamily',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Inconsolata', value: 'Inconsolata'},
                                                    {text: 'FiraGO', value: 'FiraGO'}
                                                ]
                                            },
                                            {
                                                field: 'weight',
                                                display: 'Weight',
                                                description: 'The font weight',

                                                path: 'stats/sparkLines/sections/reportLine/value/font/weight',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Bold', value: 'bold'}
                                                ]
                                            },
                                            {
                                                field: 'style',
                                                display: 'Style',
                                                description: 'The font style',

                                                path: 'stats/sparkLines/sections/reportLine/value/font/style',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Italic', value: 'italic'},
                                                    {text: 'Oblique', value: 'oblique'},
                                                ]
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the stat value',

                                        path: 'stats/sparkLines/sections/reportLine/value/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                    {
                                        field: 'align',
                                        display: 'Align',
                                        description: 'The alignment of the stat value',

                                        path: 'stats/sparkLines/sections/reportLine/value/align',
                                        hive: 'stats',

                                        type: 'select',
                                        options: [
                                            {text: 'Left', value: 'left'},
                                            {text: 'Center', value: 'center'},
                                            {text: 'Right', value: 'right'},
                                        ]
                                    },
                                ]
                            },
                            {
                                name: 'No value (empty cell)',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'thickness',
                                                display: 'Thickness',
                                                description: 'The thickness of the border',

                                                path: 'stats/sparkLines/sections/reportLine/noValue/border/thickness',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 0,
                                                max: 10,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLine/noValue/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the empty cell',

                                        path: 'stats/sparkLines/sections/reportLine/noValue/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                ]
                            },
                            {
                                name: 'Highlighters',
                                items: [
                                    {
                                        field: 'low',
                                        display: 'Range: Low',
                                        description: 'The background color of the lowest range',

                                        path: 'stats/sparkLines/sections/reportLine/highlighters/low',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                    {
                                        field: 'mid',
                                        display: 'Range: Mid',
                                        description: 'The background color of the middle range',

                                        path: 'stats/sparkLines/sections/reportLine/highlighters/mid',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                    {
                                        field: 'high',
                                        display: 'Range: High',
                                        description: 'The background color of the highest range',

                                        path: 'stats/sparkLines/sections/reportLine/highlighters/high',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                    {
                                        field: 'topProcess',
                                        display: 'Top process',
                                        description: 'The background color of the Top Process',

                                        path: 'stats/sparkLines/sections/reportLine/highlighters/topProcess',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                ]
                            },
                        ]
                    },
                    {
                        name: 'Report line dark',
                        children: [
                            {
                                name: 'Header',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLineDark/header/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                name: 'Region',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLineDark/region/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLineDark/region/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLineDark/region/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the region',

                                        path: 'stats/sparkLines/sections/reportLineDark/region/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                ]
                            },
                            {
                                name: 'Process',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLineDark/process/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLineDark/process/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLineDark/process/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the process',

                                        path: 'stats/sparkLines/sections/reportLineDark/process/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                ]
                            },
                            {
                                name: 'Sample name',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLineDark/sample/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLineDark/sample/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLineDark/sample/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the sample name',

                                        path: 'stats/sparkLines/sections/reportLineDark/sample/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                ]
                            },
                            {
                                name: 'Sample type',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLineDark/sampleMap/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLineDark/sampleMap/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLineDark/sampleMap/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the sample type',

                                        path: 'stats/sparkLines/sections/reportLineDark/sampleMap/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                ]
                            },
                            {
                                name: 'Stat value',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLineDark/value/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Font',
                                        children: [
                                            {
                                                name: 'Decoration',
                                                items: [
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font decoration',

                                                        path: 'stats/sparkLines/sections/reportLineDark/value/font/decoration/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                ]
                                            }
                                        ],
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the font',

                                                path: 'stats/sparkLines/sections/reportLineDark/value/font/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                        ]
                                    }
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the stat value',

                                        path: 'stats/sparkLines/sections/reportLineDark/value/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                ]
                            },
                            {
                                name: 'No value (empty cell)',
                                children: [
                                    {
                                        name: 'Border',
                                        items: [
                                            {
                                                field: 'color',
                                                display: 'Color',
                                                description: 'The color of the border',

                                                path: 'stats/sparkLines/sections/reportLineDark/noValue/border/color',
                                                hive: 'stats',

                                                type: 'color',
                                            }
                                        ]
                                    },
                                ],
                                items: [
                                    {
                                        field: 'color',
                                        display: 'Background color',
                                        description: 'The background color of the empty cell',

                                        path: 'stats/sparkLines/sections/reportLineDark/noValue/background',
                                        hive: 'stats',

                                        type: 'color',
                                    },
                                ]
                            },
                        ]
                    }, {
                        name: 'Graph',
                        children: [
                            {
                                name: 'Series',
                                children: [
                                    {
                                        name: 'Default colors',
                                        items: [
                                            {
                                                field: '0',
                                                display: 'Series 1 color ',
                                                description: 'The default series 1 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/0',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '1',
                                                display: 'Series 2 color ',
                                                description: 'The default series 2 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/1',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '2',
                                                display: 'Series 3 color ',
                                                description: 'The default series 3 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/2',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '3',
                                                display: 'Series 4 color ',
                                                description: 'The default series 4 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/3',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '4',
                                                display: 'Series 5 color ',
                                                description: 'The default series 5 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/4',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '5',
                                                display: 'Series 6 color ',
                                                description: 'The default series 6 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/5',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '6',
                                                display: 'Series 7 color ',
                                                description: 'The default series 7 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/6',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '7',
                                                display: 'Series 8 color ',
                                                description: 'The default series 8 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/7',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '8',
                                                display: 'Series 9 color ',
                                                description: 'The default series 9 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/8',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '9',
                                                display: 'Series 10 color ',
                                                description: 'The default series 10 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/9',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '10',
                                                display: 'Series 11 color ',
                                                description: 'The default series 11 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/10',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: '11',
                                                display: 'Series 12 color ',
                                                description: 'The default series 12 color',

                                                path: 'stats/sparkLines/sections/graph/series/colors/11',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                        ]
                                    },
                                    {
                                        name: 'Line',
                                        children: [
                                            {
                                                name: 'Stroke',
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'The line type',

                                                        path: 'stats/sparkLines/sections/graph/series/line/stroke/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Smooth', value: 'smooth'},
                                                            {text: 'Straight', value: 'straight'},
                                                            {text: 'Step Line', value: 'stepline'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'smoothTension',
                                                        display: 'Smooth tension',
                                                        description: 'The tension of the interpolator',

                                                        path: 'stats/sparkLines/sections/graph/series/line/stroke/smoothTension',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 0.1,
                                                        max: 0.8,
                                                        step: 0.1,
                                                        unit: '',
                                                    },
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the line',

                                                        path: 'stats/sparkLines/sections/graph/series/line/stroke/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                    {
                                                        field: 'width',
                                                        display: 'Width',
                                                        description: 'The width of the line',

                                                        path: 'stats/sparkLines/sections/graph/series/line/stroke/width',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 1,
                                                        max: 10,
                                                        step: 1,
                                                        unit: '',
                                                    },
                                                    {
                                                        field: 'dashArray',
                                                        display: 'Dashing',
                                                        description: 'The optional dash spacing of the line',

                                                        path: 'stats/sparkLines/sections/graph/series/line/stroke/dashArray',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 0,
                                                        max: 10,
                                                        step: 1,
                                                        unit: '',
                                                    },
                                                ]
                                            },
                                            {
                                                name: 'Marking',
                                                children: [
                                                    {
                                                        name: 'Markers',
                                                        items: [
                                                            {
                                                                field: 'shape',
                                                                display: 'Shape',
                                                                description: 'The shape of the marker',

                                                                path: 'stats/sparkLines/sections/graph/series/line/marking/markers/shape',
                                                                hive: 'stats',

                                                                type: 'select',
                                                                options: [
                                                                    {text: 'Circle', value: 'circle'},
                                                                    {text: 'Square', value: 'rect'},
                                                                    {text: 'Rounded square', value: 'rectRounded'},
                                                                    {text: 'Cross', value: 'cross'},
                                                                    {text: 'Star', value: 'star'},
                                                                    {text: 'Triangle', value: 'triangle'},
                                                                ]
                                                            },
                                                            {
                                                                field: 'size',
                                                                display: 'Size',
                                                                description: 'The size of the marker',

                                                                path: 'stats/sparkLines/sections/graph/series/line/marking/markers/size',
                                                                hive: 'stats',

                                                                type: 'numeric',
                                                                min: 0,
                                                                max: 20,
                                                                step: 1,
                                                                unit: '',
                                                            },
                                                            {
                                                                field: 'color',
                                                                display: 'Color',
                                                                description: 'The color of the marking',

                                                                path: 'stats/sparkLines/sections/graph/series/line/marking/markers/color',
                                                                hive: 'stats',

                                                                type: 'color',
                                                            },
                                                        ]
                                                    },

                                                ],
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'A Marker will highlight the data point.',

                                                        path: 'stats/sparkLines/sections/graph/series/line/marking/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'None', value: 'none'},
                                                            {text: 'Markers', value: 'Markers'},
                                                        ]
                                                    },

                                                ]
                                            }

                                        ]
                                    },
                                    {
                                        name: 'Area',
                                        children: [
                                            {
                                                name: 'Stroke',
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'The line type',

                                                        path: 'stats/sparkLines/sections/graph/series/area/stroke/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Smooth', value: 'smooth'},
                                                            {text: 'Straight', value: 'straight'},
                                                            {text: 'Step Line', value: 'stepline'},
                                                        ]
                                                    },
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the line',

                                                        path: 'stats/sparkLines/sections/graph/series/area/stroke/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                    {
                                                        field: 'width',
                                                        display: 'Width',
                                                        description: 'The width of the line',

                                                        path: 'stats/sparkLines/sections/graph/series/area/stroke/width',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 1,
                                                        max: 10,
                                                        step: 1,
                                                        unit: '',
                                                    },
                                                    {
                                                        field: 'dashArray',
                                                        display: 'Dashing',
                                                        description: 'The optional dash spacing of the line',

                                                        path: 'stats/sparkLines/sections/graph/series/area/stroke/dashArray',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 0,
                                                        max: 10,
                                                        step: 1,
                                                        unit: '',
                                                    },
                                                ]
                                            },
                                            {
                                                name: 'Marking',
                                                children: [
                                                    {
                                                        name: 'Markers',
                                                        items: [
                                                            {
                                                                field: 'shape',
                                                                display: 'Shape',
                                                                description: 'The shape of the marker',

                                                                path: 'stats/sparkLines/sections/graph/series/area/marking/markers/shape',
                                                                hive: 'stats',

                                                                type: 'select',
                                                                options: [
                                                                    {text: 'Circle', value: 'circle'},
                                                                    {text: 'Square', value: 'square'},
                                                                ]
                                                            },
                                                            {
                                                                field: 'size',
                                                                display: 'Size',
                                                                description: 'The size of the marker',

                                                                path: 'stats/sparkLines/sections/graph/series/area/marking/markers/size',
                                                                hive: 'stats',

                                                                type: 'numeric',
                                                                min: 0,
                                                                max: 20,
                                                                step: 1,
                                                                unit: '',
                                                            },
                                                            {
                                                                field: 'color',
                                                                display: 'Color',
                                                                description: 'The color of the marking',

                                                                path: 'stats/sparkLines/sections/graph/series/area/marking/markers/color',
                                                                hive: 'stats',

                                                                type: 'color',
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        name: 'Data labels',
                                                        items: [
                                                            {
                                                                field: 'size',
                                                                display: 'Size',
                                                                description: 'The size of the font',

                                                                path: 'stats/sparkLines/sections/graph/series/area/marking/dataLabels/size',
                                                                hive: 'stats',

                                                                type: 'numeric',
                                                                min: 6,
                                                                max: 100,
                                                                step: 1,
                                                                unit: 'px',
                                                            },
                                                            {
                                                                field: 'color',
                                                                display: 'Color',
                                                                description: 'The color of the font',

                                                                path: 'stats/sparkLines/sections/graph/series/area/marking/dataLabels/color',
                                                                hive: 'stats',

                                                                type: 'color',
                                                            },
                                                            {
                                                                field: 'fontFamily',
                                                                display: 'Family',
                                                                description: 'The font family',

                                                                path: 'stats/sparkLines/sections/graph/series/area/marking/dataLabels/fontFamily',
                                                                hive: 'stats',

                                                                type: 'select',
                                                                options: [
                                                                    {text: 'Inconsolata', value: 'Inconsolata'},
                                                                    {text: 'FiraGO', value: 'FiraGO'}
                                                                ]
                                                            },
                                                            {
                                                                field: 'weight',
                                                                display: 'Weight',
                                                                description: 'The font weight',

                                                                path: 'stats/sparkLines/sections/graph/series/area/marking/dataLabels/weight',
                                                                hive: 'stats',

                                                                type: 'select',
                                                                options: [
                                                                    {text: 'Normal', value: 'normal'},
                                                                    {text: 'Bold', value: 'bold'}
                                                                ]
                                                            },
                                                        ],
                                                    },
                                                ],
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'A Marker will highlight the data point.<br> A Data label will also display the value.',

                                                        path: 'stats/sparkLines/sections/graph/series/area/marking/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'None', value: 'none'},
                                                            {text: 'Markers', value: 'Markers'},
                                                            {text: 'Data labels', value: 'dataLabels'},
                                                        ]
                                                    },
                                                ]
                                            },
                                            {
                                                name: 'Fill',
                                                items: [
                                                    {
                                                        field: 'type',
                                                        display: 'Type',
                                                        description: 'A gradient fill will apply a gardient fill to the area',

                                                        path: 'stats/sparkLines/sections/graph/series/area/fill/type',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Solid', value: 'solid'},
                                                            {text: 'Gradient', value: 'gradient'},
                                                        ]
                                                    },
                                                ],
                                                children: [
                                                    {
                                                        name: 'Gradient',
                                                        items: [
                                                            {
                                                                field: 'type',
                                                                display: 'Type',
                                                                description: 'The gradient direction',

                                                                path: 'stats/sparkLines/sections/graph/series/area/fill/gradient/type',
                                                                hive: 'stats',

                                                                type: 'select',
                                                                options: [
                                                                    {text: 'Vertical', value: 'vertical'},
                                                                    {text: 'Horizontal', value: 'horizontal'},
                                                                    {text: 'Diagonal1', value: 'diagonal1'},
                                                                    {text: 'Diagonal2', value: 'diagonal2'},
                                                                ]
                                                            },
                                                            {
                                                                field: 'shade',
                                                                display: 'Shade',
                                                                description: 'The gradient shade',

                                                                path: 'stats/sparkLines/sections/graph/series/area/fill/gradient/shade',
                                                                hive: 'stats',

                                                                type: 'select',
                                                                options: [
                                                                    {text: 'Light', value: 'light'},
                                                                    {text: 'Dark', value: 'dark'},
                                                                ]
                                                            },
                                                            {
                                                                field: 'opacityFrom',
                                                                display: 'Opacity From',
                                                                description: 'The start transparency of the gradient',

                                                                path: 'stats/sparkLines/sections/graph/series/area/fill/gradient/opacityFrom',
                                                                hive: 'stats',

                                                                type: 'numeric',
                                                                min: 0,
                                                                max: 1,
                                                                step: 0.1,
                                                            },
                                                            {
                                                                field: 'opacityTo',
                                                                display: 'Opacity to',
                                                                description: 'The end transparency of the gradient',

                                                                path: 'stats/sparkLines/sections/graph/series/area/fill/gradient/opacityTo',
                                                                hive: 'stats',

                                                                type: 'numeric',
                                                                min: 0,
                                                                max: 1,
                                                                step: .1,
                                                            },
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                ],
                                items: [
                                    {
                                        field: 'default',
                                        display: 'Default',
                                        description: 'The default series type',

                                        path: 'stats/sparkLines/sections/graph/series/default',
                                        hive: 'stats',

                                        type: 'select',
                                        options: [
                                            {text: 'Line', value: 'line'},
                                            {text: 'Area', value: 'area'},
                                        ]
                                    },

                                ]
                            },
                            {
                                name: 'Chart',
                                children: [
                                    {
                                        name: 'Global settings',
                                        items: [
                                            {
                                                field: 'background',
                                                display: 'Background color',
                                                description: 'The background color of the chart',

                                                path: 'stats/sparkLines/sections/graph/chart/globals/background',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Foreground color',
                                                description: 'The foreground color of the chart',

                                                path: 'stats/sparkLines/sections/graph/chart/globals/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'fontFamily',
                                                display: 'Font family',
                                                description: 'The font family used in the chart. This value can be overridden for specific items.',

                                                path: 'stats/sparkLines/sections/graph/chart/globals/fontFamily',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Inconsolata', value: 'Inconsolata'},
                                                    {text: 'FiraGO', value: 'FiraGO'}
                                                ]
                                            },
                                            {
                                                field: 'theme',
                                                display: 'Default theme',
                                                description: 'The default theme used in the graph',

                                                path: 'stats/sparkLines/sections/graph/chart/globals/theme',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Light', value: 'light'},
                                                    {text: 'Dark', value: 'dark'}
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        name: 'Global settings dark theme',
                                        items: [
                                            {
                                                field: 'background',
                                                display: 'Background color',
                                                description: 'The background color of the chart',

                                                path: 'stats/sparkLines/sections/graph/chart/globalsDark/background',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'color',
                                                display: 'Foreground color',
                                                description: 'The foreground color of the chart',

                                                path: 'stats/sparkLines/sections/graph/chart/globalsDark/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                        ]
                                    },
                                    //path: 'stats/sparkLines/sections/graph/chart/title/text',
                                    {
                                        name: 'Title',
                                        items: [
                                            {
                                                field: 'display',
                                                display: 'Display',
                                                description: 'If display the title or not',

                                                path: 'stats/sparkLines/sections/graph/chart/title/display',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                            {
                                                field: 'align',
                                                display: 'Text alignment',
                                                description: 'The alignment of the text',

                                                path: 'stats/sparkLines/sections/graph/chart/title/align',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Start', value: 'start'},
                                                    {text: 'Center', value: 'center'},
                                                    {text: 'End', value: 'end'},
                                                ]
                                            },
                                            {
                                                field: 'position',
                                                display: 'Text position',
                                                description: 'The position of the text',

                                                path: 'stats/sparkLines/sections/graph/chart/title/position',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Top', value: 'top'},
                                                    {text: 'Left', value: 'left'},
                                                    {text: 'Bottom', value: 'bottom'},
                                                    {text: 'Right', value: 'right'},
                                                ]
                                            },
                                        ],
                                        children: [
                                            {
                                                name: 'Style',
                                                items: [
                                                    {
                                                        field: 'size',
                                                        display: 'Size',
                                                        description: 'The size of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/title/style/size',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 6,
                                                        max: 100,
                                                        step: 1,
                                                        unit: 'px',
                                                    },
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/title/style/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                    {
                                                        field: 'fontFamily',
                                                        display: 'Family',
                                                        description: 'The font family',

                                                        path: 'stats/sparkLines/sections/graph/chart/title/style/fontFamily',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Inconsolata', value: 'Inconsolata'},
                                                            {text: 'FiraGO', value: 'FiraGO'}
                                                        ]
                                                    },
                                                    {
                                                        field: 'weight',
                                                        display: 'Weight',
                                                        description: 'The font weight',

                                                        path: 'stats/sparkLines/sections/graph/chart/title/style/weight',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Bold', value: 'bold'}
                                                        ]
                                                    },

                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Sub title',
                                        items: [
                                            {
                                                field: 'display',
                                                display: 'Display',
                                                description: 'If display the title or not',

                                                path: 'stats/sparkLines/sections/graph/chart/subTitle/display',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                            {
                                                field: 'align',
                                                display: 'Text alignment',
                                                description: 'The alignment of the text',

                                                path: 'stats/sparkLines/sections/graph/chart/subTitle/align',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Start', value: 'start'},
                                                    {text: 'Center', value: 'center'},
                                                    {text: 'End', value: 'end'},
                                                ]
                                            },
                                            {
                                                field: 'position',
                                                display: 'Text position',
                                                description: 'The position of the text',

                                                path: 'stats/sparkLines/sections/graph/chart/subTitle/position',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Top', value: 'top'},
                                                    {text: 'Left', value: 'left'},
                                                    {text: 'Bottom', value: 'bottom'},
                                                    {text: 'Right', value: 'right'},
                                                ]
                                            },
                                        ],
                                        children: [
                                            {
                                                name: 'Style',
                                                items: [
                                                    {
                                                        field: 'size',
                                                        display: 'Size',
                                                        description: 'The size of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/subTitle/style/size',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 6,
                                                        max: 100,
                                                        step: 1,
                                                        unit: 'px',
                                                    },
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'The color of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/subTitle/style/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                    {
                                                        field: 'fontFamily',
                                                        display: 'Family',
                                                        description: 'The font family',

                                                        path: 'stats/sparkLines/sections/graph/chart/subTitle/style/fontFamily',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Inconsolata', value: 'Inconsolata'},
                                                            {text: 'FiraGO', value: 'FiraGO'}
                                                        ]
                                                    },
                                                    {
                                                        field: 'weight',
                                                        display: 'Weight',
                                                        description: 'The font weight',

                                                        path: 'stats/sparkLines/sections/graph/chart/subTitle/style/weight',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Bold', value: 'bold'}
                                                        ]
                                                    },

                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Legend',
                                        items: [
                                            {
                                                field: 'display',
                                                display: 'Display',
                                                description: 'Whether to show or hide the legend container',

                                                path: 'stats/sparkLines/sections/graph/chart/legend/display',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                            {
                                                field: 'position',
                                                display: 'Position',
                                                description: 'The position of the legend',

                                                path: 'stats/sparkLines/sections/graph/chart/legend/position',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Top', value: 'top'},
                                                    {text: 'Bottom', value: 'bottom'},
                                                    {text: 'Left', value: 'left'},
                                                    {text: 'Right', value: 'right'},
                                                ]
                                            },
                                            {
                                                field: 'hAlign',
                                                display: 'H align',
                                                description: 'The alignment of the text',

                                                path: 'stats/sparkLines/sections/graph/chart/legend/hAlign',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Start', value: 'start'},
                                                    {text: 'Center', value: 'center'},
                                                    {text: 'End', value: 'end'},
                                                ]
                                            },

                                        ],
                                        children: [
                                            {
                                                name: 'Font',
                                                items: [
                                                    {
                                                        field: 'size',
                                                        display: 'Size',
                                                        description: 'The size of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/legend/font/size',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 6,
                                                        max: 100,
                                                        step: 1,
                                                        unit: 'px',
                                                    },
                                                    {
                                                        field: 'fontFamily',
                                                        display: 'Family',
                                                        description: 'The font family',

                                                        path: 'stats/sparkLines/sections/graph/chart/legend/font/fontFamily',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Inconsolata', value: 'Inconsolata'},
                                                            {text: 'FiraGO', value: 'FiraGO'}
                                                        ]
                                                    },
                                                    {
                                                        field: 'weight',
                                                        display: 'Weight',
                                                        description: 'The font weight',

                                                        path: 'stats/sparkLines/sections/graph/chart/legend/font/weight',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Bold', value: 'bold'}
                                                        ]
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        name: 'Grid',
                                        items: [
                                            {
                                                field: 'xAxisLines',
                                                display: 'X Axis Lines',
                                                description: 'Whether to show / hide x-axis lines',

                                                path: 'stats/sparkLines/sections/graph/chart/grid/xAxisLines',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                            {
                                                field: 'yAxisLines',
                                                display: 'Y Axis Lines',
                                                description: 'Whether to show / hide y-axis lines',

                                                path: 'stats/sparkLines/sections/graph/chart/grid/yAxisLines',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                        ],
                                        children: [
                                            {
                                                name: 'Row',
                                                items: [
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'Grid background colors filling in row pattern. It will alternate with "transparent"',

                                                        path: 'stats/sparkLines/sections/graph/chart/grid/row/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                ]
                                            },
                                            {
                                                name: 'Column',
                                                items: [
                                                    {
                                                        field: 'color',
                                                        display: 'Color',
                                                        description: 'Grid background colors filling in row pattern. It will alternate with "transparent"',

                                                        path: 'stats/sparkLines/sections/graph/chart/grid/column/color',
                                                        hive: 'stats',

                                                        type: 'color',
                                                    },
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        name: 'Timescale',
                                        items: [
                                            {
                                                field: 'show',
                                                display: 'Show',
                                                description: 'If display the labels or not',

                                                path: 'stats/sparkLines/sections/graph/chart/xaxis/labels/show',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                            {
                                                field: 'colors',
                                                display: 'Color',
                                                description: 'The color of the labels',

                                                path: 'stats/sparkLines/sections/graph/chart/xaxis/labels/colors',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'fontSize',
                                                display: 'Font size',
                                                description: 'The size of the font',

                                                path: 'stats/sparkLines/sections/graph/chart/xaxis/labels/fontSize',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 6,
                                                max: 100,
                                                step: 1,
                                                unit: 'px',
                                            },
                                            {
                                                field: 'fontFamily',
                                                display: 'Family',
                                                description: 'The font family',

                                                path: 'stats/sparkLines/sections/graph/chart/xaxis/labels/fontFamily',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Inconsolata', value: 'Inconsolata'},
                                                    {text: 'FiraGO', value: 'FiraGO'}
                                                ]
                                            },
                                            {
                                                field: 'fontWeight',
                                                display: 'Font weight',
                                                description: 'The font weight',

                                                path: 'stats/sparkLines/sections/graph/chart/xaxis/labels/fontWeight',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Normal', value: 'normal'},
                                                    {text: 'Bold', value: 'bold'}
                                                ]
                                            },

                                        ]
                                    },
                                    {
                                        name: 'Tooltips',
                                        items: [
                                            {
                                                field: 'enabled',
                                                display: 'Enabled',
                                                description: 'Whether to show / hide the tooltips on hover',

                                                path: 'stats/sparkLines/sections/graph/chart/tooltips/enabled',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                        ]
                                    },
                                    {
                                        name: 'Y axis common defaults',
                                        items: [
                                            {
                                                field: 'show',
                                                display: 'Show',
                                                description: 'Set it to true to display the y axis',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/show',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                            {
                                                field: 'colors',
                                                display: 'Color',
                                                description: 'The color of the labels',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/color',
                                                hive: 'stats',

                                                type: 'color',
                                            },
                                            {
                                                field: 'type',
                                                display: 'Linear or Logarithmic',
                                                description: 'Choose between a linear or logarithmic scale',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/type',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Linear', value: 'linear'},
                                                    {text: 'Logarithmic', value: 'logarithmic'}
                                                ]
                                            },
                                            {
                                                field: 'useManualSetting',
                                                display: 'Manually force the scaling',
                                                description: 'When set to true it will use the min and max value to set the scale',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/useManualSetting',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                            {
                                                field: 'min',
                                                display: 'Minimum value',
                                                description: 'The minimum value of the scale.',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/min',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 1,
                                                max: 1000000000,
                                                step: 1,
                                            },
                                            {
                                                field: 'max',
                                                display: 'Maximum value',
                                                description: 'The maximum value of the scale.',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/max',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 1,
                                                max: 1000000000,
                                                step: 1,
                                            },
                                        ],
                                        children: [
                                            {
                                                name: 'Labels',
                                                items: [
                                                    {
                                                        field: 'show',
                                                        display: 'Show the labels',
                                                        description: 'Set it to true if you want to display the labels',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/labels/show',
                                                        hive: 'stats',

                                                        type: 'boolean',
                                                    },
                                                    {
                                                        field: 'fontSize',
                                                        display: 'Font size',
                                                        description: 'The size of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/labels/style/fontSize',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 6,
                                                        max: 48,
                                                        step: 1,
                                                        unit: 'px',
                                                    },
                                                    {
                                                        field: 'fontWeight',
                                                        display: 'Weight',
                                                        description: 'The weight of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/labels/style/fontWeight',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Bold', value: 'bold'}
                                                        ]
                                                    },
                                                    {
                                                        field: 'fontFamily',
                                                        display: 'Family',
                                                        description: 'The font family',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/labels/style/fontFamily',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Inconsolata', value: 'Inconsolata'},
                                                            {text: 'FiraGO', value: 'FiraGO'}
                                                        ]
                                                    },
                                                ]
                                            },
                                            {
                                                name: 'Border',
                                                items: [
                                                    {
                                                        field: 'show',
                                                        display: 'Show the border',
                                                        description: 'Set it to true if you want to display the border',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/axisBorder/show',
                                                        hive: 'stats',

                                                        type: 'boolean',
                                                    },
                                                ]
                                            },
                                            {
                                                name: 'Title',
                                                items: [
                                                    {
                                                        field: 'text',
                                                        display: 'Text',
                                                        description: 'The text to display as description',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/title/text',
                                                        hive: 'stats',

                                                        type: 'text',
                                                    },
                                                    {
                                                        field: 'align',
                                                        display: 'Alignment',
                                                        description: 'The alignment of the title',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/title/style/align',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Start', value: 'start'},
                                                            {text: 'Center', value: 'center'},
                                                            {text: 'End', value: 'end'}
                                                        ]
                                                    },
                                                    {
                                                        field: 'fontSize',
                                                        display: 'Font size',
                                                        description: 'The size of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/title/style/fontSize',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 6,
                                                        max: 48,
                                                        step: 1,
                                                        unit: 'px',
                                                    },
                                                    {
                                                        field: 'fontWeight',
                                                        display: 'Size',
                                                        description: 'The weight of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/title/style/fontWeight',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Bold', value: 'bold'}
                                                        ]
                                                    },
                                                    {
                                                        field: 'fontFamily',
                                                        display: 'Family',
                                                        description: 'The font family',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsCommon/title/style/fontFamily',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Inconsolata', value: 'Inconsolata'},
                                                            {text: 'FiraGO', value: 'FiraGO'}
                                                        ]
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        name: 'Y axis series defaults',
                                        items: [
                                            {
                                                field: 'type',
                                                display: 'Linear or Logarithmic',
                                                description: 'Choose between a linear or logarithmic scale',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/type',
                                                hive: 'stats',

                                                type: 'select',
                                                options: [
                                                    {text: 'Linear', value: 'linear'},
                                                    {text: 'Logarithmic', value: 'logarithmic'}
                                                ]
                                            },
                                            {
                                                field: 'useManualSetting',
                                                display: 'Manually force the scaling',
                                                description: 'When set to true it will use the min and max value to set the scale',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/useManualSetting',
                                                hive: 'stats',

                                                type: 'boolean',
                                            },
                                            {
                                                field: 'min',
                                                display: 'Minimum value',
                                                description: 'The minimum value of the scale.',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/min',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 1,
                                                max: 1000000000,
                                                step: 1,
                                            },
                                            {
                                                field: 'max',
                                                display: 'Maximum value',
                                                description: 'The maximum value of the scale.',

                                                path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/max',
                                                hive: 'stats',

                                                type: 'numeric',
                                                min: 1,
                                                max: 1000000000,
                                                step: 1,
                                            },
                                        ],
                                        children: [
                                            {
                                                name: 'Labels',
                                                items: [
                                                    {
                                                        field: 'show',
                                                        display: 'Show the labels',
                                                        description: 'Set it to true if you want to display the labels',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/labels/show',
                                                        hive: 'stats',

                                                        type: 'boolean',
                                                    },
                                                    {
                                                        field: 'fontSize',
                                                        display: 'Font size',
                                                        description: 'The size of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/labels/style/fontSize',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 6,
                                                        max: 48,
                                                        step: 1,
                                                        unit: 'px',
                                                    },
                                                    {
                                                        field: 'fontWeight',
                                                        display: 'Weight',
                                                        description: 'The weight of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/labels/style/fontWeight',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Bold', value: 'bold'}
                                                        ]
                                                    },
                                                    {
                                                        field: 'fontFamily',
                                                        display: 'Family',
                                                        description: 'The font family',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/labels/style/fontFamily',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Inconsolata', value: 'Inconsolata'},
                                                            {text: 'FiraGO', value: 'FiraGO'}
                                                        ]
                                                    },
                                                ]
                                            },
                                            {
                                                name: 'Border',
                                                items: [
                                                    {
                                                        field: 'show',
                                                        display: 'Show the border',
                                                        description: 'Set it to true if you want to display the border',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/axisBorder/show',
                                                        hive: 'stats',

                                                        type: 'boolean',
                                                    },
                                                ]
                                            },
                                            {
                                                name: 'Title',
                                                items: [
                                                    {
                                                        field: 'text',
                                                        display: 'Text',
                                                        description: 'The text to display as description',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/title/text',
                                                        hive: 'stats',

                                                        type: 'text',
                                                    },
                                                    {
                                                        field: 'align',
                                                        display: 'Alignment',
                                                        description: 'The alignment of the title',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/title/style/align',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Start', value: 'start'},
                                                            {text: 'Center', value: 'center'},
                                                            {text: 'End', value: 'end'}
                                                        ]
                                                    },
                                                    {
                                                        field: 'fontSize',
                                                        display: 'Font size',
                                                        description: 'The size of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/title/style/fontSize',
                                                        hive: 'stats',

                                                        type: 'numeric',
                                                        min: 6,
                                                        max: 48,
                                                        step: 1,
                                                        unit: 'px',
                                                    },
                                                    {
                                                        field: 'fontWeight',
                                                        display: 'Size',
                                                        description: 'The weight of the font',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/title/style/fontWeight',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Normal', value: 'normal'},
                                                            {text: 'Bold', value: 'bold'}
                                                        ]
                                                    },
                                                    {
                                                        field: 'fontFamily',
                                                        display: 'Family',
                                                        description: 'The font family',

                                                        path: 'stats/sparkLines/sections/graph/chart/yAxis/defaultsSeries/title/style/fontFamily',
                                                        hive: 'stats',

                                                        type: 'select',
                                                        options: [
                                                            {text: 'Inconsolata', value: 'Inconsolata'},
                                                            {text: 'FiraGO', value: 'FiraGO'}
                                                        ]
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                ],
                            },
                        ],
                        items: [
                            {
                                field: 'buffering',
                                display: 'Buffer size',
                                description: 'The maximum number of samples that will be used, before the buffer starts to rotate.<br>This value is related to the speed of the client computer. A too high number may have the socket disconnect. If that occurs, choose a lower value',

                                path: 'stats/sparkLines/sections/graph/buffering',
                                hive: 'stats',

                                type: 'numeric',
                                min: 0,
                                max: 500,
                                step: 50,
                            },
                        ]
                    },
                ],
                items: [
                    {
                        field: 'theme',
                        display: 'Theme',
                        description: 'The theme to be used, light or dark',

                        path: 'stats/sparkLines/theme',
                        hive: 'stats',

                        type: 'select',
                        options: [
                            {text: 'Light', value: 'light'},
                            {text: 'Dark', value: 'dark'}
                        ]
                    },
                ]
            },
            {
                name: 'Web sockets',
                items: [
                    {
                        field: 'connectionTimeout',
                        display: 'Connection timeout',
                        description: 'The timeout for the Web sockets client on connection',

                        path: 'stats/webSockets/connectionTimeout',
                        hive: 'stats',

                        type: 'numeric',
                        min: 500,
                        max: 10000,
                        step: 100,
                        unit: 'milliseconds',
                    },
                    {
                        field: 'defaultResponseTimeout',
                        display: 'Response timeout',
                        description: 'The timeout for the Web sockets message response',

                        path: 'stats/webSockets/defaultResponseTimeout',
                        hive: 'stats',

                        type: 'numeric',
                        min: 500,
                        max: 10000,
                        step: 100,
                        unit: 'milliseconds',
                    },
                ]
            }
        ],
        items: [
            {
                field: 'displayHelpAtTabOpen',
                display: 'Display help at tab open',
                description: 'When enabled, it will display a help dialog when the tab is opened',

                path: 'stats/displayHelpAtTabOpen',
                hive: 'stats',

                type: 'boolean',
            },

        ]
    },

    // Replication
    {
        name: 'Replication',
        children: [
            {
                name: 'Dashboard',
                items: [
                    {
                        field: 'refreshBacklog',
                        display: 'Refresh backlog',
                        description: 'If refresh the backlog when the Topology is not in use',

                        path: 'defaults/replication/dashboard/refreshBacklog',
                        hive: 'defaults',

                        type: 'select',
                        options: [
                            {text: 'Off', value: 'off'},
                            {text: '2 seconds', value: '2'},
                            {text: '5 seconds', value: '5'},
                            {text: '10 seconds', value: '10'},
                            {text: '20 seconds', value: '20'},
                            {text: '30 seconds', value: '30'},
                            {text: '1 minute', value: '60'},
                            {text: '5 minutes', value: '300'},
                            {text: '15 minutes', value: '900'}
                        ]
                    },
                    {
                        field: 'processRange',
                        display: 'Process range',
                        description: 'Changes the backlog color when data is greater than 0 AND is not getting spooled',

                        path: 'defaults/replication/dashboard/processRange',
                        hive: 'defaults',

                        type: 'boolean',
                    },
                ]
            },
            {
                name: 'Discovery service',
                children: [
                    {
                        name: 'Servers',
                        children: [
                            {
                                name: '0',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/0/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/0/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/0/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/0/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/0/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/0/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/0/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            },
                            {
                                name: '1',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/1/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/1/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/1/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/1/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/1/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/1/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/1/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            },
                            {
                                name: '2',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/2/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/2/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/2/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/2/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/2/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/2/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/2/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '3',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/3/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/3/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/3/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/3/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/3/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/3/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/3/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '4',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/4/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/4/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/4/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/4/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/4/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/4/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/4/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '5',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/5/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/5/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/5/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/5/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/5/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/5/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/5/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '6',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/6/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/6/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/6/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/6/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/6/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/6/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/6/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '7',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/7/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/7/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/7/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/7/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/7/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/7/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/7/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '8',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/8/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/8/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/8/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/8/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/8/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/8/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/8/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '9',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/9/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/9/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/9/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/9/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/9/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/9/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/9/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '10',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/10/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/10/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/10/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/10/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/10/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/10/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/10/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '11',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/11/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/11/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/11/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/11/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/11/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/11/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/11/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '12',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/12/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/12/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/12/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/12/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/12/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/12/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/12/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '13',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/13/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/13/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/13/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/13/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/13/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/13/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/13/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '14',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/14/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/14/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/14/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/14/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/14/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/14/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/14/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            }, {
                                name: '15',
                                items: [
                                    {
                                        field: 'instance',
                                        display: 'Instance name',
                                        description: 'The name of the instance',

                                        path: 'defaults/replication/discoveryService/servers/15/instance',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'host',
                                        display: 'Host',
                                        description: 'The host of the instance. It can either an IP number or a host name',

                                        path: 'defaults/replication/discoveryService/servers/15/host',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'port',
                                        display: 'GUI Port',
                                        description: 'The port number of the GUI',

                                        path: 'defaults/replication/discoveryService/servers/15/port',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'statsPort',
                                        display: 'GUI statistics port',
                                        description: 'The (optional) port where the GUI statistics is listening)',

                                        path: 'defaults/replication/discoveryService/servers/15/statsPort',
                                        hive: 'defaults',

                                        type: 'numeric',
                                        min: 1,
                                        max: 65535,
                                        step: 1,
                                        unit: '',
                                    },
                                    {
                                        field: 'protocol',
                                        display: 'Protocol',
                                        description: 'The protocol used in the communication',

                                        path: 'defaults/replication/discoveryService/servers/15/protocol',
                                        hive: 'defaults',

                                        type: 'select',
                                        options: [
                                            {text: 'http', value: 'http'},
                                            {text: 'https', value: 'https'},
                                        ]
                                    },
                                    {
                                        field: 'tlsFileLocation',
                                        display: 'Certificate Chain Location',
                                        description: 'The location of the TLS Certificate Chain to Trust',

                                        path: 'defaults/replication/discoveryService/servers/15/tlsFileLocation',
                                        hive: 'defaults',

                                        type: 'text',
                                    },
                                    {
                                        field: 'verifyCertificates',
                                        display: 'Verify certificates',
                                        description: 'By selecting false we allow self-signed certificates',

                                        path: 'defaults/replication/discoveryService/servers/15/verifyCertificates',
                                        hive: 'defaults',

                                        type: 'boolean',
                                    },
                                ]
                            },
                        ],
                    },
                    {
                        name: 'Timeouts',
                        items: [
                            {
                                field: 'onConnect',
                                display: 'Connection timeout',
                                description: 'The timeout for the crawler on connection',

                                path: 'defaults/replication/discoveryService/timeouts/onConnect',
                                hive: 'defaults',

                                type: 'numeric',
                                min: 500,
                                max: 10000,
                                step: 100,
                                unit: 'milliseconds',
                            },
                            {
                                field: 'onResponse',
                                display: 'Response timeout',
                                description: 'The timeout for the crawler on waiting for a response',

                                path: 'defaults/replication/discoveryService/timeouts/onResponse',
                                hive: 'defaults',

                                type: 'numeric',
                                min: 2000,
                                max: 10000,
                                step: 500,
                                unit: 'milliseconds',
                            },
                        ]
                    },
                ]
            },
            {
                name: 'Topology',
                children: [
                    {
                        name: 'Links',
                        items: [
                            {
                                field: 'connector',
                                display: 'Default connector',
                                description: 'The default connector that will be initially used',

                                path: 'defaults/replication/topology/links/connector',
                                hive: 'defaults',

                                type: 'select',
                                options: [
                                    {text: 'Straight', value: 'straight'},
                                    {text: 'Curve', value: 'curve'},
                                    {text: 'Smooth', value: 'smooth'},
                                ]
                            },
                            {
                                field: 'router',
                                display: 'Default router',
                                description: 'The default router that will be initially used',

                                path: 'defaults/replication/topology/links/router',
                                hive: 'defaults',

                                type: 'select',
                                options: [
                                    {text: 'Normal', value: 'normal'},
                                    {text: 'Manhattan', value: 'manhattan'},
                                    {text: 'Metro', value: 'metro'},
                                    {text: 'Orthogonal', value: 'orthogonal'},
                                ]
                            },
                        ]
                    },
                    {
                        name: 'Backlog',
                        items: [
                            {
                                field: 'processRange',
                                display: 'Process range',
                                description: 'Changes the backlog color when data is greater than 0 AND is not getting spooled',

                                path: 'defaults/replication/topology/backlog/processRange',
                                hive: 'defaults',

                                type: 'boolean',
                            },
                        ]
                    }
                ],
                items: [
                    {
                        field: 'layout',
                        display: 'Default layout orientation',
                        description: 'The default layout orientation to use',

                        path: 'defaults/replication/topology/layout',
                        hive: 'defaults',

                        type: 'select',
                        options: [
                            {text: 'Left to Right', value: 'LR'},
                            {text: 'Top to Bottom', value: 'TB'},
                        ]
                    },
                    {
                        field: 'refresh',
                        display: 'Default refresh value',
                        description: 'The default refresh value',

                        path: 'defaults/replication/topology/refresh',
                        hive: 'defaults',

                        type: 'select',
                        options: [
                            {text: 'Off', value: '0'},
                            {text: '2 seconds', value: '2'},
                            {text: '5 seconds', value: '5'},
                            {text: '10 seconds', value: '10'},
                            {text: '20 seconds', value: '20'},
                            {text: '30 seconds', value: '30'},
                            {text: '1 minute', value: '60'},
                            {text: '5 minutes', value: '300'},
                            {text: '15 minutes', value: '900'}
                        ]
                    },
                    {
                        field: 'undoBufferSize',
                        display: 'Undo buffer size',
                        description: 'The maximum number of entries in the undo buffer',

                        path: 'defaults/replication/topology/undoBufferSize',
                        hive: 'defaults',

                        type: 'numeric',
                        min: 20,
                        max: 1000,
                        step: 20,
                        unit: '',
                    },
                ]
            },

        ],
    },

    // General
    {
        name: 'General',
        children: [],
        items: [
            {
                field: 'flashInterval',
                display: 'Flash interval',
                description: 'The interval in milliseconds for the flashing of red alerts',

                path: 'dashboard/flashInterval',
                hive: 'dashboard',

                type: 'numeric',
                min: 100,
                max: 3000,
                step: 100,
                unit: 'milliseconds',
            },
            {
                field: 'mainScreenRefresh',
                display: 'Dashboard auto refresh',
                description: 'The interval in minutes for the refresh of the main screen',

                path: 'defaults/mainScreenRefresh',
                hive: 'defaults',

                type: 'select',
                options: [
                    {text: 'Off', value: 'off'},
                    {text: '1 minute', value: '1'},
                    {text: '2 minutes', value: '2'},
                    {text: '5 minutes', value: '5'},
                    {text: '10 minutes', value: '10'},
                    {text: '30 minutes', value: '30'},
                ],

                postProcess: 'dashboardTimeout.set(true)'
            },
        ]
    },
]

app.prefs.manifestRebuild = () => {
    app.prefs.manifest = JSON.parse(JSON.stringify(app.prefs.manifestBaseline))
}
