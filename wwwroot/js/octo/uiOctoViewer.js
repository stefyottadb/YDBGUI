/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.octoViewer.highlightActiveTab = (tab, type) => {
    const tabType = tab.id.split('-')[0]
    const tabOctoTable = $('#tabOctoTable' + type)
    const tabOctoText = $('#tabOctoText' + type)

    $('#tabOctoResultTabs' + type).children().find('[name="octo-sub-tab"]')
        .css('text-decoration', 'none')

    $(tab).find('[name="octo-sub-tab"]')
        .css('text-decoration', 'underline')
}

app.ui.octoViewer.onResizeSplitter = (event, ui) => {
    const type = ui.element.attr('id').slice(-4)

    // track height changing from splitter // +
    const offset = ui.size.height + 224
    $('#divOctoResultDivs' + type).children().css('height', 'calc(100vh - ' + offset + 'px)')
}

app.ui.octoViewer.onResizeSplitterVertical = (event, ui) => {
    const type = ui.element.attr('id').slice(-4)

    // track width changing from splitter
    $('#divOctoTables' + type).width(ui.size.width)
    app.ui.octoTree.width[type] = ui.size.width

    $('#divOctoTables' + type).find('#octoSearchBar').width(ui.size.width - 42)


    $('#divTreeButton' + type).css('padding-right', (app.ui.octoTree.width[type] - 67) > 966 ? 966 : app.ui.octoTree.width[type] - 67)
}

app.ui.octoViewer.zoomPressed = (tabId, direction) => {
    let fontSize = app.ui.octoViewer.fontSize[tabId];

    $('body').addClass('wait');

    if (direction === '-') {
        if (fontSize > 6) {
            fontSize--

        } else {
            $('body').removeClass('wait');

            return
        }

    } else {
        if (fontSize < 35) {
            fontSize++

        } else {
            $('body').removeClass('wait');

            return
        }
    }

    app.ui.octoViewer.fontSize[tabId] = fontSize;

    const el = $('#divOctoEditor' + tabId).find('div')[0];
    el.style.fontSize = fontSize + 'px';

    const doc = app.ui.octoViewer.cm[tabId]
    doc.refresh();

    $('body').removeClass('wait');
}

app.ui.octoViewer.themePressed = tabId => {
    const lightFlag = $('#btnOctoTheme' + tabId).attr('aria-pressed');
    const doc = app.ui.octoViewer.cm[tabId]

    app.ui.octoViewer.theme.theme = lightFlag === 'true' ? 'light' : 'dark';

    doc.setOption('theme', app.ui.octoViewer.theme.theme === 'light' ? 'default' : 'night')
    doc.getAllMarks().forEach(marker => marker.clear())

    doc.refresh()
}

app.ui.octoViewer.clearText = type => {
    const doc = app.ui.octoViewer.cm[type]

    doc.setValue('')
}

app.ui.octoViewer.copyText = type => {
    const doc = app.ui.octoViewer.cm[type]

    // first we check if the doc is empty
    if (doc.getValue() === '') {
        app.ui.msgbox.show('There is no text to copy...', app.ui.msgbox.INPUT_ERROR)

        return
    }

    // then we check if there is some selected text
    const selection = doc.getSelection()

    if (selection !== '') {
        // copy selected
        app.ui.copyToClipboard(selection)

    } else {
        // copy entire doc
        app.ui.copyToClipboard(doc.getValue())
    }
}

app.ui.octoViewer.cliClear = type => {
    $('#divOctoCli' + type).empty()

    const tab = $('#tabOctoText' + type).find('.octo-queried-tab').remove()
}

app.ui.octoViewer.undoPressed = type => {
    const doc = app.ui.octoViewer.cm[type]

    doc.undo()
}

app.ui.octoViewer.redoPressed = type => {
    const doc = app.ui.octoViewer.cm[type]

    doc.redo()
}

app.ui.octoViewer.cmKeyPress = (e, type) => {
    // trap Ctrl + Enter
    if ((e.which && e.which === 10) || (e.originalEvent.ctrlKey === true && e.originalEvent.which === 13))
        app.ui.octoViewer.runPressed(type)
}


// display or hide the tree
app.ui.octoViewer.treePressed = async (type, res = null) => {
    let showTree = $('#btnOctoTree' + type).attr('aria-pressed');
    const timeout = res !== null ? 0 : 250

    if (res !== null) showTree = "false"

    $('#btnOctoTreeRefresh' + type).css('display', showTree === 'false' ? 'block' : 'none')
    $('#divOctoTables' + type).animate({width: showTree === 'true' ? 0 : app.ui.octoTree.width[type]}, timeout)
    $('#divTreeButton' + type).animate({'padding-right': showTree === 'true' ? 20 : app.ui.octoTree.width[type] - 67 > 966 ? 966 : app.ui.octoTree.width[type] - 67}, timeout)

    if (app.ui.octoTree.loaded[type] === false) {
        app.ui.octoTree.refresh(type, res)
        app.ui.octoTree.loaded[type] = true
    }
}

app.ui.octoViewer.resetButtons = type => {
    // enable RUN button
    $('#btnOctoRun' + type)
        .attr('disabled', false)
        .removeClass('disabled')
}

// extract the SQL command out of multiple lines
app.ui.octoViewer.getSql = type => {
    const doc = app.ui.octoViewer.cm[type]
    const currentLine = doc.getCursor().line
    const docTotalLines = doc.lineCount()
    let sql = []
    let start = {}
    let end = {}

    sql[currentLine] = doc.getLine(currentLine)
    start.line = currentLine
    start.ch = 0

    end.line = currentLine
    end.ch = doc.getLine(currentLine).length

    if (sql[currentLine] === '') {

        return {sql: '', sel: {start: start, end: end}}
    }

    // go backward first
    if (currentLine > 0) {
        for (let ix = currentLine; ix >= 0; ix--) {
            const line = doc.getLine(ix)

            if (line === '') break

            sql[ix] = line
            start.line = ix
        }
    }

    // then forward
    if (currentLine + 1 < docTotalLines) {
        for (let ix = currentLine; ix < docTotalLines; ix++) {
            const line = doc.getLine(ix)

            if (line === '') break

            sql[ix] = line
            end.line = ix
            end.ch = line.length
        }
    }

    // *********************
    // highlight the commands
    // *********************

    // clear all classes across the all document
    doc.getAllMarks().forEach(marker => marker.clear())

    // then apply bgnd class to selected rows
    doc.markText(start, end, {
        className: doc.getOption('theme') === 'night' ? 'text-bgnd-orchid' : 'text-bgnd-yellow'
    })

    // generate a single string
    sql = sql.join('\n')

    // *********************
    // append LIMIT where needed
    // *********************

    // we need to split the queries by semicolon and process them one by one
    const splitQuery = sql.split(';')

    splitQuery.forEach((query, ix) => {
        const sqlLowerCase = query.toLowerCase().trimStart()
        if (sqlLowerCase.indexOf('select') === 0 && sqlLowerCase.indexOf('limit') === -1 && parseInt($('#inpOctoLimit' + type).val()) > 0) {
            splitQuery[ix] = query + ' limit ' + $('#inpOctoLimit' + type).val()
        }
    })

    sql = splitQuery.join(';')

    return {sql: sql, sel: {start: start, end: end}}
}

// determine if query can be executed in RO mode
app.ui.octoViewer.checkSqlRights = sql => {
    if (app.serverMode !== 'RW') {
        let command = sql.split(' ')[0].toLowerCase().trim()
        if (command.indexOf(';') > -1) {
            command = sql.slice(0, -1)
        }
        command = command.replace(/\n/gm, '')
        return command === '\\q' || command === '\\d'

    } else return true
}

// ************************************************
// drag and drop
// ************************************************
app.ui.octoViewer.processDrop = async (cm, e) => {
    e.preventDefault()

    if (app.ui.octoTree.dragStart[0] === undefined) return

    if (app.userSettings.defaults.octo.displayHelpOnDrop === true) {
        const ret = await app.ui.msgboxCheck.show(app.ui.octoViewer.helpText, 'HELP', 'Don\'t show this again', false)

        if (ret === true) {
            app.userSettings.defaults.octo.displayHelpOnDrop = false
            app.ui.storage.save('defaults', app.userSettings.defaults)
        }
    }

    // get Code Mirror drop coordinates in char unit and extract operation
    const drop = {
        pos: cm.coordsChar({left: e.pageX, top: e.pageY}, 'page'),
        source: app.ui.octoTree.dragStart[0],
        ctrl: e.ctrlKey,
        pageX: e.pageX,
        pageY: e.pageY,
        cm: cm
    }
    Object.assign(drop, {
        line: cm.getLine(drop.pos.line),
        opCode: drop.source.data.selection[0],
        text: drop.source.data.selection[1],
        table: drop.source.data.selection[3] || '',
        type: drop.source.data.type
    })

    if (drop.opCode === 'column') {
        // dropping column, check modifier to prefix table name + dot
        const text = drop.ctrl === false ? drop.text : drop.table + '.' + drop.text

        cm.replaceRange(text, drop.pos, drop.pos)

        cm.focus()

    } else if (drop.opCode === 'function') {
        cm.replaceRange(drop.text, drop.pos, drop.pos)

        cm.focus()

    } else {
        // dropping table / view
        if (drop.ctrl === false) {
            // no modifier, drop table name
            cm.replaceRange(drop.text, drop.pos, drop.pos)

            cm.focus()

        } else {
            // modifier found, display list of fields
            $('#divOctoHelperStatement')
                .css('display', 'block')
                .css('left', e.pageX + 'px')
                .css('top', e.pageY + 'px')

            $('#btnOctoHelperInsert')
                .attr('disabled', drop.opCode === 'view')
                .addClass(drop.opCode === 'view' ? 'disabled' : '')
                .removeClass(drop.opCode === 'table' ? 'disabled' : '')

            $('#btnOctoHelperUpdate')
                .attr('disabled', drop.opCode === 'view')
                .addClass(drop.opCode === 'view' ? 'disabled' : '')
                .removeClass(drop.opCode === 'table' ? 'disabled' : '')

            $('#btnOctoHelperDelete')
                .attr('disabled', drop.opCode === 'view')
                .addClass(drop.opCode === 'view' ? 'disabled' : '')
                .removeClass(drop.opCode === 'table' ? 'disabled' : '')

            app.ui.octoViewer.mountModalHandlers('divOctoHelperStatement')

            app.ui.octoViewer.dropContext = drop

            $('#divOctoHelperStatement').focus()
        }
    }
}

// drag and drop popups in when dropping with CTRL down
app.ui.octoViewer.processDropHelperStatement = async opCode => {
    let text = ''
    const drop = app.ui.octoViewer.dropContext
    let columns = []

    switch (opCode) {
        case 'select': {
            $('#divOctoHelperStatement').css('display', 'none')
            if (drop.source.children.length >= 1) {
                // we need to fetch the children
                drop.source.children = []

                if (drop.opCode === 'table') {
                    await app.ui.octoTree.populateTable(drop.source, drop.type)

                    columns = $('#treeOctoTables' + drop.type).jstree(true).get_node(drop.source.children[2]).children

                } else {
                    await app.ui.octoTree.populateView(drop.source, drop.type)
                    columns = $('#treeOctoTables' + drop.type).jstree(true).get_node(drop.source.children[1]).children
                }
            }

            let options = '<option name="octoOpt" selected>*</option>'

            columns.forEach(column => {
                options += '<option name="octoOpt">' + column.split('-')[3] + '</option>'
            })

            $('#divOctoHelperColumns')
                .css('display', 'block')
                .css('left', drop.pageX + 'px')
                .css('top', drop.pageY + 'px')

            app.ui.octoViewer.mountModalHandlers('divOctoHelperColumns')

            $('#selOctoHelperColumns')
                .empty()
                .append(options)
                .focus()

            return
        }
        case 'insert': {
            text = 'INSERT INTO ' + drop.text + ' '
            break
        }
        case 'update': {
            text = 'UPDATE ' + drop.text + ' SET '
            break
        }
        case 'delete': {
            text = 'DELETE ' + drop.text + ' '
        }
    }

    drop.cm.replaceRange(text, drop.pos, drop.pos)
    $('#divOctoHelperStatement').css('display', 'none')

    drop.cm.focus()
}

app.ui.octoViewer.processDropHelperColumns = () => {
    const drop = app.ui.octoViewer.dropContext
    let text = 'SELECT '
    let starFound = false

    $('#selOctoHelperColumns option:selected').toArray().map(item => {
        if (starFound === true) return

        text += item.text + ', '

        if (item.text === '*') starFound = true
    })

    text = text.slice(0, -2) + ' FROM ' + drop.text

    drop.cm.replaceRange(text, drop.pos, drop.pos)
    $('#divOctoHelperColumns').css('display', 'none')

    drop.cm.focus()
}

app.ui.octoViewer.mountModalHandlers = target => {
    setTimeout(() => {
        $('body').on('click', e => {
            $('#divOctoPopupDefinition').css('display', 'none')

            const name = e.target.getAttribute("name")

            if (name === 'octoOpt' || name === 'octoBtn') return

            $('#' + target).css('display', 'none')

            $('body').off('click')
        })
    }, 200)     // timeout needed for when modal msgbox is displayed
}

app.ui.octoViewer.helpText = 'You can drop tables, columns and function.<br><br>' +
    'By holding the CTRL key while dragging, you can get the following functionality:<br><br>' +
    '- When dragging a column, it will insert the text: [table].[column]<br>' +
    '- When dragging a table, it will pop up a dialog where you can choose the action: SELECT, INSERT, UPDATE or DELETE<br>' +
    '- If you choose SELECT, a new dialog will appear where you can multi-select the fields to be inserted.'

app.ui.octoViewer.processColumnsEnter = (op, e) => {
    const char = e.which || e.originalEvent.which

    if (char === 13 && op === 'column') app.ui.octoViewer.processDropHelperColumns()
    if (char === 27) {
        $('#divOctoHelperColumns').css('display', 'none')
        $('#divOctoHelperStatement').css('display', 'none')
    }
}

app.ui.octoViewer.closeTab = (tabId) => {
    //const prevItem = $('#' + tabId).parent().prev();

    $('#' + tabId)
        .tab('dispose')
        .remove()

    $('#divOctoTable' + tabId.substring(12, 99)).remove();
};

// **********************************************
// Tab edit
// **********************************************

app.ui.octoViewer.tabInEdit = {}

app.ui.octoViewer.renameTab = type => {
    app.ui.octoViewer.tabInEdit = {
        oldValue: $('#tab' + type + '  [name="tab-editable"').text(),
        type: type
    }

    $('#btnOctoTabEditOk')
        .attr('disabled', true)
        .addClass('disabled')

    $('#inpOctoTabEdit').val(app.ui.octoViewer.tabInEdit.oldValue)

    $('#modalOctoTabEdit')
        .modal({show: true, backdrop: 'static'})
        .on('shown.bs.modal', () => {
            $('#inpOctoTabEdit').focus()
        })
}

app.ui.octoViewer.renameTabKeyUp = e => {
    const char = e.which || e.originalEvent.which

    if (char === 13) {
        app.ui.octoViewer.renameTabOkPressed()

        return
    }

    $('#btnOctoTabEditOk')
        .attr('disabled', false)
        .removeClass('disabled')

}
app.ui.octoViewer.renameTabOkPressed = () => {
    $('#tab' + app.ui.octoViewer.tabInEdit.type + '  [name="tab-editable"').text($('#inpOctoTabEdit').val())

    $('#modalOctoTabEdit').modal('hide')
}
