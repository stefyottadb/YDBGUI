/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

let settings = {};

// *******************
// Initialization
// *******************

// System Initialization at startup
window.onload = async () => {
    // ********************************
    // loads all included HTML
    // ********************************
    await loadFiles('data-include');

    // ********************************
    // initialize popper
    // ********************************
    $('[data-toggle="popover"]').popover({
        html: true, container: 'body', template: '<div class="popover" role="tooltip"><div class="arrow "></div><h3 class="popover-header ydb-popover-header "></h3><div class="popover-body"></div></div>'
    });

    // ********************************
    // detect storage operation RESET
    // ********************************
    storageReset = window.location.search.indexOf('?storage=reset') > -1;

    // ********************************
    // Init handlers
    // ********************************
    app.ui.addHandlers();

    // ********************************
    // set debug mode if needed
    // ********************************
    let debugMode = false

    try {
        const res = await app.REST._debugMode()

        if (res.debugMode === true) {
            console.log('Fetching debug settings...')
            try {
                await loadTestScript('js/debug/userSettings.debug.js')
                debugMode = true
            } catch (err) {
                console.log('Couldn\'t load debug mode file...')
            }
        }

    } catch (err) {
        console.log('Couldn\'t detect debug mode...')
    }

    // ********************************
    // detect testMode
    // ********************************
    testMode = window.location.search.indexOf('?test=') > -1;
    testStatsMode = window.location.search.indexOf('?testStats') > -1;

    // ********************************
    // detect and validate permalink
    // ********************************
    permaMode = {
        set: window.location.search.indexOf('?P=') > -1 && window.location.search.length > 3,
        link: window.location.search.slice(3)
    };

    if (permaMode.set === true) {

        // validate data in link
        try {
            permaMode.link = atob(permaMode.link)

            // check terminator
            if (permaMode.link.slice(-2) !== app.ui.permalinks.TERMINATOR) {
                app.ui.msgbox.show('The permalink is not valid...')

                permaMode = {
                    set: false,
                    link: ''
                }
            } else {
                // strip terminator
                permaMode.link = permaMode.link.slice(0, -2)

                const permaSplitted = permaMode.link.split('|')

                if (permaSplitted[0] !== 'routine' && permaSplitted[0] !== 'global') {
                    app.ui.msgbox.show('The permalink is not valid...')

                    permaMode = {
                        set: false,
                        link: ''
                    }
                }
            }
        } catch (err) {
            app.ui.msgbox.show('The permalink is not valid...')

            permaMode = {
                set: false,
                link: ''
            }
        }
    }

    // ********************************
    // get YDBGUI version number
    // ********************************
    try {
        $.getJSON('_ydbgui.manifest.json', async json => {
            app.version = json.version;

            // ********************************
            // Start the app, defer to dashboard refresh if in test mode
            // ********************************
            let authMode
            let clientConfig = ''

            try {
                const res = await app.REST._getAuth()
                authMode = res.auth

            } catch (err) {
                authMode = false
            }

            // ********************************
            // Load test file if needed
            // ********************************
            if (testMode === true) {
                await loadTestScript('test/helpers/mockData.js')
            }

            if (testStatsMode === true) {
                await loadTestScript('test/helpers/stats.js')
            }

            if (authMode === false) {
                $('#menuUserMode').removeAttr('data-toggle')

                $('#mnuDashUserOptions').css('display', 'none')

                app.ui.dashboard.refresh()

                // process Permalink
                if (permaMode.set === true) app.ui.permalinks.process(permaMode.link)

            } else {
                app.ui.login.show()
            }

            // display info summary in console
            const str = '%cYottaDB GUI  \n' +
                '%cVersion:\t\t\t' + app.version +
                '\nStorage version:\t' + app.userSettings.current.version +
                '\nTest mode:\t\t\t' + (testMode === true ? 'Yes' : 'No') +
                '\nPermalink mode:\t\t' + (permaMode.set === true ? 'Yes' : 'No') +
                (permaMode.set === true ? '\nPermalink path: ' + permaMode.link : '') +
                '\nAuth mode:\t\t\t' + (authMode === true ? '' : 'No') + ' login required' +
                '\nws-logging\t\t\t' + (window.location.search.indexOf('?ws-logging') > -1 ? 'Yes' : 'No') +
                '\nDebug mode:\t\t\t' + (debugMode ? 'Yes' : 'No')

            console.log(
                str,
                'font-family:monospace, monospace; font-size: 22px; color: #ff7f27;',
                'font-family:monospace, monospace; font-size: 16px; color: #eab83b;'
            )

            // set dashboard timeout
            dashboardTimeout.set(true)
        })

    } catch (err) {
        console.log('The following error occurred while fetching the version.json file:');
        console.log(err)
    }

    window.addEventListener('beforeunload', async e => {
        // check if we need to display message because some background tasks are still running
        if (app.ui.toaster.tasks > 0) {
            const str = 'There are tasks still running in the background...'

            e.preventDefault();
            e.returnValue = str

            return str
        }
    });

    // initialize timeout
    app.ui.login.timeout.init()

    // initialize the resize
    $('#divGviewerJsonViewer').resizable()
};

// set dashboard timeout
const dashboardTimeout = {
    hTimeout: 0,

    set: function (refreshDashboard = false) {
        if (refreshDashboard === true) app.ui.dashboard.autorefresh.onLoad()


        if (app.userSettings.defaults.mainScreenRefresh === 'off') {
            if (this.hTimeout > 0) this.reset()

        } else {
            if (this.hTimeout > 0) this.reset()

            this.hTimeout = setInterval(() => {
                app.ui.dashboard.refresh()

            }, parseInt(app.userSettings.defaults.mainScreenRefresh) * 1000 * 60)
        }
    },

    reset: function () {
        clearTimeout(this.hTimeout)

        this.hTimeout = 0
    }
}

const loadFiles = async (tag) => {
    let promises = [];

    let includes = $('[' + tag + ']');
    jQuery.each(includes, async function () {
        let def = new $.Deferred();

        const newThis = this;
        let file = './html/' + $(newThis).data('include') + '.html';
        $(this).load(file, () => {
            def.resolve();
        });
        promises.push(def)
    });

    return $.when.apply(undefined, promises).promise();
};

const loadTestScript = async filename => {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: filename,
            dataType: "script",
            success: resolve,
            error: reject
        })
    })
}

const loadJsonFile = filename => {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: filename,
            dataType: "json",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

// *******************
// Main app object
// *******************

let app = {
    libs: {},

    REST: {
        host: window.location.hostname,
        port: window.location.port,
        root: '/api/',
        username: '',
        password: ''
    },
    ui: {
        login: {},

        menu: {},

        timings: {},

        msgbox: {},
        msgboxCheck: {},
        inputbox: {},
        wait: {},
        createDir: {},
        dirSelect: {},

        dashboard: {},

        regionView: {},
        regionAdd: {
            name: {}
        },
        regionNames: {
            add: {},
            delete: {}
        },
        regionShared: {},
        regionCreateDbFile: {},
        regionSelect: {},
        regionDelete: {},
        regionExtend: {},
        regionEdit: {},
        regionJournalSwitch: {},
        regionFilename: {},
        regionJournalFiles: {},
        globalSize: {},
        locksManager: {},
        globalsManager: {},
        defrag: {},
        backup: {},
        gViewer: {},
        rViewer: {
            select: {}
        },
        help: {},
        tabs: {},
        compact: {},
        integ: {},
        storageSettings: {},
        storageRegion: {},
        storageWizard: {},
        backupWizard: {},
        storage: {},
        systemInfo: {
            envVars: {}
        },
        deviceInfo: {},
        dirSelect: {},
        fileSelect: {},
        globalMappings: {},
        permalinks: {},

        toaster: {},
        octoViewer: {},
        octoTree: {},

        gldFileSelect: {},
    },
    system: {},

    prefs: {},

    config: {
        timeout: 3600
    },
    currMouseTime: 0,
    timeoutId: 0
};

