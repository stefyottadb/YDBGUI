/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file manages the garphs settings when you have more than one graph (H or V orientation)

app.ui.stats.sparkChart.graphSettings = {
    init: function () {
        $('#btnStatsSparkChartGraphSettingsOk').on('click', () => this.okPressed())
        $('#btnStatsSparkChartGraphSettingsHelp').on('click', () => app.ui.help.show('stats/spark-chart/graph-config'))

        app.ui.setupDialogForTest('modalStatsSparkChartGraphSettings')
    },

    show: function (entry) {
        let graphCount = 0
        let msg = ''
        this.entry = entry

        entry.data.forEach(source => {
            source.data.views.forEach(view => {
                if (view.type === 'graph' && app.ui.stats.utils.mapObjToArray(view).length > 0) {
                    graphCount++
                }
            })
        })

        switch (graphCount) {
            case 0:
                msg = 'There are not graphs in the current configuration or they are not mapped...'
                break
            case 1:
                msg = 'There is only 1 graph in the configuration. No setup available.'
                break
            default:
        }

        if (msg !== '') {
            app.ui.msgbox.show(msg, 'WARNING')

            return
        }

        const orientation = entry.sparkChart.graphsConfig.orientation

        if (orientation === 'h') {
            $('#optStatsSparkChartGraphSettingsOrientationH').prop('checked', true)
            console.log('setting h')

        } else {
            $('#optStatsSparkChartGraphSettingsOrientationV').prop('checked', true)
            console.log('setting v')
        }

        $('#modalStatsSparkChartGraphSettings')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    okPressed: function () {
        if ($('#optStatsSparkChartGraphSettingsOrientationH').is(':checked')) {
            this.entry.sparkChart.graphsConfig.orientation = 'h'

        } else {
            this.entry.sparkChart.graphsConfig.orientation = 'v'
        }

        $('#modalStatsSparkChartGraphSettings').modal('hide')
    },

    entry: {}
}
