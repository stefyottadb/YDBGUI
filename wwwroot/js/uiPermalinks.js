/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.permalinks = {
    TERMINATOR: '@@',

    init: function () {
        $('#btnCopyToClipboard').on('click', () => this.copyToClipboard())

        // setup dialog for test
        app.ui.setupDialogForTest('modalPermalinks')

    },

    show: function () {
        const link = this.computeLink()

        if (link === '') return

        $('#inpPermalink').val(window.location.origin + '?P=' + link)

        $('#modalPermalinks').modal({show: true, backdrop: 'static'})
    },

    computeLink: function () {
        // is active window a global or a routine ?
        const type = app.ui.tabs.currentTab.split('-')[1]

        if (type === 'G') {
            // global
            const path = $('#inpGviewerPath' + app.ui.tabs.currentTab).val()
            const instance = app.ui.gViewer.instance[app.ui.tabs.currentTab]

            if (path === '^' || path === '' || instance.viewer.buffer.path === '') {
                app.ui.msgbox.show('No correct path has been specified...', 'Global viewer')

                return ''
            }

            return btoa('global|' + instance.viewer.buffer.path + this.TERMINATOR)

        } else if (type === 'R') {
            // routine
            const instance = app.ui.rViewer.instance[app.ui.tabs.currentTab];

            // detect changes in zroutines
            const newZroutines = app.ui.rViewer.searchPath.zroutines === app.system.zroutines

            return btoa('routine|' + instance.routine.name + (newZroutines ? '' : '|' + app.ui.rViewer.searchPath.zroutines) + this.TERMINATOR)
        }
    },

    copyToClipboard: () => {
        app.ui.copyToClipboard($('#inpPermalink').val(), $('#btnCopyToClipboard'))
    },

    process: link => {
        const perma = link.split('|')

        console.log('Permalink detected', perma)

        if (perma[0] === 'global') {
            const id = app.ui.gViewer.addNew()

            $('#inpGviewerPath' + id).val(perma[1])
            app.ui.gViewer.parser.parse(id, perma[1])

        } else {
            if (perma[2] !== undefined) {
                app.ui.rViewer.searchPath.zroutines = perma[2]
            }

            app.ui.rViewer.addNew({routineName: perma[1], tabId: null});
        }
    }
}
