/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.globalMappings.init = () => {
    app.ui.setupDialogForTest('modalGlobalMappings')
}

app.ui.globalMappings.show = () => {
    let globalMapping = [];

    // consolidate all names from existing regions
    Object.keys(app.system.regions).forEach((regionName, ix) => {
        const region = app.system.regions[regionName];
        region.names.forEach(name => {
            name.region = regionName
            globalMapping.push(name)
        })
    })

    // and sort them by global
    globalMapping = globalMapping.sort((a, b) => a.name > b.name ? 1 : -1)

    // populate the table
    const tblGlobalMappings = $('#tblGlobalMappings > tbody')

    tblGlobalMappings.empty()
    globalMapping.forEach(name => {
        let row = '<tr>'

        row += '<td>' + name.name + '</td>'
        row += '<td>' + name.region + '</td>'

        row += '</tr>'

        tblGlobalMappings.append(row)
    })

    $('#modalGlobalMappings').modal({show: true, backdrop: 'static'})
}
