<!--
/****************************************************************
*                                                              *
* Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
* All rights reserved.                                         *
*                                                              *
* This source code contains the intellectual property          *
* of its copyright holder(s), and is made available            *
* under a license.  If you do not know the terms of            *
* the license, please stop and do not read further.            *
*                                                              *
****************************************************************/
-->


## TESTING

The testing is performed by the means of standard Java Script testing tools: chai and puppeteer.
While chai manages the tests, puppeteer provides a headless Chrome browser with DOM access, to inspect graphic elements.

We test Client and Server independently, in the following way:

### Client

The Client testing is performed by generating different mock data in the `app.system` object, thus emulating multiple server responses scenario's. Each test number generates a different response, stimulating the interface to react according, so that we can inspect the graphic elements and determine the validity of the test.

### Server

The Server testing is not using the interface at all and is fully focused on the response returned by the REST call. By modifying the database structure, using OS or YDB MUPIP calls, we alter the REST response, so that we can analyze it and determine the validity of the test.];


# **CLIENT**

 

#### Dashboard: gld file

| Number | Description |
| :---:| ---      |
| 30 | When gld file is missing|

#### Dashboard: Regions list: Database

| Number | Description |
| :---:| ---      |
| 36 | when region[0] has a valid file|
| 37 | when region[0] has a file missing + no auto db|
| 38 | when region[0] has a file missing + auto db|
| 39 | when region[0] has a file ok but shmem is bad|
| 40 | Extension count > 0: 1 extensions left|
| 41 | Extension count > 0: 4 extensions left|
| 42 | Extension count > 0: 9 extensions left|
| 43 | Extension count = 0: 10% of db size left|
| 44 | Extension count = 0: 15% of db size left|
| 45 | Extension count = 0: 25% of db size left|
| 46 | Freeze mode is on|
| 47 | Dashboard: When an interrupted REORG is detected|
| 48 | Extension count = 0: 10% of db size left, value is 90.3|

#### Dashboard: Regions list: Journal

| Number | Description |
| :---:| ---      |
| 50 | when journal file is disabled AND repl off|
| 51 | when journal file is enabled but off AND repl off|
| 52 | when journal file is enabled and on (before) AND repl off|
| 53 | when journal file is enabled and on (nobefore) AND repl off|
| 54 | when journal file is enabled and in WasOn status|
| 55 | when journal file is enabled but off AND repl on AND 0 users|
| 56 | when journal file is enabled and on (before) AND repl on AND 0 users|
| 57 | when journal file is enabled and on (nobefore) AND repl on AND 0 users|
| 58 | when journal file is disabled AND repl on AND 0 users|
| 59 | when journal file is disabled AND repl on AND >0 users|
| 60 | when journal file is enabled but off AND repl on AND >0 users|
| 61 | when journal file is enabled and on (before) AND repl on AND >0 users|
| 62 | when journal file is enabled and on (nobefore) AND repl on AND >0 users|
| 63 | Journal file is missing and state = enabled/on|

#### Dashboard: Regions list: Devices

| Number | Description |
| :---:| ---      |
| 70 | When disk space is <70%|
| 71 | When disk space is >70% and <90%|
| 72 | When disk space is >90% and <97%|
| 73 | When disk space is >97% and <101%|

#### Dashboard: Global status

| Number | Description |
| :---:| ---      |
| 100 | When all dbs are ok|
| 101 | When a few db's have issues|
| 102 | When ALL dbs have issues|
| 103 | When all journals are ok|
| 104 | When some journals have issues|
| 105 | When all journals have issues|
| 106 | When at least one journal has the wasOn status set|
| 108 | When no region has replication turned on|
| 109 | When at least one journal has the wasOn status set|
| 110 | When freeze mode is on|
| 111 | Journal file is missing and state = enabled/on|
| 112 | If all regions have journal disabled, global status should be 'disabled'|

#### Dashboard: Events

| Number | Description |
| :---:| ---      |
| 120 | Clicking the region list[0] icon|
| 122 | Clicking the System info menu item|
| 123 | Clicking the devices[0] icon|

#### Dashboard: RO / RW mode indicator

| Number | Description |
| :---:| ---      |
| 148 | Ensure that the text is correct|
| 149 | Ensure no tick is visible when RO /RW mode, no authentication|
 

#### Help: load

| Number | Description |
| :---:| ---      |
| 34 | Try to load the help|
 

#### System Info

| Number | Description |
| :---:| ---      |
| 150 | Confirm that the three labels are populated correctly|
| 151 | Verify the existence of one plugin|
| 152 | Clicking the Env Var button should open a dialog|
| 170 | Env Vars: confirm that the list is populate with all three categories|
| 171 | Env Vars: Confirm that the list is populate with only system info|
| 172 | Env Vars: Confirm that the list is populate with only YDB data|
| 173 | Env Vars: Confirm that the list is populate with only GTM data|
| 174 | Confirm that the encryption flag is populated correctly|
| 175 | Verify that manifest file for _ydbgui is correct|
 

#### Devices

| Number | Description |
| :---:| ---      |
| 190 | Confirm that the four labels are populated correctly|
| 191 | Gauge when disk space is <70%|
| 192 | Gauge when disk space is >70% and <90%|
| 193 | Gauge when disk space is >90% and <97%|
| 194 | Gauge when disk space is >97% and <101%|
| 195 | Ensure that the extra device information is properly displayed|
 

#### Devices

| Number | Description |
| :---:| ---      |
| 190 | Confirm that the four labels are populated correctly|
 

#### Dashboard: PULLDOWN MENUS

| Number | Description |
| :---:| ---      |
| 200 | Make at least one region with no file AND autodb = true and verify that create db menu is enabled|
| 201 | Remove all journals from all regions (state = 0) and verify that journaling on/off is disabled|
| 202 | If all no regions have a file, extend menu is disabled|
| 203 | Ensure the extend menu is disabled in RO mode|
| 204 | Ensure the journal menu is disabled in RO mode|
| 205 | Ensure the create database is disabled in RO mode|
| 206 | Ensure the create region menu is disabled in RO mode|
| 207 | Ensure the edit region menu is disabled in RO mode|
| 208 | Ensure the delete region menu is disabled in RO mode|
| 209 | Ensure the journal switch menu is disabled in RO mode|
 

#### Region View: Database tab

| Number | Description |
| :---:| ---      |
| 250 | Db status when db file is missing and autodb is on|
| 251 | Db status when db file is missing and autodb is off|
| 252 | Db status when db file is bad|
| 253 | Db status when shmem  is bad|
| 254 | blocks gauge when free block space is <70%|
| 255 | blocks gauge when free block space is >70% and <90%|
| 256 | blocks gauge when free block space is >90% and <97%|
| 257 | blocks gauge when free block space is >97% and <101%|
| 258 | User sessions when users = 0|
| 259 | User sessions when users > 0|
| 260 | Alert message when db file is missing and autodb is on|
| 261 | Alert message when db file is missing and autodb is off|
| 262 | Alert message when db file is bad|
| 263 | Alert message when shmem is bad|
| 270 | Db file: Verify that the table gets populated|
| 271 | Db Access lists: Verify that the table gets populated|
| 280 | Button Create db: verify that is disabled when db file is missing and filename is empty|
| 281 | Button Create db: verify that is enabled when db file is missing and filename is specified|
| 282 | Button Create db: verify that is disabled when db file is good|
| 283 | Button Extend db: verify that is enabled at all times when database file exists and it is valid|
| 284 | Button Extend db: verify that is disabled  when database file does NOT exists|
| 285 | Button Extend db: verify that is disabled  when database file is invalid|
| 286 | Select Advanced Params and verify that extra fields are displayed in the db access table|
| 287 | when db file >90 % and extension > 0 should be green|
| 288 | Extension count > 0: 1 extensions left|
| 289 | Extension count > 0: 5 extensions left|
| 290 | Extension count > 0: 8 extensions left|
| 291 | Extension count = 0: 10% of db size left|
| 292 | Extension count = 0: 15% of db size left|
| 293 | Extension count = 0: 25% of db size left|
| 294 | With extension > 0, verify that Db Usage background is always green, but fore changes at 35%|
| 295 | Verify that, in RO mode, create database button is disabled|
| 296 | Verify that, in RO mode, extend database button is disabled|
| 297 | Region view: When an interrupted REORG is detected|

#### Region View: Journal tab

| Number | Description |
| :---:| ---      |
| 300 | Status when journal file is disabled AND repl off|
| 301 | Status when journal file is enabled but off AND repl off|
| 302 | Status when journal file is enabled and on (before) AND repl off|
| 303 | Status when journal file is enabled and in WasOn status|
| 304 | Status when journal file is enabled but off AND repl on AND 0 users|
| 305 | Status when journal file is enabled and on (before) AND repl on AND 0 users|
| 306 | Status when journal file is enabled and on (nobefore) AND repl on AND 0 users|
| 307 | Status when journal file is disabled AND repl on AND >0 users|
| 308 | Status when journal file is enabled but off AND repl on AND >0 users|
| 310 | If no journal, turn on/off button should be invisible|
| 313 | Verify that Type pill is properly populated with the type when journaling is enabled and Before|
| 314 | Verify that Type pill is properly populated with the type when journaling is enabled and Nobefore|
| 315 | Verify that Type pill is properly populated with the type when journaling disabled|
| 316 | Verify that the Parameters list gets properly populated|
| 317 | Verify that the entire Journal Parameter list is hidden when journal is disabled|
| 318 | Alert message when WAS ON is set|
| 319 | Alert message when replication is on and journal is disabled|
| 320 | Alert message when replication is on and journal is enabled / off|
| 321 | Alert message when replication is off and journal is enabled / off|
| 322 | Journal file is missing and state = 2: verify alert|
| 323 | Journal file is missing and state = 2: verify button text|
| 324 | Verify that, in RO mode, turn on/off button is disabled|
| 325 | move jrnl file, verify that alert is displayed|
| 326 | no journal file, verify that buttons are disabled, move jrnl file back|
| 327 | Verify that the journal size is correct and popup displays the correct values|
| 328 | Region: YDBJNLF, verify journal size displays N/A|

#### Region View: Globals tab

| Number | Description |
| :---:| ---      |
| 330 | No globals, verify text|
| 331 | Global found, verify text and status|
| 332 | Zombie found, verify text and status|
| 333 | Verify that Size... button is enabled when one or more globals are present|
| 334 | Verify that Size... button is disabled when NO globals are present|
| 335 | move db file, verify that globals tab is disabled, restore file|

#### Region View: Names tab

| Number | Description |
| :---:| ---      |
| 350 | Verify that the list is empty|
| 351 | Verify that the list content changes when clicking the checkbox and display also the %YDBOCTO|
| 352 | Ensure, when choosing a tab different than Region or Journal, that checkbox \"Advanced\" is hidden|

#### Region View: Stats tab

| Number | Description |
| :---:| ---      |
| 360 | Verify that the table gets populated with >0 items|

#### Region View: Locks tab

| Number | Description |
| :---:| ---      |
| 370 | Verify that the label Processes on queue gets populated|
| 371 | Verify that the label Lock slots in use gets populated|
| 372 | Verify that the label Slot bytes in use gets populated|
| 373 | Verify that the label Free lock space gets populated|
| 374 | Lock table: no locks|
| 375 | Lock table: 1 lock, no waiter|
| 376 | Lock table: 1 lock, 1 waiter|
| 377 | Lock table: 1 lock, 2 waiters|
| 378 | Lock table: 2 lock, no waiter|
| 379 | Lock table: 2 locks, 1 waiter|
| 380 | Lock table: 2 locks, 2 waiters|
 

#### Region Delete

| Number | Description |
| :---:| ---      |
| 400 | Verify that clicking YES it will display a second dialog|
| 401 | Delete a region, type in the wrong region name and verify that you can NOT submit|
| 402 | Delete a region, type in the correct region and verify that you can submit|
 

#### Region Extend

| Number | Description |
| :---:| ---      |
| 410 | Verify that the dialog is displayed|
| 411 | Verify that the top three labels are populated correctly|
| 412 | Based on entered ext. size, ensure that New size and New avail. space are correct|
| 413 | Submit the form and expect a dialog|
| 414 | Submit the form and answer NO. Expect the dialog to disappear|
| 415 | Submit the form and answer YES. Expect the dialog to disappear|
 

#### Region Select

| Number | Description |
| :---:| ---      |
| 430 | Display dialog, scroll down twice, submit: verify that the correct dialog is open|
| 431 | Display dialog, scroll down twice and then up, submit: verify that the correct dialog is open|
 

#### Create Database

| Number | Description |
| :---:| ---      |
| 440 | Verify that dialog is displayed|
| 441 | Verify that the three labels are populated correctly|
| 442 | Submit the form and expect a dialog|
| 443 | Submit the form and answer NO. Expect the dialog to disappear|
| 444 | Submit the form and answer YES. Expect the dialog to close and an error dialog open with error: file already exists|
 

#### Turn journal on/off dialog

| Number | Description |
| :---:| ---      |
| 450 | Verify that the dialog is displayed and with the text 'OFF'|
| 451 | Submit the form and answer NO. Expect the dialog to close|
| 452 | Submit the form and answer YES. Expect the dialog to close|
 

#### Switch Journal dialog

| Number | Description |
| :---:| ---      |
| 450 | Verify that the dialog is displayed and with the text 'OFF'|
| 451 | Submit the form and answer NO. Expect the dialog to close|
| 452 | Submit the form and answer YES. Expect the dialog to close|
 

#### Add Region: Region tab

| Number | Description |
| :---:| ---      |
| 490 | Default display should be BG table|
| 491 | Clicking MM it should display the MM table|
| 492 | Click on BG filename and verify dialog is displayed|
| 493 | Click on MM filename and verify dialog is displayed|
| 494 | Click on advanced mode and verify that BG table has more rows|
| 495 | Click on advanced mode and verify that MM table has more rows|
| 496 | Click on advanced mode and verify that right table has more rows|

#### Add Region: Journal tab

| Number | Description |
| :---:| ---      |
| 520 | Default display should be no journal|
| 522 | Turn the journal on and verify that table is displayed|
| 523 | Click on Journal filename and verify dialog is displayed|
| 524 | Default display should be no journal|

#### Add Region: Names tab

| Number | Description |
| :---:| ---      |
| 540 | Click on add name button and verify that dialog is displayed|
| 541 | Click on add name button and verify that dialog is displayed|
| 542 | Add a name, select it, click delete name and verify that it gets deleted|
| 543 | Add multiple names and ensure they got sorted|
 

#### Edit Region: Region tab

| Number | Description |
| :---:| ---      |
| 555 | Edit DEFAULT and verify that BG is checked|
| 556 | Edit DEFAULT and switch to MM: Edit button should be enabled|
| 557 | Edit YDBAIM and verify that MM is checked|
| 558 | Edit YDBAIM and switch to BG: Edit button should be enabled|
| 559 | Click Advanced parameters and verify that left table is properly populated|
| 560 | Click Advanced parameters and verify that right table is properly populated|
| 561 | Edit DEFAULT and click on the filename button. Verify that the message box appears|
| 562 | Edit YDBAIM and click on the filename button. Verify that the message box appears|

#### Edit Region: Journal tab

| Number | Description |
| :---:| ---      |
| 570 | Edit DEFAULT and verify that journal is ON|
| 571 | Edit DEFAULT and switch the journal off. Verify that the table gets hidden|
| 572 | Edit DEFAULT and switch the journal off. Verify that the Edit button is enabled|
| 573 | Edit DEFAULT and click on the filename button. Verify that the Filename dialog appears|

#### Edit region: Names Tab

| Number | Description |
| :---:| ---      |
| 586 | Edit YDBOCTO, add a name %a and verify that appears in the list as first and in GREEN color|
| 587 | Edit YDBOCTO, add a name %b, select it and verify that delete button caption is: Delete...|
| 588 | Edit YDBOCTO, select the first entry and delete it. Verify that text has the RED color|
| 589 | Edit YDBOCTO, select the first entry and delete it. Verify that delete button caption is: Undelete...|
| 590 | Edit YDBOCTO, select the first entry and delete it. Click the Undelete... button and verify that text is in Purple color|
 

#### Tree integrity

| Number | Description |
| :---:| ---      |
| 600 | One lock per region with different PIDs, 1, 2 and 3 waiters per lock. Test By Namespace tree, lock 1|
| 601 | One lock per region with different PIDs, 1, 2 and 3 waiters per lock. Test By Namespace tree, lock 2|
| 602 | One lock per region with different PIDs, 1, 2 and 3 waiters per lock. Test By Namespace tree, lock 3|
| 603 | One lock per region with different PIDs, 1, 2 and 3 waiters per lock. Test By Region tree, lock 1|
| 604 | One lock per region with different PIDs, 1, 2 and 3 waiters per lock. Test By Region tree, lock 2|
| 605 | One lock per region with different PIDs, 1, 2 and 3 waiters per lock. Test By Region tree, lock 3|
| 606 | One lock per region with different PIDs, 1, 2 and 3 waiters per lock. Test By PIDs tree, lock 1|
| 607 | One lock per region with different PIDs, 1, 2 and 3 waiters per lock. Test By PIDs tree, lock 2|
| 608 | One lock per region with different PIDs, 1, 2 and 3 waiters per lock. Test By PIDs tree, lock 3|
| 609 | Test sort on namespace|
| 610 | Test sort on region|
| 611 | Test sort on PIDs|
| 612 | No locks at all. Test By Namespace tree|
| 613 | No locks at all. Test By Regions tree|
| 614 | No locks at all. Test By PIDs tree|

#### Submit messages => Clear lock

| Number | Description |
| :---:| ---      |
| 615 | Test By Namespace tree, select namespace, click on Clear Lock|
| 616 | Test By Namespace tree, select region, click on Clear Lock|
| 617 | Test By Namespace tree, select PID, click on Clear Lock|
| 618 | Test By Namespace tree, select first waiter, click on Clear Lock|
| 619 | Test By Region tree, select region, click on Clear Lock|
| 620 | Test By Region tree, select namespace, click on Clear Lock|
| 621 | Test By Region tree, select pid, click on Clear Lock|
| 622 | Test By Region tree, select first waiter, click on Clear Lock|
| 623 | Test By Pids tree, select pid, click on Clear Lock|
| 624 | Test By Pids tree, select namespace, click on Clear Lock|
| 625 | Test By Pids tree, select region, click on Clear Lock|
| 626 | Test By Pids tree, select first waiter, click on Clear Lock|
| 640 | Verify that, in RO mode, clear lock button is disabled|

#### Submit messages => Terminate process

| Number | Description |
| :---:| ---      |
| 627 | Test By Namespace tree, select namespace, click on Terminate process|
| 628 | Test By Namespace tree, select region, click on Terminate process|
| 629 | Test By Namespace tree, select PID, click on Terminate process|
| 630 | Test By Namespace tree, select first waiter, click on Terminate process|
| 631 | Test By Region tree, select region, click on Terminate process|
| 632 | Test By Region tree, select namespace, click on Terminate process|
| 633 | Test By Region tree, select pid, click on Terminate process|
| 634 | Test By Region tree, select first waiter, click on Terminate process|
| 635 | Test By Pids tree, select pid, click on Terminate process|
| 636 | Test By Pids tree, select namespace, click on Terminate process|
| 637 | Test By Pids tree, select region, click on Terminate process|
| 638 | Test By Pids tree, select first waiter, click on Terminate process|
| 639 | Verify that, in RO mode, terminate process button is disabled|

#### Filtering

| Number | Description |
| :---:| ---      |
| 650 | Ensure the info bar is correct without any filter|
| 651 | Display the filter dialog |
| 652 | Display the filter dialog, type a filter and submit, verify info bar |
| 653 | Submit a filter, reset the filter and verify the info bar |
| 654 | Verify that Reset Filter button is disabled|
 

#### Globals > Tabs management

| Number | Description |
| :---:| ---      |
| 700 | Create a new tab: verify default text|
| 705 | Create 1 new tab: verify that you can switch tabs using the menu|
| 706 | Create a new tab with very long name: verify that caption gets truncated as \"settings - maxLengthCaption\" and popup is correct|
| 707 | Create a new tab with long name and change the \"settings - maxLengthCaption\" : verify that caption gets truncated as \"settings - maxLengthCaption\" and popup is correct|

#### Globals > Menu management

| Number | Description |
| :---:| ---      |
| 750 | Verify that the Last Used list gets populated with default # of items|
| 751 | Verify that the Last Used list gets populated with different # of items && more is displayed|
| 752 | Verify that the More dialog is properly populated|
| 754 | Type a valid path and verify that gets added to the list|
| 755 | Type an existing path and verify that it doesn't get added to the list|
 

#### Parser > Global

| Number | Description |
| :---:| ---      |
| 800 | Test text: ^ only: error|
| 805 | Test text: ^%v0.1 error|
| 806 | Test text: ^%a%b error|
| 807 | Test text: ^TE%ST error|
| 808 | Test text: ^a123456789012345678901234567890abc error|
| 809 | Test text: ^a123456789012345678901234567890 good|
| 810 | Test text: ^AA$% error|
| 811 | Test text: ^a@ error|
| 812 | Test text: ^A: error|
| 813 | Test text: ^notexist error|
| 814 | Create global ^TEST Test text: ^TEST good|
| 815 | Create global ^TEST Submit text using button: ^TEST good|
| 816 | Submit text using button: ^% good|
| 817 | Submit text using button: ^%test good|

#### Parser > Parens

| Number | Description |
| :---:| ---      |
| 850 | Test text: ^( error|
| 851 | Test text: ^(( error|
| 852 | Test text: ^) error|

#### Parser > Subscripts > Commas and stars

| Number | Description |
| :---:| ---      |
| 870 | Test text: ^TEST(*) good|
| 871 | Test text: ^TEST(,*) good|
| 872 | Test text: ^TEST(,,*) good|
| 873 | Test text: ^TEST(,,,) good|
| 874 | Test text: ^TEST(,) good|
| 875 | Test text: ^TEST(*,23) error|
| 924 | Test text: ^TEST(23 bad|

#### Parser > Subscripts > Numeric

| Number | Description |
| :---:| ---      |
| 900 | Test text: ^TEST(100) good|
| 901 | Test text: ^TEST(100,100) good|
| 902 | Test text: ^TEST(-100) good|
| 903 | Test text: ^TEST(100.23) good|
| 904 | Test text: ^TEST(.100) good|
| 905 | Test text: ^TEST(100e24) good|
| 906 | Test text: ^TEST(100E24) good|
| 907 | Test text: ^TEST(-100.2) good|
| 908 | Test text: ^TEST(-.100) good|
| 909 | Test text: ^TEST(100,) good|
| 910 | Test text: ^TEST(100,*) good|
| 911 | Test text: ^TEST(100,) good|
| 912 | Test text: ^TEST(100,23) good|
| 913 | Test text: ^TEST(100,45,,,) good|
| 914 | Test text: ^TEST(100,23,*) good|
| 915 | Test text: ^TEST(100,,23) good|
| 916 | Test text: ^TEST(100,,23,*) good|
| 917 | Test text: ^TEST(23e12e12) bad|
| 918 | Test text: ^TEST(23e) bad|
| 919 | Test text: ^TEST(23.12.34) bad|
| 920 | Test text: ^TEST(23.12.) bad|
| 921 | Test text: ^TEST(2312-3) bad|
| 922 | Test text: ^TEST(23Ee12e12) bad|
| 923 | Test text: ^TEST(23E) bad|
| 924 | Test text: ^TEST(23 bad|

#### Parser > Subscripts > String

| Number | Description |
| :---:| ---      |
| 950 | Test text: ^TEST(\"100\") good|
| 951 | Test text: ^TEST(\"abc\") good|
| 952 | Test text: ^TEST(\"1\"0\") error|
| 953 | Test text: ^TEST(\"a\"c\"100\") error|
| 954 | Test text: ^TEST(\"%$#^%^&*\") good|
| 955 | Test text: ^TEST(\"100\",*) good|
| 956 | Test text: ^TEST(\"100\",\"test\") good|
| 957 | Test text: ^TEST(\"100\",,\"test\") good|
| 958 | Test text: ^TEST(\"100\",,,\"test\") good|
| 959 | Test text: ^TEST(\"100\",,,\"test\",*) good|
| 960 | Test text: ^TEST(\"abc bad|
| 961 | Test text: ^TEST(\"abc) error|
| 962 | Test text: ^TEST(\"abc:\") good|

#### Parser > Subscripts > Ranges

| Number | Description |
| :---:| ---      |
| 1000 | Test text: ^TEST(:,:,*) good|
| 1001 | Test text: ^TEST(:23) good|
| 1002 | Test text: ^TEST(:\"abc\") good|
| 1003 | Test text: ^TEST(123:) good|
| 1004 | Test text: ^TEST(:,:,\"abc\":) good|
| 1005 | Test text: ^TEST(:,:,*) good|
| 1006 | Test text: ^TEST(\"abc\":\"def\",23,\"B\",*) good|
| 1007 | Test text: ^TEST(123:456,23,\"B\",*) good|
| 1008 | Test text: ^TEST(123:\"abc\",23,\"B\",*) good|
| 1009 | Test text: ^TEST(\"abc\":456,23,\"B\",*) good|
| 1010 | Test text: ^TEST(12:23,56:48,23,\"B\",*) good|
| 1011 | Test text: ^TEST(\"abc\":\"def\",12:34,23,\"B\",*) good|
| 1012 | Test text: ^TEST(:233,\"a\":,23,\"B\",*) good|
| 1013 | Test text: ^TEST(:233,*) good|
| 1014 | Test text: ^TEST(233:,*) good|
| 1015 | Test text: ^TEST(233:,:,*) good|
| 1016 | Test text: ^TEST(233:,:33,*) good|

#### Parser > Verify payload

| Number | Description |
| :---:| ---      |
| 1050 | Test text: ^TEST|
| 1051 | Test text: ^TEST(*)|
| 1052 | Test text: ^TEST(,)|
| 1053 | Test text: ^TEST(,*)|
| 1054 | Test text: ^TEST(12,,23,*)|
| 1055 | Test text: ^TEST(12,,,*)|
| 1056 | Test text: ^TEST(12,\"test\",,*)|
| 1057 | Test text: ^TEST(12:,:33,,\"a\",*)|
| 1058 | Test text: ^TEST(\"test\":\"zzz\",*)|
| 1059 | Test text: ^TEST(\"test\":\"zzz\",,*)|
| 1060 | Test text: ^TEST(100,23456,0)|
| 1061 | Test text: ^TEST(100,,23456,,0)|
| 1062 | Test text: ^TEST(:100,23456:,0)|
| 1063 | Test text: ^TEST(100,,23456,,0,*)|
| 1064 | Test text: ^TEST(,0)|
| 1065 | Test text: ^TEST(,0,*)|
 

#### Reorg

| Number | Description |
| :---:| ---      |
| 1100 | display dialog, change settings, close and reload again, verify default values are used|
| 1101 | Select the DEFAULT region, switch to Reclaim space and select the MM region, should display a message and set the switch off|
| 1102 | Select all regions and switch to Reclaim space, should display a message and set the switch off|
| 1104 | Deselect all regions, OK button should be disabled|
| 1005 | Change the range in the fillFactor, label should change|
| 1006 | Change the range in the indexFillFactor, label should change|
| 1007 | Select reclaim space, verify that range is visible, change the range in the Minimum amount for the reclamation, label should change|
| 1108 | Submit an operation. It should display an Inputpbox. Clicking cancel it should close it|
| 1109 | Reports on YDBJNLF and YDBAIM: at first it should display \"No globals\" on all regions|
| 1110 | Generate a report on DEFAULT, it should display values|
| 1111 | Generate a report on DEFAULT, generate a report, click on expand button and analyze the content|
| 1112 | set pendingReorg flag to true on DEFAULT, it should display the Pending dialog|
| 1113 | set pendingReorg flag to true on DEFAULT, the dialog should display a list with ONLY the faulty region|
| 1114 | set pendingReorg flag to true on DEFAULT, select NO, it should display a standard REORG dialog|
| 1115 | set pendingReorg flag to true on DEFAULT, select 'YES', it should display a reduced form with only the selected region, the resume switch and the background switch|
| 1116 | Generate a report on `DEFAULT`with recover space, it should display values|
| 1117 | Ensure \"Run background\" is reset after dialog close / open|
| 1118 | Submit using \"Run background\": message box should display extra text|
| 1119 | Run using \"Run background\": toaster should appear|
| 1120 | Run using \"Run background\": click on toast, should display result dialog|
| 1130 | RO mode: Menu entry should be disabled|
 

#### Globals > Table: general

| Number | Description |
| :---:| ---      |
| 1200 | Submit a query to ^ORD and verify that the record size is correct|
| 1201 | Data view: regular: Submit a query to ^ORD and verify that data is correct|
| 1202 | Data view: pieces: Submit a query to ^ORD and verify that data is correct|
| 1204 | Data view: pieces: Change piece char, submit a query to ^ORD and verify that data is correct|
| 1205 | Data view: pills: Change piece char, submit a query to ^ORD and verify that data is correct|
| 1206 | Data view: force the data size to 16 chars verify that data length gets trimmed|
| 1207 | Data view: force the data size to 16 chars verify that a button is present|
| 1208 | switch to dark mode and verify that table changes appearance|
| 1209 | Zoom in and verify that font is bigger|
| 1210 | Zoom out and verify that font is smaller|
| 1211 | Verify that the default bookmark color is loaded|
| 1212 | Verify that the default data view is loaded|
| 1213 | Verify that the default piece char is loaded|
| 1214 | Verify that the default theme is loaded|
| 1215 | View: pieces. Display ^testglobal, verify that the first entry data is set to $h|
| 1216 | View: pieces. Display ^testglobal, verify that the 2nd entry data is set to $zh|
| 1217 | View: pieces. Display ^testglobal, verify that the 3rd entry data has piece 4 set to $H|
| 1218 | View: pieces. Display ^testglobal, verify that the 4th entry data has piece 4 set to $ZH|
| 1219 | View: pieces. Display ^testglobal, verify that the 5th entry subscript 2 is set to $H|
| 1220 | View: pieces. Display ^testglobal, verify that the 6th entry subscript 2 is set to $ZH|
| 1221 | View: pieces. Display ^testglobal, verify that the 7th entry subscript 4 is set to $ZH|
| 1222 | View: pills. Display ^testglobal, verify that the 1st entry data is set to $h|
| 1223 | View: pills. Display ^testglobal, verify that the 2nd entry data is set to $zh|
| 1224 | View: pills. Display ^testglobal, verify that the 3rd entry data has piece 4 set to $H|
| 1225 | View: pills. Display ^testglobal, verify that the 4th entry data has piece 4 set to $ZH|
| 1226 | View: pills. Display ^testglobal, verify that the 5th entry subscript 2 is set to $H|
| 1227 | View: pills. Display ^testglobal, verify that the 6th entry subscript 2 is set to $ZH|
| 1228 | View: pills. Display ^testglobal, verify that the 7th entry subscript 4 is set to $ZH|
| 1229 | View: pills. Display ^testglobal, verify that the 8th entry has a GTM+1|
| 1230 | View: pills. Display ^testglobal, verify that the 9th entry has a GTM-1|
| 1231 | View: pills. Display ^testglobal, verify that the 10th isNOT recognized as $h|
| 1232 | View: plain. Display ^testglobal, verify that the 1st entry data is set to $h|
| 1233 | View: plain. Display ^testglobal, verify that the 10th is NOT set to $H|
| 1234 | View: plain. Display ^testglobal, verify that the 11th is a json|
| 1235 | View: pieces. Display ^testglobal, verify that the 11th is a json|
| 1236 | View: pieces. Display ^testglobal, verify that the 12th is a json|
| 1237 | View: pieces. Display ^testglobal, verify that the 13th is a json|
| 1238 | View: pills. Display ^testglobal, verify that the 11th is a json|
| 1239 | View: pills. Display ^testglobal, verify that the 12th is a json|
| 1240 | View: pills. Display ^testglobal, verify that the 13th is a json|
| 1241 | UTF8: try to read binary data in ^%ydboctoocto, should NOT return an error in both M and UTF8|
| 1242 | UTF8: try to read utf8 data, should be ok|

#### Globals > Table: reverse fetches

| Number | Description |
| :---:| ---      |
| 1245 | use ^PSNDF, jump to end with CTRL-END, verify|
| 1246 | use ^PSNDF, jump to end with CTRL-END, then CTRL-HOME, verify|
| 1247 | use ^PSNDF, jump to end with CTRL-END, then PG-UP until it scrolls, verify|
| 1248 | use ^PSNDF, jump to end with CTRL-END, then PG-UP until it scrolls, verify, then UP to scroll again, verify|
| 1249 | use ^PSNDF, jump to end with CTRL-END, then PG-UP until it scrolls, verify, then DOWN to scroll down , verify|
| 1250 | use ^PSNDF, jump to end with CTRL-END, then CTRL-HOME, verify, then PAGE-DOWN until it scrolls, verify|
| 1251 | use ^PSNDF(50.68,*), jump to end with CTRL-END, verify|
| 1252 | create a global with negative subscripts, verify, jump to end with CTRL-END, verify|

#### Globals > Table: fetches

| Number | Description |
| :---:| ---      |
| 1295 | Set the max buffer to 100, initial fetch to 50: verify that data size = 50|
| 1296 | Set the max buffer to 100, initial fetch to 50 and lazy to 25: verify that, scrolling down, it is fetching|
| 1297 | Set the max buffer to 100, initial fetch to 50 and lazy to 25: verify that, scrolling down after buffer is full , it is fetching and beheading the buffer|
| 1298 | Set the max buffer to 100, initial fetch to 50 and lazy to 25: verify that, scrolling down after buffer is full and up again, it is fetching and betailing the buffer|
 

#### Globals > Editor: first tab: Global save to global

| Number | Description |
| :---:| ---      |
| 1300 | Change ALL Data buffer parameters, apply them to the current tab and inspect the class to verify|
| 1301 | Change the Last Used Count parameter, apply and verify that the menu size has changed|
| 1302 | Change the default: Piece char, apply and verify that the new tab gets the new value|
| 1303 | Change the default: Piece View, apply and verify that the new tab gets the new value|
| 1304 | Change the default: Theme, apply and verify that the new tab gets the new value|
| 1305 | Change the default: Bookmark color, apply and verify that the new tab gets the new value|

#### Globals > Editor: first tab: Path bar as global

| Number | Description |
| :---:| ---      |
| 1310 | Change the path bar: Global, apply and verify that the new tab gets the new value|
 

#### Menus

| Number | Description |
| :---:| ---      |
| 1500 | Select menu and verify that routines selector is displayed|

#### Routines select

| Number | Description |
| :---:| ---      |
| 1530 | Verify that query submit button is disabled|
| 1531 | Verify that open button is disabled|
| 1532 | Type: a and verify that submit is disabled|
| 1533 | Type: % and verify that submit is disabled|
| 1534 | Type: * and verify that submit is disabled|
| 1535 | Type: a* and verify that submit is disabled|
| 1536 | Type: %* and verify that submit is disabled|
| 1537 | Type: %y* and verify that submit is enabled, but open is disabled|
| 1538 | Type: %y* and verify that list is populated correctly|
| 1539 | Type: norou* and verify that list returns: No routines found...|
| 1540 | Type: %y*  and select one item, submit and verify that open is enabled|
| 1541 | Type: %y* and select two items, submit and verify that open is enabled|
| 1542 | Type: %y*  and select one item, submit and verify that one tab gets created|
| 1543 | Type: %y* and select two items, submit and verify that two tabs gets created|
 

#### Integ

| Number | Description |
| :---:| ---      |
| 1600 | RO mode: Menu entry should be disabled|
| 1601 | Display dialog: ok button should be disabled|
| 1602 | Display dialog: select region, ok button should be enabled|
| 1603 | Display dialog: select region, deselect, ok button should be disabled|
| 1604 | Select type: fast'. Reporting detailed` should be disabled|
| 1605 | Select type: fast and then complete. Reporting detailed should be enabled|
| 1606 | select Advanced option, options should be visible|
| 1607 | select Advanced option twice, options should be hidden|
| 1608 | change all options on dialog, close and open up again, everything should be reset|
| 1609 | Select DEFAULT region, press Ok, inputbox should be displayed|
| 1610 | Select DEFAULT region, press Ok, inputbox should be displayed, press cancel, should close|
| 1611 | Select DEFAULT region, press Ok, inputbox should be displayed, press Ok, should generate report complete, summary|
| 1612 | Ensure \"Run background\" is reset after dialog close / open|
| 1613 | Submit using \"Run background\": message box should display extra text|
| 1614 | Run using \"Run background\": toaster should appear|
| 1615 | Run using \"Run background\": click on toast, should display result dialog|
| 1616 | Select DEFAULT region, press Ok, inputbox should be displayed, press Ok, should generate report complete, summary|

#### Backup

| Number | Description |
| :---:| ---      |
| 1650 | Display dialog, ensure all default values are correct|
| 1651 | Change all values, close dialog, reopen it and expect all default values correct|
| 1652 | Select DEFAULT, Ok button should be enabled|
| 1653 | Enter / as target directory, Validate should be enabled|
| 1654 | Enter / as target directory, press validate, it should be valid|
| 1655 | Enter / as target directory, press validate, ok button should be disabled|
| 1656 | Enter / as target directory, press validate, select DEFAULT, ok button should be enabled|
| 1658 | Select Create New Journal Files: Yes, link,  SyncIO should be enabled|
| 1659 | Select Create New Journal Files: Yes, nolink,  SyncIO should be enabled|
| 1660 | Select Create New Journal Files: No,  SyncIO should be disabled|
| 1661 | Mark DEFAULT as repl and clear repl env vars, select DEFAULT, msgbox should appear|
| 1662 | Mark DEFAULT as repl, select DEFAULT, msgbox should appear|
| 1663 | Mark DEFAULT as repl, create file, select DEFAULT, repl options should be visible|
| 1664 | Mark DEFAULT as repl, create file, select DEFAULT, validate path, repl options should be visible and ok button should be enabled|
| 1665 | Mark DEFAULT as repl, create file, select DEFAULT, uncheck Backup Replication Instance, other repl options should be invisible|
| 1666 | Mark DEFAULT as repl, create file, select DEFAULT, uncheck Backup Replication Instance, ok button should be enabled|
| 1667 | Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, extra fields should be visible|
| 1668 | Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, ok button should be enabled|
| 1669 | Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, validate should be disabled, type in / and validate should be enabled|
| 1670 | Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, validate should be disabled, type in /, validate: it should validate ok|
| 1671 | Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, validate should be disabled, type in /, validate: ok button should be enabled|
| 1672 | Select DEFAULT, enter / in path and validate, submit, msgbox should appear|
| 1673 | Select DEFAULT, enter / in path and validate, submit, msgbox should appear, cancel, should close|
| 1674 | Select DEFAULT, enter / in path and validate, submit, msgbox should appear, submit, should display the result dialog|
| 1675 | Select DEFAULT, enter / in path and validate, submit, msgbox should appear, submit, should check the result dialog for correctness|
| 1676 | click on ..., it should display the Dir Select dialog and have the path set to /|
| 1677 | type $ydb_dir, click on ..., it should display the Dir Select dialog and have the path set to /data|
| 1678 | type $ydb_dir, click on validate, should be ok|
| 1679 | type /notexist, click on validate, should be error|
| 1680 | type /YDBGUI/wwwroot/index.html, validate, should return error|
| 1681 | RO mode, should be disabled|
| 1682 | Ensure \"Run background\" is reset after dialog close / open|
| 1683 | Submit using \"Run background\": message box should display extra text|
| 1684 | Run using \"Run background\": toaster should appear|
| 1685 | Run using \"Run background\": click on toast, should display result dialog|
 

#### Journal files

| Number | Description |
| :---:| ---      |
| 1700 | Display the dialog, it should list only the active journal file|
| 1701 | Switch journal files, it should list the active and the extra files in the chain|
| 1702 | Break the journal chain, it should display the active and the orphans|
| 1703 | Fill up the journal to trigger a switch, it should display the active file, the extra journal in the chain and the orphans|
| 1704 | Click on View details..., it should display the journal details dialog|
| 1705 | Click on View details..., ensure all entries are correct|
| 1706 | Ensure total disk space is displayed|

#### Switch Journal File

| Number | Description |
| :---:| ---      |
| 1720 | Click on the `Switch Journal` button, ensure inputbox is displayed|
| 1721 | Click on the Switch Journal button, cancel should close it|
| 1722 | Click on the Switch Journal button, select Yes, it should close it|
 

#### Toaster

| Number | Description |
| :---:| ---      |
| 1750 | Submit a REORG, check if the Async pill is visible, wait for reorg to complete, should disappear|
 

#### searchPath

| Number | Description |
| :---:| ---      |
| 1760 | Display the dialog|
| 1761 | Ensure that the tree is properly populated|
| 1762 | Ensure first item is selected: check status of all buttons|
| 1763 | Select source path: check status of all buttons|
| 1764 | Select System SO: check status of all buttons|
| 1765 | Select first entry (OBJ): move down: confirm|
| 1766 | Select second entry (SRC): move down: confirm|
| 1767 | Select 2nd object: move up: confirm|
| 1768 | Select 2nd item (first source): DELETE: confirm only source is deleted|
| 1769 | Select 4th item (2nd object): DELETE: confirm both obj and source are deleted|
| 1770 | Display dialog, select Add object path, should display dialog with Auto-relink option visible, Validate disabled, ok disabled and ... enabled|
| 1771 | Display dialog, select Add object path, type / as path and type validate, should validate ok and ok enabled|
| 1772 | Display dialog, select Add object path, type / as path and type validate, should validate ok and ok enabled, then type /123, ok should be disabled|
| 1773 | Display dialog, select Add object path, type /123 and validate, should display msgbox|
| 1774 | Display dialog, select Add object path, type /123 and ..., should display msgbox|
| 1775 | Display dialog, select Add object path, type / and ..., should display dir selector|
| 1776 | Display dialog, select Add object path, press ..., should display dir selector and default to /|
| 1777 | Display dialog, select Add object path, type / and validate, type ok, verify it has been added|
| 1778 | Display dialog, select Add object path, type / and validate, check the Auto-relink, type ok, verify it has been added with the * after the path|
| 1779 | Display dialog, select Add object path, type /YDBGUI/objects and validate, should display a message saying it already exists|
| 1780 | Display dialog, select Add source path, should display dialog with Validate disabled, ok disabled and ... enabled and no Auto-relink visible|
| 1781 | Display dialog, select Add source path, type / and validate, type ok, verify it has been added|
| 1782 | Display dialog, select Add source path, type /YDBGUI/routines and validate, type ok, should display a message saying that it already exists|
| 1783 | Display dialog, select Add library, should display dialog with Validate disabled, ok disabled and ... enabled|
| 1784 | Display dialog, select Add library, type /, click validate, should display a dialog saying that it is not a file|
| 1785 | Display dialog, select Add library, type /, click ..., should display the fileSelect dialog|
| 1786 | Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, should validate ok|
| 1787 | Display dialog, select Add library, type '/YDBGUI/package.json', validate, should return message that it is not an .so file|
| 1788 | Display dialog, select Add library, type '/', validate, should return message that it is not an .so file|
| 1789 | Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK and verify that has been added|
| 1790 | Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK. Verify that buttons are correct|
| 1791 | Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK. Move it up|
| 1792 | Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK. Move it down once|
| 1793 | Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK. Move it down until the System SO, button down should be disabled|
| 1794 | Display dialog, select Add library, type '/YDBGUI/package.json', type ..., should display the dialog with path set to /YDBGUI|
| 1795 | Perform one add, then delete, then select ok: should display alert message: Nothing changed|
| 1796 | Add one object path, submit and check the inputbox text for correctness|
| 1797 | Add one object path, submit and check the inputbox text for correctness, re-open the window and confirm that the new path is displayed|
| 1798 | Create a routine bbb.m in /, add the path, perform a routine search, it should be found|
| 1799 | Create a routine bb.m in /, compile it as object in /, add the object, perform a routine search, it should be found, but with no source|
| 1800 | Create a routine bbb.m in /, compile it as object with the -emebed_source switch in /, add the object, perform a routine search, it should be found and ready to edit|
| 1801 | add the object dir '/' (as 2nd entry), copy the object bbb.o in /YDBGUI/objects, perform a routine search, it should be found in /YDBGUI/objects|
| 1802 | add the object dir '/' (as 1st entry), copy the object bbb.o in /YDBGUI/objects, perform a routine search, it should be found in /|
| 1803 | Display dialog, select Add object and source path, type / and validate, type ok, verify it has been added|
| 1804 | Display dialog, select Add object and source path, type / and validate, type ok, verify all buttons|
 

#### Global mappings

| Number | Description |
| :---:| ---      |
| 1820 | display dialog, ensure list is correct|
| 1821 | display dialog, ensure list contains *|
 

#### PERMALINKS: global links

| Number | Description |
| :---:| ---      |
| 1900 | append ?P=, should work fine|
| 1901 | append ?testing, should work fine|
| 1902 | append ?P=fsdffsdfsdfsd, should return the message: The permalink is not valid...|
| 1903 | append ?P=Z2xvYmFsfF5QU05ERkBA, should work and display the ^PSNDF global|
| 1904 | append ?P=Z2xvYmFsfF5QU05ERig1MC42LDUsKilAQA==, should work and display the ^PSNDF(50.6,5,*) global|
| 1905 | append ?P=Z2xvYmFsfF5QU05ERkB, should return the message: The permalink is not valid...|
| 1906 | append ?P=Z2xvYmFsfF5QU05ERkA, should return the message: The permalink is not valid...|
| 1907 | append ?P=Z2xvYmFsfF5QU05ERBA, should return the message: The permalink is not valid...|
| 1908 | append ?P=2xvYmFsfF5QU05ERkBA, should return the message: The permalink is not valid...|
| 1909 | append ?P=ZxvYmFsfF5QU05ERBA, should return the message: The permalink is not valid...|
| 1910 | append ?P=ZxvYfF5QU05ERBA, should return the message: The permalink is not valid...|

#### PERMALINKS: routine links

| Number | Description |
| :---:| ---      |
| 1911 | append ?P=cm91dGluZXwleWRiZ3VpQEA=, should display the %ydbgui routine|
| 1912 | append ?P=cm91dGluZXwlZ29AQA==, should display the %go routine|
| 1913 | append ?P=cm91dGluZXwlZ29AQA==, should display the %go routine|

#### PERMALINKS: UI

| Number | Description |
| :---:| ---      |
| 1930 | Select the global ^PSNDF, click on icon and verify the permalink|
| 1931 | Select the global ^PSNDF(50.6,0), click on icon and verify the permalink|
| 1932 | Select the routine %ydbgui, click on icon and verify the permalink|
| 1933 | Modify the zroutines, select the routine %go, click on icon and verify the permalink|
 

#### Region View: Globals tab

| Number | Description |
| :---:| ---      |
| 1950 | Open the dialog, select scan and close. Open again and verify that dialog display default option and param|
| 1951 | Open the dialog, select Importance sampling, verify that param is set to 1000|
| 1952 | Open the dialog, select Scan, verify that param is set to 0|
| 1953 | Open the dialog, execute using default values, verify that table gets populated|
| 1954 | Open the dialog, execute using default values, verify that sort headers are correctly colored|
| 1955 | Open the dialog, execute using default values, verify that data is correctly sorted|
| 1956 | Open the dialog, execute using default values, sort by name, verify that data is correctly sorted|
| 1957 | Open the dialog, execute using default values, sort by name twice, verify that data is correctly sorted|
| 1958 | Open the dialog, execute using default values, sort by status, verify that data is correctly sorted|
| 1959 | Open the dialog, execute using default values, sort by status twice, verify that data is correctly sorted|
| 1960 | Open the dialog, execute using default values, sort by size blocks, verify that data is correctly sorted|
| 1961 | Open the dialog, execute using default values, sort by size blocks twice, verify that data is correctly sorted|
| 1962 | Open the dialog, execute using default values, sort by size bytes, verify that data is correctly sorted|
| 1963 | Open the dialog, execute using default values, sort by size bytes twice, verify that data is correctly sorted|
| 1964 | Open the dialog, execute using default values, click the refresh button, verify that table is reset|
| 1965 | Open the dialog, execute using default values, click the refresh button, verify that table is reset, fetch size again, verify that default sort order and coloring is correct|

#### Region View: Globals: Tree map

| Number | Description |
| :---:| ---      |
| 1980 | Open DEFAULT Region View, select Regions tab, Tree map... button should be disabled|
| 1981 | Open DEFAULT Region View, select Regions tab, compute space, Tree map... button should be enabled|
| 1982 | Open DEFAULT Region View, select Regions tab, compute space, Tree map... button should be enabled, close Region view, open it again, select Regions tab, Tree map button should be disabled|
| 1983 | Open DEFAULT Region View, select Regions tab, compute space, press Tree map... button, dialog should be displayed|
| 1984 | Open DEFAULT Region View, select Regions tab, compute space, press Tree map... button, dialog should be displayed, input should have max = 12|
| 1985 | Open YDBOCTO Region View, select Regions tab, compute space, press Tree map... button, dialog should be displayed, input should have max = 2|
 

#### Node detection

| Number | Description |
| :---:| ---      |
| 2200 | if missing, display msgbox|
| 2201 | if ok, just display the tab|
 

#### Stats: Sources: Add

| Number | Description |
| :---:| ---      |
| 2250 | YGBLSTAT: Should display dialog|
| 2251 | FHEAD: Should display dialog|
| 2252 | PEEKBYNAME: Should display dialog|

#### Stats: Sources: Edit

| Number | Description |
| :---:| ---      |
| 2254 | YGBLSTAT: Select entry, then edit, should display dialog|
| 2255 | YGBLSTAT: Double-click selection, should display dialog|
| 2256 | FHEAD: Load test 9, select entry, then edit, should display dialog|
| 2257 | FHEAD: Load test 9, double-click selection, should display dialog|
| 2258 | PEEKBYNAME: Load test 10, select entry, then edit, should display dialog|
| 2259 | PEEKBYNAME: Load test 9, double-click selection, should display dialog|

#### Stats: Sources: Delete

| Number | Description |
| :---:| ---      |
| 2260 | Select with no selection, should display msgbox|
| 2261 | Select entry, then delete, should delete it|

#### Stats: Sources: Clear all

| Number | Description |
| :---:| ---      |
| 2265 | Create 2 entries, then clear all, list should be empty|

#### Stats: Sources: Ok button

| Number | Description |
| :---:| ---      |
| 2266 | Delete entry, click Cancel, reopen it, should not have deleted it|
| 2267 | Delete entry, click ok, reopen it, should have deleted it|
| 2270 | create item, disable it, close dialog, reopen, confirm|

#### Stats: PIDs

| Number | Description |
| :---:| ---      |
| 2271 | verify that PIds list is empty|
| 2272 | verify that PIds list is empty, run test program, refresh, verify it gets populated|
| 2272 | verify that PIds list is empty, run test program, refresh, verify it gets populated|
| 2274 | select samples, PIDS, no pid selected, press OK, expect msgbox|
| 2275 | select samples, run test program, PIDS select PIDs, press ok, expect NO msgbox|
| 2276 | Create source with aggr, edit with PIDs, click ok, should save|
| 2277 | Load report 1, edit first source, select PIDs, click ok, expect inputbox|
| 2278 | start program, create source with PIDs, restart test program, edit again, verify PIDs gets deselected|

#### Stats: ygbl dialog: Add mode

| Number | Description |
| :---:| ---      |
| 2280 | click ok, should display msg asking for samples|
| 2282 | select samples, select region, set sample rate to 0, click ok, should display msgbox asking for sample rate|
| 2283 | select samples, select region, click ok, should close and add the entry|
| 2284 | select samples, select region, select processes top, click ok, should display msg asking for # of processes|
| 2286 | select samples, select region, select processes top, #, click ok, should close and add the entry sample name|
| 2283 | select samples, select region, click ok, should close and add the entry|
| 2287 | select samples, select region, click cancel, should close and NOT add the entry|

#### Stats: ygbl dialog: Edit mode

| Number | Description |
| :---:| ---      |
| 2290 | Should populate list with all data correctly|
| 2291 | edit samples, click ok, verify|
| 2292 | edit regions, click ok, verify|
| 2293 | edit processes, click ok, verify|
| 2294 | edit sample rate, click ok, verify|
| 2295 | edit sample rate, click ok, verify|
| 2296 | edit regions, click ok, inputbox should appear, click yes, verify|
| 2297 | load report 1, edit samples [add MLK], click ok, inputbox should appear, should have no warnings, click yes, verify|
| 2298 | load report 1, edit samples [remove SET], click ok, inputbox should appear, should have no warnings, click yes, verify|
 

#### Stats: Sparkchart: Editor

| Number | Description |
| :---:| ---      |
| 2305 | open when no sources, msgbox should be displayed|
| 2306 | open with disabled source, msgbox should be displayed|
| 2307 | open with one valid source, verify population|
| 2308 | open with one valid source and one disabled, verify population|
| 2309 | open sparkchart with no views, click ok, should display msgbox|
| 2310 | add report line, verify|
| 231 | add report line, click ok, should display msgbox|
| 2312 | add report line, delete, verify|
| 2313 | add graph, verify|
| 2314 | add graph, click ok, should close|
| 2315 | add graph, delete, verify|
| 2316 | add 2 graphs, verify|
| 2317 | add 3 graphs, should display a msgbox|
| 2318 | add 2 graphs, click ok, go to source, disable source, create new source, add graph, should be ok|

#### Stats: Sparkchart: Mapping

| Number | Description |
| :---:| ---      |
| 2322 | Create new report line, mapping should be properly populated and have no selections|
| 2323 | Create new report line, select individuals, click cancel, reopen, should be empty|
| 2324 | Create new report line, select individuals, click ok, reopen, should be populated|
| 2325 | Create new report line, select individuals, click ok, close Spark chart with ok, reopen, reopen mapping, selection should be there|
| 2326 | Create new report line, select individuals, click ok, close Spark chart with cancel, reopen, reopen mapping, no selection should be there|
| 2327 | Create new report line, select all with header 1, verify|
| 2328 | Create new report line, select all with header 1, then deselect all, verify|
| 2329 | Create new report line, select all with header 2, verify|
| 2330 | Create new report line, select all with header 2, then deselect all, verify|
| 2331 | Create new report line, select all with header 3, verify|
| 2332 | Create new report line, select all with header 3, then deselect all, verify|
| 2333 | Create new report line, select all with header 4, verify|
| 2334 | Create new report line, select all with header 4, then deselect all, verify|
| 2335 | Create new report line, select all with header 4, verify|
| 2336 | Create new report line, select all with header 4, then deselect all, verify|
| 2337 | Create new report line, select one entry, then select all, verify|
| 2338 | Create new report line, select one entry, then deselect all, verify|
| 2339 | Create source with 2 regions, open spark, create graph, select mapping, should have region combo displayed|
| 2340 | Create source with 2 regions, open spark, create graph, select mapping, should have region combo displayed, close, select normal mapping, should NOT have region combo displayed|
 

#### Stats: Tab management

| Number | Description |
| :---:| ---      |
| 2210 | Open tab, verify connection|
| 2211 | Open tab, verify no second tab can be open|
| 2212 | Open tab, close tab, verify tab can be opened again|
| 2213 | Open tab, verify no name and brackets are displayed|
| 2214 | open a file, verify name is displayed in brackets|
| 2215 | open a file, close a file, verify name AND brackets are gone|
| 2216 | open a file, modify it, verify that dirty * appears|
| 2217 | open a file, modify it, save it, verify that dirty * disappears|

#### Stats: Storage management: New

| Number | Description |
| :---:| ---      |
| 2220 | Verify inputbox is displayed|
| 2221 | If prev doc is dirty, verify double msg box is displayed|
| 2222 | load report, clear report verify it gets deleted|

#### Stats: Storage management: Save

| Number | Description |
| :---:| ---      |
| 2226 | Click on icon, expect dialog to show|
| 2227 | Type no name, no description, save|
| 2228 | Type a name, no description, save|
| 2229 | Type a name, with description, save|
| 2230 | Type an existing name, save, expect overwrite dialog|
| 2231 | Load existing, perform a change, verify dirty flag, click save, type a name, save, overwrite, dirty flag should disappear|

#### Stats: Storage management: Load

| Number | Description |
| :---:| ---      |
| 2235 | Open dialog, verify list is populated, select item, verify description is displayed|
| 2236 | Load report, make it dirty, open dialog, select and load, verify save dirty dialog appears|

#### Stats: Storage management: Delete

| Number | Description |
| :---:| ---      |
| 2240 | Open the load dialog, select an item, delete, expect msgbox, cancel, verify|
| 2241 | Open the load dialog, select an item, delete, expect msgbox, confirm, verify|

#### Stats: Message in main screen

| Number | Description |
| :---:| ---      |
| 2242 | Stat the stats, msg \"No view is defined\" should be visible|
| 2243 | Stat the stats, load report 1, msg \"No view is defined\" should NOT be visible|
 

#### Stats: Sparkchart: Report Line

| Number | Description |
| :---:| ---      |
| 2350 | Create report line, open directly the properties, should display input box, select no, should close|
| 2351 | Create report line, open directly the properties, should display input box, select yes, should open the mapping|
| 2352 | Ensure correct information is displayed in the header|
| 2353 | Ensure preview matches the orientation|
| 2354 | Change orientation: ensure preview matches orientation|
| 2355 | Change orientation, press cancel, open again, should have NOT saved it|
| 2356 | Change orientation, press ok, open again, should have saved it|
| 2357 | Click on top left ... button in the preview, it should display the highlights dialog|

#### Stats: Sparkchart: Report Line: Highlights

| Number | Description |
| :---:| ---      |
| 2360 |    it("Test # 2360:select a aggregate Report Line, open highlights, max of processes should be disabled|
| 2361 |    it("Test # 2361:select a aggregate Report Line, open highlights, change the option into Range, no input data, hit ok, should display msgbox|
| 2362 | select a aggregate Report Line, open highlights, change the option into Range, input only greater than, hit ok, should display msgbox|
| 2363 | select a aggregate Report Line, open highlights, change the option into Range, click cancel, verify it didn't change in the Report Line|
| 2364 | select a aggregate Report Line, open highlights, change the option into Range, click ok, verify it did change in the Report Line|
| 2365 | select a aggregate Report Line, open highlights, change the option into Range, click ok, verify verify that Report Line preview changed the cell caption|
| 2366 | select a aggregate Report Line, open highlights, change the option into Range, enter first value smaller than second, expect msgbox|
| 2367 | select a aggregate Report Line, open highlights, change the option into Range, enter first value equals to second, expect NO msgbox|
| 2368 | select a aggregate Report Line, open highlights, change the option into Range, enter first value and 0 as second value, expect NO msgbox|
| 2371 | select a max Report Line, open highlights, verify that Max of processes is ENABLED|
| 2372 | select a max Report Line, open highlights, select Max of processes, hit Cancel, verify it didn't get stored|
| 2373 | select a max Report Line, open highlights, select Max of processes, hit Ok, verify it did get stored|

#### Stats: Sparkchart: Report Line: properties

| Number | Description |
| :---:| ---      |
| 2380 | Click on properties, verify Preferences opens with new title|
| 2381 | Click on Reset properties, verify msgbox is displayed|
| 2382 | Click on Save properties as default: verifiy msgbox is displayed|
 

#### Stats: Sparkchart: Graph

| Number | Description |
| :---:| ---      |
| 2400 | Create graph, open directly the settings, should display input box, select no, should close|
| 2401 | Create graph, open directly the settings, should display input box, select yes, should open the mapping|
| 2402 | Create graph and mapping, ensure series are displayed correctly|
| 2405 | Create graph and mapping, click on Title, verify Preferences dialog opens with correct title|
| 2406 | Create graph and mapping, click on Sub title, verify Preferences dialog opens with correct title|
| 2407 | Create graph and mapping, click on Grid, verify Preferences dialog opens with correct title|
| 2408 | Create graph and mapping, click on Legend, verify Preferences dialog opens with correct title|
| 2409 | Create graph and mapping, click on Time scale, verify Preferences dialog opens with correct title|
| 2410 | Create graph and mapping, click on Y axis, verify Preferences dialog opens with correct title|
| 2412 | Create graph and mapping, click on Tooltips, verify Preferences dialog opens with correct title|
| 2430 | Click on first Series button, ensure dialog is shown with correct title|
| 2431 | Click on second Series button, ensure dialog is shown with correct title|
| 2432 | Click on Line settings, ensure Preferences is loaded with correct title|
| 2433 | Click on Y Axis, ensure Preferences is loaded with correct title|

#### Stats: Sparkchart: Graph configuration

| Number | Description |
| :---:| ---      |
| 2440 | Use report with no graphs, msgbox should be displayed|
| 2441 | Use report with one graph, msgbox should be displayed|
| 2442 | Use report with two graphs, dialog should be displayed|
| 2443 | Use report with two graphs, dialog should be displayed, ensure correct option is displayed|
| 2444 | Use report with two graphs, dialog should be displayed, change the option, click cancel, verify it is NOT saved|
| 2445 | Use report with two graphs, dialog should be displayed, change the option, click ok, verify it is saved|

#### Stats: Dirty status

| Number | Description |
| :---:| ---      |
| 2500 | Change source, click ok, verify|
| 2501 | Change spark chart, click ok, verify|
 

#### Stats: Toolbar

| Number | Description |
| :---:| ---      |
| 2550 | check all buttons status at startup|
| 2551 | check all buttons status after loading report|
| 2552 | check all buttons status after creating a source|
| 2553 | check all buttons status after creating a source and a report line w mapping|
| 2554 | check all buttons status after creating a source and a graph w mapping|
| 2555 | check all buttons status after creating a source and a report line w mapping and then disabling the source|
| 2556 | check all buttons status after loading a report, then select new to clear it|
| 2557 | load report, hit start, check other buttons enabled status|
| 2558 | load report, hit start, then pause, check other buttons enabled status|
| 2559 | load report, hit start, then stop, check other buttons enabled status|
| 2560 | load report, hit start, then start again, it should be paused|

#### Stats: Status bar for PIDs process mode

| Number | Description |
| :---:| ---      |

#### Stats: Status bar

| Number | Description |
| :---:| ---      |
| 2570 | at startup, check Status field|
| 2571 | at startup, check Sample rate field|
| 2572 | at startup, check Timestamp field|
| 2573 | at startup, check # of samples field|
| 2574 | load a report, run it, check Status field|
| 2575 | load a report, run it, check Sample rate field|
| 2576 | load a report, run it, check Timestamp field|
| 2577 | load a report, run it, check # of samples field|
| 2578 | load a report, run it, pause it, check Status field|
| 2579 | load a report, run it, pause it, check Sample rate field|
| 2580 | load a report, run it, stop it, check Status field|
| 2581 | load a report, run it, stop it, check Sample rate field|
| 2582 | load a report, run it, stop it, check Timestamp field|
| 2583 | load a report, run it, stop it, check # of samples field|
| 2584 | load a report with multiple source, multiple sample rates, verify Sample rate field|
| 2585 | load a report, clear it, verify all fields|
 

#### Stats: Rendering: Static: Layout

| Number | Description |
| :---:| ---      |
| 2600 | Load report 6, verify 1st view Header and fields (H)|
| 2601 | Load report 6, verify 1st view Header and fields (V)|
| 2602 | Load report 6, verify 2nd view Header and fields (H)|
| 2603 | Load report 6, verify 2nd view Header and fields (V)|
| 2604 | Load report 6, verify 3rd view Header and fields (H)|
| 2605 | Load report 6, verify 3rd view Header and fields (V)|
| 2606 | Load report 6, verify 4th view Header and fields (H)|
| 2607 | Load report 6, verify 4th view Header and fields (V)|
| 2608 | Load report 6, verify 5th view Header and fields (H)|
| 2609 | Load report 6, verify 5th view Header and fields (V)|

#### Stats: Rendering: Static: Data

| Number | Description |
| :---:| ---      |
| 2610 | Load report 6, verify 1st view Data (H)|
| 2611 | Load report 6, verify 1st view Data (V)|
| 2612 | Load report 6, verify 2nd view Data (H)|
| 2613 | Load report 6, verify 2nd view Data (V)|
| 2614 | Load report 6, verify 3rd view Data (H)|
| 2615 | Load report 6, verify 3rd view Data (V)|
| 2616 | Load report 6, verify 4th view Data (H)|
| 2617 | Load report 6, verify 4th view Data (V)|
| 2618 | Load report 6, verify 5th view Data (H)|
| 2619 | Load report 6, verify 5th view Data (V)|

#### Stats: Rendering: Static: Graph

| Number | Description |
| :---:| ---      |
| 2630 | Single graph|
| 2631 | Double graph V orientation|
| 2632 | Double graph H orientation|
| 2633 | No graph|
 

#### Stats: Results: General tab and filters population

| Number | Description |
| :---:| ---      |
| 2650 | Load test-1, run it without bgnd program running, stop it, it should display msgbox: There is no data to display in the result tab.|
| 2651 | Load test-3, run it without bgnd program running, stop it, it should display msgbox: There is no data to display in the result tab.|
| 2652 | Load test-8, run it without bgnd program running, stop it, it should display msgbox: There is no data to display in the result tab.|
| 2653 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, tab should be enabled|
| 2654 | Load test-3, run bgnd program, acquire 5 secs of samples, stop, tab should be enabled|
| 2655 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, select results tab, source, region and processes should be disabled, samples and values should be populated|
| 2656 | Load test-3, run bgnd program, acquire 5 secs of samples, stop, select results tab, source, and region should be disabled, processes, samples and values should be populated|
| 2657 | Load test-8, run bgnd program, acquire 5 secs of samples, stop, select results tab, region and processes should be disabled, source, samples and values should be populated|
| 2658 | Load test-8, run 2 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 2 entries|
| 2659 | Load test-8, run 3 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 3 entries|
| 2660 | Load test-8, run 4 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 4 entries|
| 2661 | Load test-8, run 5 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 5 entries|
| 2662 | Load test-8, run 6 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 5 entries|

#### Stats: Results: Headers and filters + source switching

| Number | Description |
| :---:| ---      |
| 2665 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, verify result headers|
| 2666 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, remove samples, verify result headers|
| 2667 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, remove data, verify result headers|
| 2668 | Load test-8, run bgnd program, acquire 5 secs of samples, stop, verify result headers on second source|
| 2669 | Load test-8, run bgnd program, acquire 5 secs of samples, stop, switch to second source, remove one process, verify result headers|
| 2670 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, select 2nd source, verify result headers|
| 2671 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, select 4th source, verify result headers|
 

#### Stats: Rendering: Dynamic: Layout

| Number | Description |
| :---:| ---      |
| 2700 | Load report 6, verify 2nd view ProcessId (H)|
| 2701 | Load report 6, verify 2nd view ProcessId (V)|
| 2702 | Load report 6, verify 4th view ProcessId (H)|
| 2703 | Load report 6, verify 4th view ProcessId (V)|
| 2704 | Load report 6, verify 1st view Data (H)|
| 2705 | Load report 6, verify 1st view Data (V)|
| 2706 | Load report 6, verify 2nd view Data (H)|
| 2707 | Load report 6, verify 2nd view Data (V)|
| 2708 | Load report 6, verify 3rd view Data (H)|
| 2709 | Load report 6, verify 3rd view Data (V)|
| 2710 | Load report 6, verify 4th view Data (H)|
| 2711 | Load report 6, verify 4th view Data (V)|
| 2712 | Load report 6, verify 5th view Data (H)|
| 2713 | Load report 6, verify 5th view Data (V)|
 

#### Stats: Results: Source properties

| Number | Description |
| :---:| ---      |
| 2675 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, display properties for source 1|
| 2676 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, display properties for source 2|
| 2677 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, display properties for source 3|
| 2678 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, display properties for source 4|
| 2679 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, display properties for source 1|

#### Stats: Results: Paging, page navigation

| Number | Description |
| :---:| ---      |
| 2680 | Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, check # of pages|
| 2681 | Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, check enabled status of buttons|
| 2682 | Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to page 2, check enabled status and pages caption|
| 2683 | Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to page 3 (last), navigate back to 2, check enabled status and pages caption|
| 2684 | Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to page 3 (last), navigate back to 1,, check enabled status and pages caption|
| 2685 | Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to last, check enabled status and pages caption|
| 2686 | Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to last, then first, check enabled status and pages caption|
| 2687 | Load test-8, run bgnd program, acquire 3 secs of samples, stop, display result, check # of pages|
| 2688 | Load test-8, run bgnd program, acquire 3 secs of samples, stop, display result, check enabled status of buttons|

#### Stats: Results: Zoom

| Number | Description |
| :---:| ---      |
| 2690 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, display result, zoom in, verify|
| 2691 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, display result, zoom out, verify|
| 2692 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, display result, zoom in 20 times, verify|
| 2693 | Load test-1, run bgnd program, acquire 5 secs of samples, stop, display result, zoom out 20 times, verify|

#### Stats: Results: Export dialog

| Number | Description |
| :---:| ---      |
| 2720 | Load test-8, run bgnd program, acquire 5 secs of samples, stop, display result, click export, dialog should appear|
| 2721 | Load test-8, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select header, no region, process and sample/data should appear|
| 2722 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select source 2, header should be selected, no region should appear, process and sample/data should appear|
| 2723 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select source 3, header should be selected, no process should appear, regions and sample/data should appear|
| 2724 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select source 4, header should be selected, regions, process and sample/data should appear|
| 2725 | Load test-6, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select source 4, deselect header, regions, process and sample/data should disappear|
 

#### Stats: Sources: FHEAD Add

| Number | Description |
| :---:| ---      |
| 2730 | Click ok, should display msgbox|
| 2731 | select 1 sample,click ok, should create source, verify in table|
| 2732 | select 3 samples, change region, change sample rate, click ok, verify in table|

#### Stats: Sources: FHEAD Edit

| Number | Description |
| :---:| ---      |
| 2740 | Create source, enter edit mode, change sample rate, submit, expect inputbox|
| 2741 | Create source, enter edit mode, change sample rate, submit, expect inputbox, answer YES, verify|
| 2742 | Create source, enter edit mode, change region, submit, expect inputbox|
| 2743 | Create source, enter edit mode, change region, submit, expect inputbox, answer YES, verify|
| 2744 | Create source, enter edit mode, change region and sample rate, submit, expect inputbox|
| 2745 | Create source, enter edit mode, change region and sample rate, submit, expect inputbox, answer YES, verify|
| 2746 | Create source, enter edit mode, change samples, submit, expect inputbox|
| 2747 | Create source, enter edit mode, change samples, submit, expect inputbox, answer YES, verify|
| 2748 | Create source, enter edit mode, change samples and sample rate, submit, expect inputbox|
| 2749 | Create source, enter edit mode, change samples and sample rate, submit, expect inputbox, answer YES, verify|
| 2750 | Create source, enter edit mode, change samples and region, submit, expect inputbox|
| 2751 | Create source, enter edit mode, change samples and region, submit, expect inputbox, answer YES, verify|
| 2752 | Create source, enter edit mode, change samples, sample rate and region, submit, expect inputbox|
| 2753 | Create source, enter edit mode, change samples, sample rate and region, submit, expect inputbox, answer YES, verify|
 

#### Stats: Sources: PEEKBYNAME  Add

| Number | Description |
| :---:| ---      |
| 2770 | Click ok, should display msgbox|
| 2771 | select 1 sample,click ok, should create source, verify in table|
| 2772 | select 3 samples, change region, change sample rate, click ok, verify in table|

#### Stats: Sources: PEEKBYNAME Edit

| Number | Description |
| :---:| ---      |
| 2780 | Create source, enter edit mode, change sample rate, submit, expect inputbox|
| 2781 | Create source, enter edit mode, change sample rate, submit, expect inputbox, answer YES, verify|
| 2782 | Create source, enter edit mode, change region, submit, expect inputbox|
| 2783 | Create source, enter edit mode, change region, submit, expect inputbox, answer YES, verify|
| 2784 | Create source, enter edit mode, change region and sample rate, submit, expect inputbox|
| 2785 | Create source, enter edit mode, change region and sample rate, submit, expect inputbox, answer YES, verify|
| 2786 | Create source, enter edit mode, change samples, submit, expect inputbox|
| 2787 | Create source, enter edit mode, change samples, submit, expect inputbox, answer YES, verify|
| 2788 | Create source, enter edit mode, change samples and sample rate, submit, expect inputbox|
| 2789 | Create source, enter edit mode, change samples and sample rate, submit, expect inputbox, answer YES, verify|
| 2790 | Create source, enter edit mode, change samples and region, submit, expect inputbox|
| 2791 | Create source, enter edit mode, change samples and region, submit, expect inputbox, answer YES, verify|
| 2792 | Create source, enter edit mode, change samples, sample rate and region, submit, expect inputbox|
| 2793 | Create source, enter edit mode, change samples, sample rate and region, submit, expect inputbox, answer YES, verify|
 

#### Global Directory

| Number | Description |
| :---:| ---      |
| 2900 | Create the all-good.gld file, select it, open it and validate it|
| 2901 | Create the cwd.gld file, select it, open it and validate it|
| 2902 | Create the envvars.gld file, select it, open it and validate it|
| 2903 | Create the cwd-envvars.gld file, select it, open it and validate it|
| 2904 | Select bad.gld, open it and validate it|
| 2905 | Select all-good.gld, hit Set and verify, then open again, hit Reset and verify|
| 2906 | Select all-good.gld, change the cwd, hit Set and verify|
| 2907 | Select cwd.gld, verify, change the cwd to a bad one, verify again|
| 2908 | Select cwd.gld, verify, change the cwd to a good one, verify, hit Set and verify, open again, hit Reset and verify|
 

#### Global Directory

| Number | Description |
| :---:| ---      |
| 2909 | Select envvars.gld, change the empty env var to a bad one, verify again|
| 2910 | Select envvars.gld, change the empty env var to a good one, verify, hit Set and verify, open again, hit Reset and verify|
| 2911 | Select envvars.gld, change the empty env var to a good one, the existing to a bad one, verify again|
| 2912 | Select cwd-envvars.gld, change the cwd to a bad one, verify again|
| 2913 | Select cwd-envvars.gld, change the cwd to a good one, verify again|
| 2914 | Select cwd-envvars.gld, change the empty env var to a bad one, verify again|
| 2915 | Select cwd-envvars.gld, change the empty env var to a good one, verify again|
| 2916 | Select cwd-envvars.gld, change the empty env var to a bad one, the cwd to a bad one, verify again|
 

#### Global Directory

| Number | Description |
| :---:| ---      |
| 2917 | Select cwd-envvars.gld, change the empty env var to a bad one, the cwd to a good one, verify again|
| 2918 | Select cwd-envvars.gld, change the empty env var to a good one, the cwd to a bad one, verify again|
| 2919 | Select cwd-envvars.gld, change the empty env var to a good one, the cwd to a good one, verify, hit Set and verify, open again, hit Reset and verify|
| 2920 | Select cwd-envvars.gld, change the empty env var to a good one, the cwd to a good one, verify, hit Set, open again, verify overall dialog status|
| 2921 | Simply open the dialog, ensure it is correctly populated with default values|
| 2922 | Select all-good.gld, apply, reset, re-display dialog: ensure all fields are correctly populated|
 

#### Devices Tree map

| Number | Description |
| :---:| ---      |
| 2930 | Open the dialog, verify that the space object gets properly populated|
| 2931 | Open the dialog, click on Databases, By Regions should become disabled|
| 2932 | Open the dialog, click on Journals, By Regions should become disabled|
| 2933 | Open the dialog, click on Databases / By Regions, then 'Databases', By regions should become unchecked|
| 2934 | Open the dialog, click on Journals / By Regions, then 'Journals', By regions should become unchecked|
| 2935 | Open the dialog, select all checkboxes, close dialog, open it again, only Database and Journals should be selected|
 

#### Preferences: general

| Number | Description |
| :---:| ---      |
| 2950 | Open the dialog, click on Backup, should display two PATH entries in the table|
| 2951 | Open the dialog, double-click REST, click on Timeout, should display two numeric entries in the table|
| 2952 | Open the dialog, double-click Octo, click on DragAndDrop, should display one boolean entry in the table|
| 2955 | Open the dialog, select Storage / Disk space alerts, it should display the Range Percent pane with correct title|
| 2956 | Open the dialog, select Region / Journal, it should display the Range Percent pane with correct title|
| 2957 | Open the dialog, select Region / Database file / Disk space alert, it should expand to \"Default\" and a list of regions (upper case)|
| 2958 | Open the dialog, double-click REST, click on Timeout, change the Short value, click OK to save it, refresh the browser, value should be changed|
| 2961 | Open the dialog, double-click Octo, click on DragAndDrop, change the option from Yes to No, save it, refresh the browser and verify|
| 2962 | Display dialog, click ok, msgbox should appear with message: No changes|

#### Preferences: ranges

| Number | Description |
| :---:| ---      |
| 2970 | Storage: change 0 - 70 % down to 0 - 60 %. Save, refresh and test|
| 2971 | Storage: change 71 - 90 % down to 71 - 80 %. Save, refresh and test|
| 2972 | Storage: change 91 - 97 % down to 91 - 95 %. Save, refresh and test|
| 2973 | Regions / Journal: change 0 - 70 % down to 0 - 60 %. Save, refresh and test|
| 2974 | Regions / Journal: change 71 - 90 % down to 71 - 80 %. Save, refresh and test|
| 2975 | Regions / Journal: change 91 - 97 % down to 91 - 95 %. Save, refresh and test|
| 2976 | Regions /Disk alert / Default: change 0 - 70 % down to 0 - 60 %. Save, refresh and test|
| 2977 | Regions /Disk alert / Default: change 71 - 90 % down to 71 - 80 %. Save, refresh and test|
| 2978 | Regions /Disk alert / Default: change 91 - 97 % down to 91 - 95 %. Save, refresh and test|
| 2979 | Regions /Disk alert / Default: unl. to 11 ext. to 14 to 9 ext. Save, refresh and test|
| 2980 | Regions /Disk alert / Default: 10 to 6 ext. to 10 to 8 ext. Save, refresh and test|
| 2981 | Regions /Disk alert / Default: 5 to 3 ext. to 5 to 4 ext. Save, refresh and test|
| 2982 | Regions /Disk alert / Region: Default: change 0 - 70 % down to 0 - 60 %. Save, refresh and test|
| 2983 | Regions /Disk alert / Region: Default: change 71 - 90 % down to 71 - 80 %. Save, refresh and test|
| 2984 | Regions /Disk alert / Region: Default: change 91 - 97 % down to 91 - 95 %. Save, refresh and test|
 

#### REPL > Topology static > bc7 > Melbourne

| Number | Description |
| :---:| ---      |
| 3156 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc7 > Paris

| Number | Description |
| :---:| ---      |
| 3158 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc7 > rome

| Number | Description |
| :---:| ---      |
| 3160 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc7 > tokio

| Number | Description |
| :---:| ---      |
| 3162 | verify graph links and rects are correct|
 

#### REPL > Topology static model details > bc1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3200 | verify header|
| 3201 | verify role|
| 3202 | verify data access|
 

#### REPL > Topology static model details > bc2 > Paris

| Number | Description |
| :---:| ---      |
| 3210 | verify header|
| 3211 | verify role|
| 3212 | verify data access|
 

#### REPL > Topology static model details > bc7 > Rome

| Number | Description |
| :---:| ---      |
| 3225 | verify header|
| 3226 | verify role|
| 3227 | verify data access|
 

#### REPL > Topology static model details > bc2bc4 > Rome

| Number | Description |
| :---:| ---      |
| 3245 | verify header|
| 3246 | verify role|
| 3247 | verify data access|
 

#### REPL > Topology static model details > bc2si1si1 > Paris

| Number | Description |
| :---:| ---      |
| 3263 | verify Paris header|
| 3264 | verify Paris role|
| 3265 | verify Paris data access|
| 3266 | verify Rome header|
| 3267 | verify Rome role|
| 3268 | verify Rome data access|
 

#### REPL > Topology static model details > bc2si4 > Paris

| Number | Description |
| :---:| ---      |
| 3295 | verify Paris header|
| 3296 | verify Paris role|
| 3297 | verify Paris data access|
| 3298 | verify Rome header|
| 3299 | verify Rome role|
| 3300 | verify Rome data access|
 

#### REPL > Topology static model details > bc2si4 > santiago

| Number | Description |
| :---:| ---      |
| 3304 | verify amsterdam header|
| 3305 | verify amsterdam role|
| 3306 | verify amsterdam data access|
 

#### REPL > Topology UI > bc2bc1 > Context menu status

| Number | Description |
| :---:| ---      |
| 3320 | popup Melbourne, verify enabled / disabled status|
| 3321 | popup Paris, verify enabled / disabled status|
| 3322 | popup Santiago, verify enabled / disabled status|
| 3323 | popup Rome, verify enabled / disabled status|

#### REPL > Topology UI > bc2bc1 > Log dialog

| Number | Description |
| :---:| ---      |
| 3350 | select Melbourne popup, open dialog|
| 3351 | select Melbourne popup, should have two tabs only|
| 3352 | select Melbourne popup, verify they are both filled|
| 3353 | select Melbourne popup, verify dialog caption is correct|
| 3355 | select Paris popup, open dialog|
| 3356 | select Paris popup, should have 4 tabs only|
| 3357 | select Paris popup, verify they are all filled|
| 3358 | select Paris popup, verify dialog caption is correct|
| 3360 | select Santiago popup, open dialog|
| 3361 | select Santiago popup, should have 3 tabs only|
| 3362 | select Santiago popup, verify they are all filled|
| 3363 | select Santiago popup, verify dialog caption is correct|
| 3365 | select Rome popup, open dialog|
| 3366 | select Rome popup, should have 3 tabs only|
| 3367 | select Rome popup, verify they are all filled|
| 3368 | select Rome popup, verify dialog caption is correct|
| 3369 | select Rome popup, change the tail to 2 and refresh, verify list length has changed|
| 3370 | select Rome popup, change the tail to -2 and refresh, verify list length has changed|
| 3371 | select Rome popup, change the tail to -2 and refresh, change it back to 0 and verify list length has changed|

#### REPL > Topology UI > bc2bc1 > Instance file dialog

| Number | Description |
| :---:| ---      |
| 3380 | select Melbourne popup, open dialog|
| 3381 | select Melbourne popup, verify dialog caption is correct|
| 3382 | select Melbourne popup, verify header receiver group is empty|
| 3384 | select Melbourne popup, verify 2 slots are present|
| 3385 | select Melbourne popup, verify History contains 1 record|
| 3390 | select Paris popup, open dialog|
| 3391 | select Paris popup, verify dialog caption is correct|
| 3392 | select Paris popup, verify header receiver group is populated|
| 3394 | select Paris popup, verify 2 slots are present|
| 3395 | select Paris popup, verify History contains 1 record|
| 3400 | select Rome popup, open dialog|
| 3401 | select Rome popup, verify dialog caption is correct|
| 3402 | select Rome popup, verify header receiver group is populated|
| 3404 | select Rome popup, verify 1 slot are present|
| 3405 | select Rome popup, verify History contains 1 record|
| 3410 | select Santiago popup, open dialog|
| 3411 | select Santiago popup, verify dialog caption is correct|
| 3412 | select Santiago popup, verify header receiver group is populated|
| 3414 | select Santiago popup, verify 1 slot are present|
| 3415 | select Santiago popup, verify History contains 1 record|

#### REPL > Topology UI > bc2bc1 > Open GUI from popup

| Number | Description |
| :---:| ---      |
| 3420 | open Paris popup, select open GUI, verify tab gets created|
| 3421 | open Rome popup, select open GUI, verify tab gets created|
| 3422 | open Paris popup, select open GUI, verify tab gets created|
 

#### REPL > Topology UI > bc1 > Save dialog

| Number | Description |
| :---:| ---      |
| 3450 | open layout, click save, expect dialog|
| 3451 | open layout, click save, click ok, expect msgbox|
| 3452 | open layout, click save, enter name bc1_test_1, click ok, expect dialog to close|
| 3453 | save layout, close, save it again with name bc2_test_1, enter description, expect inputbox, say no, dialog should still be open|
| 3454 | save layout, close, save it again with name bc2_test_1, expect inputbox, say yes, dialog should close|
| 3455 | modify layout, save it with name bc1_test_2, mark it as autoload, close layout, reopen, verify|
| 3456 | modify layout, save it with name bc1_test_3, mark it as autoload, close layout, reopen, verify toolbar got switched|

#### REPL > Topology UI > bc1 > Load dialog

| Number | Description |
| :---:| ---      |
| 3460 | open layout, click load, expect dialog|
| 3461 | open layout, save an entry, click load, verify that list has one item|
| 3465 | open layout, create an entry, click load, click delete, expect inputbox|
| 3466 | open layout, click load, click delete, expect inputbox, choose Yes, close dialog, reopen, verify total entries|
 

#### REPL > Topology UI > bc1 > Design toolbar

| Number | Description |
| :---:| ---      |
| 3500 | Zoom in: open layout, , zoom in, verify scale() has changed|
| 3502 | Zoom out: open layout, , zoom out, verify scale() has changed|
| 3505 | Fit To Content: Open layout, modify layout, select FitToContent, verify scale() has changed|
| 3506 | Router: open layout, change router, verify|
| 3507 | Connector: open layout, change connector, verify|

#### REPL > Topology UI > bc1 > Design toolbar > Undo

| Number | Description |
| :---:| ---      |
| 3510 | Open layout, at launch it should be empty: icon disabled|
| 3511 | Open layout, make changes, so it will become enabled, close tab, open again, should be disabled again|
| 3512 | Open layout, perform two changes, click undo, verify, click again, verify again|
| 3513 | Open layout, zoom in, verify buffer is bigger|
| 3514 | Open layout, zoom out, verify buffer is bigger|
| 3515 | Open layout, change router, verify buffer is bigger|
| 3516 | Open layout, change connector, verify buffer is bigger|
| 3517 | Open layout, pop Auto-layout, change direction, verify buffer is bigger|
| 3518 | Open layout, pop Auto-layout, change rank up, verify buffer is bigger|
| 3519 | Open layout, pop Auto-layout, change rank down, verify buffer is bigger|
| 3520 | Open layout, pop Auto-layout, change node up, verify buffer is bigger|
| 3521 | Open layout, pop Auto-layout, change node down, verify buffer is bigger|

#### REPL > Topology UI > bc1 > Design toolbar > Redo

| Number | Description |
| :---:| ---      |
| 3525 | Open layout, at launch it should be empty: icon disabled|
| 3526 | Open layout, make changes, so it will become enabled, close tab, open again, should be disabled again|
| 3527 | Open layout, perform one change, click undo, then redo, verify, then redo once again, verify|

#### REPL > Topology UI > bc1 > Design toolbar > Redo

| Number | Description |
| :---:| ---      |
| 3528 | Open layout, click on arrow, should display popup|
| 3529 | Open layout, click on arrow, should display popup, select Left To Right, verify|
| 3530 | Open layout, click on arrow, change rank down, verify|
| 3531 | Open layout, click on arrow, change rank up, verify|
| 3532 | Open layout, click on arrow, change node down, verify|
| 3533 | Open layout, click on arrow, change node up, verify|
 

#### REPL > Topology static > bc2si1si1 > rome

| Number | Description |
| :---:| ---      |
| 3160 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc2bc4 > Melbourne

| Number | Description |
| :---:| ---      |
| 3164 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc2bc4 > Paris

| Number | Description |
| :---:| ---      |
| 3166 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc2bc4 > rome

| Number | Description |
| :---:| ---      |
| 3168 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc2si1si1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3170 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc2si1si1 > Paris

| Number | Description |
| :---:| ---      |
| 3172 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc2si4 > Melbourne

| Number | Description |
| :---:| ---      |
| 3176 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc2si4 > Paris

| Number | Description |
| :---:| ---      |
| 3178 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc2si4 > rome

| Number | Description |
| :---:| ---      |
| 3180 | verify graph links and rects are correct|
 

#### REPL > Topology tls > bc7 > Melbourne

| Number | Description |
| :---:| ---      |
| 3190 | verify graph links and rects are correct and machines are not dead|
 

#### REPL > Topology static model details > bc2 > Melbourne

| Number | Description |
| :---:| ---      |
| 3205 | verify header|
| 3206 | verify role|
| 3207 | verify data access|
 

#### REPL > Topology static model details > bc7 > Melbourne

| Number | Description |
| :---:| ---      |
| 3215 | verify header|
| 3216 | verify role|
| 3217 | verify data access|
 

#### REPL > Topology static model details > bc7 > Paris

| Number | Description |
| :---:| ---      |
| 3220 | verify header|
| 3221 | verify role|
| 3222 | verify data access|
 

#### REPL > Topology static model details > bc7 > Tokio

| Number | Description |
| :---:| ---      |
| 3230 | verify header|
| 3231 | verify role|
| 3232 | verify data access|
 

#### REPL > Topology static model details > bc2bc4 > Melbourne

| Number | Description |
| :---:| ---      |
| 3235 | verify header|
| 3236 | verify role|
| 3237 | verify data access|
 

#### REPL > Topology static model details > bc2bc4 > Paris

| Number | Description |
| :---:| ---      |
| 3240 | verify header|
| 3241 | verify role|
| 3242 | verify data access|
 

#### REPL > Topology static model details > bc2si1si1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3250 | verify Melbourne header|
| 3251 | verify Melbourne role|
| 3252 | verify Melbourne data access|
| 3253 | verify Paris header|
| 3254 | verify Paris role|
| 3255 | verify Paris data access|
| 3256 | verify Rome header|
| 3257 | verify Rome role|
| 3258 | verify Rome data access|
| 3259 | verify Amsterdam header|
| 3260 | verify Amsterdam role|
| 3261 | verify Amsterdam data access|
 

#### REPL > Topology static model details > bc2si1si1 > Rome

| Number | Description |
| :---:| ---      |
| 3270 | verify Rome header|
| 3271 | verify Rome role|
| 3272 | verify Rome data access|
 

#### REPL > Topology static model details > bc2si1si1 > amsterdam

| Number | Description |
| :---:| ---      |
| 3274 | verify amsterdam header|
| 3275 | verify amsterdam role|
| 3276 | verify amsterdam data access|
 

#### REPL > Topology static model details > bc2si4 > Melbourne

| Number | Description |
| :---:| ---      |
| 3280 | verify Melbourne header|
| 3281 | verify Melbourne role|
| 3282 | verify Melbourne data access|
| 3283 | verify Paris header|
| 3284 | verify Paris role|
| 3285 | verify Paris data access|
| 3286 | verify Santiago header|
| 3287 | verify Santiago role|
| 3288 | verify Santiago data access|
| 3289 | verify Rome header|
| 3290 | verify Rome role|
| 3291 | verify Rome data access|
 

#### REPL > Topology static model details > bc2si4 > Rome

| Number | Description |
| :---:| ---      |
| 3301 | verify Rome header|
| 3302 | verify Rome role|
| 3303 | verify Rome data access|
 

#### REPL > Dashboard static > bc1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3000 | verify repl pill, no backlog|
| 3001 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3002 | verify instance name|
| 3003 | verify popup|
 

#### REPL > Dashboard static >  bc1 > Paris

| Number | Description |
| :---:| ---      |
| 3004 | verify repl pill, no backlog|
| 3005 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3006 | verify instance name|
| 3007 | verify popup|
 

#### REPL > Dashboard static > bc2bc4 > Melbourne

| Number | Description |
| :---:| ---      |
| 3008 | verify repl pill, no backlog|
| 3009 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3010 | verify instance name|
| 3011 | verify popup|
 

#### REPL > Dashboard static > bc2bc4 > Paris

| Number | Description |
| :---:| ---      |
| 3012 | verify repl pill, no backlog|
| 3013 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3014 | verify instance name|
| 3015 | verify popup|
 

#### REPL > Dashboard static > bc2bc4 > Rome

| Number | Description |
| :---:| ---      |
| 3016 | verify repl pill, no backlog|
| 3017 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3018 | verify instance name|
| 3019 | verify popup|
 

#### REPL > Dashboard static > bc2bc4 > Santiago

| Number | Description |
| :---:| ---      |
| 3020 | verify repl pill, no backlog|
| 3021 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3022 | verify instance name|
| 3023 | verify popup|
 

#### REPL > Dashboard static > bc2si1si1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3024 | verify repl pill, no backlog|
| 3025 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3026 | verify instance name|
| 3027 | verify popup|
 

#### REPL > Dashboard static > bc2si1si1 > Paris

| Number | Description |
| :---:| ---      |
| 3028 | verify repl pill, no backlog|
| 3029 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3030 | verify instance name|
| 3031 | verify popup|
 

#### REPL > Dashboard static > bc2si1si1 > Rome

| Number | Description |
| :---:| ---      |
| 3032 | verify repl pill, no backlog|
| 3033 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3034 | verify instance name|
| 3034 | verify popup|
 

#### REPL > Dashboard static > bc2si1si1 > Amsterdam

| Number | Description |
| :---:| ---      |
| 3036 | verify repl pill, no backlog|
| 3037 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3038 | verify instance name|
| 3039 |    it("Test # 3039:verify popup|
 

#### REPL > Dashboard static > bc2si4 > Melbourne

| Number | Description |
| :---:| ---      |
| 3040 | verify repl pill, no backlog|
| 3041 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3042 | verify instance name|
| 3043 | verify popup|
 

#### REPL > Dashboard static > bc2si4 > Paris

| Number | Description |
| :---:| ---      |
| 3044 | verify repl pill, no backlog|
| 3045 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3046 | verify instance name|
| 3047 | verify popup|
 

#### REPL > Dashboard static > bc2si4 > Rome

| Number | Description |
| :---:| ---      |
| 3048 | verify repl pill, no backlog|
| 3049 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3050 | verify instance name|
| 3051 | verify popup|
 

#### REPL > Dashboard static > bc2si4 > Amsterdam

| Number | Description |
| :---:| ---      |
| 3052 | verify repl pill, no backlog|
| 3053 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3054 | verify instance name|
| 3055 | verify popup|
 

#### REPL > Dashboard static > bc2si4 > Santiago

| Number | Description |
| :---:| ---      |
| 3056 | verify repl pill, no backlog|
| 3057 | verify backlog pill, turn backlog off, verify backlog pill is gone|
| 3058 | verify instance name|
| 3059 | verify popup|
 

#### REPL > Dashboard static > bc1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3120 | verify replication menu is enabled|
| 3121 | verify log files is working|
| 3122 | verify instance file is working|
| 3123 | Try to launch topology|
 

#### REPL > Dashboard static > bc2bc1 > Paris

| Number | Description |
| :---:| ---      |
| 3125 | verify replication menu is enabled|
| 3126 | verify log files is working|
| 3127 | verify instance file is working|
| 3128 | Try to launch topology|
 

#### REPL > Dashboard static > bc2bc1 > Santiago

| Number | Description |
| :---:| ---      |
| 3130 | verify replication menu is enabled|
| 3131 | verify log files is working|
| 3132 | verify instance file is working|
| 3133 | Try to launch topology|
 

#### REPL > Dashboard static > bc2si1si1 > Paris

| Number | Description |
| :---:| ---      |
| 3135 | verify replication menu is enabled|
| 3136 | verify log files is working|
| 3137 | verify instance file is working|
| 3138 | Try to launch topology|
 

#### REPL > Dashboard static > bc2si1si1 > Rome

| Number | Description |
| :---:| ---      |
| 3140 | verify replication menu is enabled|
| 3141 | verify log files is working|
| 3142 | verify instance file is working|
| 3143 | Try to launch topology|
 

#### REPL > Topology static > bc1 > Paris

| Number | Description |
| :---:| ---      |
| 3154 | verify graph links and rects are correct|
 

#### REPL > Dashboard dynamic > bc2si1si1 > Paris

| Number | Description |
| :---:| ---      |
| 3104 | verify receiver backlog|
| 3105 | verify sources backlog|
 

#### REPL > Dashboard dynamic > bc2si1si1 > Rome

| Number | Description |
| :---:| ---      |
| 3106 | verify receiver backlog|
| 3107 | verify sources backlog|
 

#### REPL > Topology static > bc1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3150 | verify graph links and rects are correct|
 

#### REPL > Topology static > bc2 > Melbourne

| Number | Description |
| :---:| ---      |
| 3152 | verify graph links and rects are correct|
 

#### REPL > Topology dynamic > bc1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3600 | verify backlog is displayed on all nodes paris down, backlog steady and red, paris up, backlog gets spooled|
 

#### REPL > Topology dynamic > bc1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3601 | verify backlog is displayed on all nodes, london down, backlog steady and red, london up, backlog gets spooled, paris down, backlog steady and red, paris up, backlog gets spooled|
 

#### REPL > Dashboard dynamics > bc2bc4 > Paris

| Number | Description |
| :---:| ---      |
| 3102 | verify receiver backlog|
| 3103 | verify sources backlog|
 

#### REPL > Dashboard dynamic > bc1 > Melbourne

| Number | Description |
| :---:| ---      |
| 3100 | verify sources backlog|
| 3101 | verify there is no receiver backlog|
 
# **SERVER**


#### Endpoints verification

| Number | Description |
| :---:| ---      |
| 5000 | dashboard/getAll|
| 5001 | dashboard/regions/DEFAULT|
| 5002 | Ensure the extra device information is returned|
| 5020 | Use a wrong REST path and ensure that a 404 is returned|
| 5021 | Trigger an error in the server on GET and verify that an error is returned|
| 5022 | Trigger an error in the server on POST and verify that an error is returned|
| 5023 | Trigger an error in the server on DELETE and verify that an error is returned|
| 5024 | Rename a db file and verify that segment fields get populated anyway (with GDE data)|
| 5025 | Rename a db file and verify that region fields get populated anyway (with GDE data)|
| 5026 | Rename a db file and verify that journal fields get populated anyway (with GDE data)|
| 5027 | Create region with different segment name, pull the data and verify, delete the region|

#### GLD ERRORS

| Number | Description |
| :---:| ---      |
| 5100 | Rename the gld to make it appear missing|
| 5101 | GLD env var is set to empty string (same as not existing)|

#### REGION

| Number | Description |
| :---:| ---      |
| 5120 | Rename the default.dat file to make it appear missing|
| 5121 | Check # of sessions by increasing it with a timed session accessing a global|
| 5122 | Verify that processIds of sessions are returned as array|

#### JOURNAL

| Number | Description |
| :---:| ---      |
| 5140 | Switch journaling off in DEFAULT|
| 5141 | Enable journaling in YDBAIM|
| 5142 | Disable journaling in YDBAIM|
| 5143 | Journal file is missing|
| 5144 | Journal file switch: bad region|
| 5145 | Journal file switch: returns OK|

#### REPLICATION

| Number | Description |
| :---:| ---      |
| 5160 | Turn replication on on YDBAIM as verify the response|

#### MAPS

| Number | Description |
| :---:| ---      |
| 5200 | Create a new MAP and see it appearing in the response|

#### LOCKS

| Number | Description |
| :---:| ---      |
| 5220 | LOCKS: Create a lock and verify|
| 5221 | LOCKS: Create a lock and a waiter and verify|
| 5222 | LOCKS: Create a lock and two waiters and verify|
 

#### Get Templates

| Number | Description |
| :---:| ---      |
| 5260 | Verify that BG segment data is complete, including limits|
| 5261 | Verify that MM segment data is complete, including limits|
| 5262 | Verify that region data is complete, including limits|

#### Verify Namespace

| Number | Description |
| :---:| ---      |
| 5270 | Submit just name|
| 5271 | Submit just name with asterisk|
| 5272 | Submit name and subscript|
| 5273 | Submit name and multiple subscripts|
| 5274 | Submit name and multiple subscripts with ranges|
| 5275 | Submit bad name|
| 5276 | Submit name and only open paren|
| 5277 | Submit name and only close paren|
| 5278 | Submit name and bad subscript (alphanumeric, no quotes)|
| 5279 | Submit name and bad subscript (string, only left quote)|
| 5280 | Submit name and bad subscript (string, only right quote)|
| 5281 | Submit name and left range missing|
| 5282 | Submit name and right range missing|
| 5283 | Submit existing name|

#### Verify Filename

| Number | Description |
| :---:| ---      |
| 5290 | Submit name with double /|
| 5291 | Submit name with bad env var|
| 5292 | Submit valid name with existing file|
| 5293 | Submit valid name with absolute path|
| 5294 | Submit valid name with relative path|
| 5295 | Submit valid name with env vars and check for proper extension|
| 5296 | path doesn't exist|
 

#### Create Region

| Number | Description |
| :---:| ---      |
| 5310 | Create random region with default params and verify creation + file existence|
| 5311 | Create random region with different filename and verify creation + file existence|
| 5312 | Create random region with default params, but no db file creation and verify + no file|
| 5313 | Create random region with default params and journal, turned on|
| 5314 | Create random region with default params and journal, turned off|
| 5315 | Create random region with default params and journal, turned off, with different journal path|
| 5316 | Create random region with Auto Db = on|
| 5317 | Create random region with Access Method = MM|
| 5318 | Create random region with all dbAccess fields different and verify them all|
| 5319 | Create random region with all fields different and verify them all|
| 5320 | Create random region with asyncio =true and blocksize= 8192 and verify them all|
| 5321 | Create random region with journal/ON and default fields|
| 5322 | Create random region with journal/OFF and default fields|
| 5323 | Create random region with all journal fields different and verify them all|
| 5324 | Create random region with all segment field different, store them on template, create a new region and verify|
| 5325 | Create random region with all journal field different, store them on template, create a new region and verify|
| 5326 | Add region with fields from Region and Journal changed|

#### Delete Region

| Number | Description |
| :---:| ---      |
| 5400 | Delete using a bad region name|
| 5401 | Delete using a null region name|
| 5402 | Delete an existing region|
| 5403 | Delete an existing region and delete the files|
| 5404 | Delete an existing region and delete the files with segment name different than region name|

#### Journal files

| Number | Description |
| :---:| ---      |
| 6100 | Call the REST /regions/[region]/getJournalInfo with a bad region, error should be returned|
| 6101 | Create a full view situation (multiple journals, with orphans, and active journal file and verify that response is correct|
 

#### Edit Region

| Number | Description |
| :---:| ---      |
| 5340 | Edit YDBOCTO, add one name and delete another, submit and verify|
| 5341 | Edit RANDOM, change BG to MM, submit and verify|
| 5342 | Edit RANDOM, change MM back to BG, submit and verify|
| 5343 | Edit DEFAULT, switch journal OFF, submit and verify|
| 5344 | Edit DEFAULT, switch journal back ON, submit and verify|
| 5345 | Edit DEFAULT, change AutoDB to true and verify|
| 5346 | Edit YDBAIM, change AutoDB to false and verify|
| 5347 | Edit YDBAIM, change AutoDB back to true, change filename, submit and verify|
| 5347 | Edit YDBAIM, change AutoDB back to true, change filename, submit and verify|
| 5348 | Edit DEFAULT, change all Segment related fields, submit and verify|
| 5349 | Edit DEFAULT, change all Segment and Region related fields, submit and verify|
| 5350 | Edit DEFAULT, change all Journaling and Region related fields, submit and verify|
| 5351 | Edit DEFAULT, change all Journaling, Region and Segment related fields, submit and verify|
| 5352 | Edit YDBOCTO, add one name and delete another, change region and journaling and verify|
| 5353 | Edit YDBAIM, add one name and delete another, switch journaling on and verify|
| 5354 | Edit YDBAIM, add one name and delete another, switch journaling off and verify|
| 5355 | Edit DEFAULT, change the journal filename on MUPIP, update and verify|
| 5356 | Edit DEFAULT, change the journal filename on GDE, update and verify\n|
| 5357 | Edit DEFAULT, change the journal filename on GDE, update and verify\n|
 

#### Get All Locks

| Number | Description |
| :---:| ---      |
| 5420 | Set Lock: ^%ydbocto, no waiters, verify|
| 5421 | Set Lock  ^%ydbaim, 1 waiter, verify|
| 5422 | Set Lock  ^%ydbaim, 2 waiters, verify|
| 5423 | Set Lock: test, no waiters, verify|
| 5424 | Set Lock: test, 1 waiter, verify|
| 5425 | Set Lock: test, 1 waiter, verify|
| 5426 | Set Lock  test, test2, ^test3, no waiters, verify|
| 5427 | Set Lock  test, test2, ^test3, 2 waiters, verify|
| 5428 | No locks, verify|
| 5429 | Set lock ^test(\"test with spaces\|

#### Clear lock

| Number | Description |
| :---:| ---      |
| 5430 | No namespace, verify correct error|
| 5431 | Not existing namespace, verify correct error|
| 5432 | Correct namespace, verify correct response and lock gone|
| 5433 | Clear lock in RO mode|

#### Terminate process

| Number | Description |
| :---:| ---      |
| 5435 | Not existing process Id, verify correct error|
| 5436 | process Id 0, verify correct error|
| 5437 | process Id is not YDB, verify correct error|
| 5438 | process Id is correct, verify correct response and terminated process|
 

#### Find globals

| Number | Description |
| :---:| ---      |
| 5500 | List globals using no mask parameter|
| 5501 | List globals using an empty mask|
| 5502 | List globals using AA as mask|
| 5503 | List globals using AAA as mask|
| 5504 | List globals using AAB as mask|
| 5505 | List globals using CC as mask|
| 5506 | List globals using CCCCC|
| 5507 | List globals using CCCC1|
| 5508 | List globals using DDDZ|
| 5509 | List globals using DDDa|

#### getData

| Number | Description |
| :---:| ---      |
| 5525 | Try to supply no path at all|
| 5526 | Try to supply not existing path|
| 5527 | Try to get the path and check its length|

#### dollarData

| Number | Description |
| :---:| ---      |
| 5530 | Verify that a bad name global returns -1|
| 5531 | Verify that a not existing global returns the correct $data value: expect 0|
| 5532 |  Verify that a global returns the correct $data value: expect 1|
| 5533 | Verify that a global returns the correct $data value: expect 10|
| 5534 | Verify that a global returns the correct $data value: expect 11|
 

#### Traverse globals >>> Records validation

| Number | Description |
| :---:| ---      |
| 5551 | Ask for ^PSNDF, no subscripts, 10 entries|
| 5552 | Ask for ^PSNDF, no subscripts, 1000 entries|
| 5553 | Ask for ^PSNDF, no subscripts, 10000 entries|

#### Traverse globals >>> Commas and star after global: regular

| Number | Description |
| :---:| ---      |
| 5570 | ^ORD(,)|
| 5571 | ^ORD(,,)|
| 5572 | ^ORD(,,,)|
| 5573 | ^ORD(*)|
| 5574 | ^ORD(,*)|
| 5575 | ^ORD(,,*)|

#### Traverse globals >>> Commas and star after global: All records

| Number | Description |
| :---:| ---      |
| 5580 | ^ORD(,)|
| 5581 | ^ORD(,,)|
| 5582 | ^ORD(,,,)|
| 5583 | ^ORD(*)|
| 5584 | ^ORD(,*)|
| 5585 | ^ORD(,,*)|

#### Traverse globals >>> Commas and start after 1st subscript: Regular

| Number | Description |
| :---:| ---      |
| 5590 | ^ORD(100.01)|
| 5591 | ^ORD(100.01,)|
| 5592 | ^ORD(100.01,,)|
| 5593 | ^ORD(100.01,,,)|
| 5594 | ^ORD(100.01,*)|
| 5595 | ^ORD(100.01,,*)|
| 5596 | ^ORD(100.01,,,*)|

#### Traverse globals >>> Commas and start after 1st subscript: All records

| Number | Description |
| :---:| ---      |
| 5600 | ^ORD(100.01)|
| 5601 | ^ORD(100.01,)|
| 5602 | ^ORD(100.01,,)|
| 5603 | ^ORD(100.01,,,)|
| 5604 | ^ORD(100.01,*)|
| 5605 | ^ORD(100.01,,*)|
| 5606 | ^ORD(100.01,,,*)|

#### Traverse globals >>>  2 subscripts, numeric and string: Regular

| Number | Description |
| :---:| ---      |
| 5610 | ^ORD(100.01,\"B\")|
| 5611 | ^ORD(100.01,\"B\",)|
| 5612 | ^ORD(100.01,\"B\",,)|
| 5613 | ^ORD(100.01,\"B\",*)|
| 5614 | ^ORD(100.01,\"AMASTERVUID\")|
| 5615 | ^ORD(100.01,\"AMASTERVUID\",)|
| 5616 | ^ORD(100.01,\"AMASTERVUID\",,)|
| 5617 | ^ORD(100.01,\"AMASTERVUID\",*)|

#### Traverse globals >>>  2 subscripts, numeric and string: All records

| Number | Description |
| :---:| ---      |
| 5620 | ^ORD(100.01,\"B\")|
| 5621 | ^ORD(100.01,\"B\",)|
| 5622 | ^ORD(100.01,\"B\",,)|
| 5623 | ^ORD(100.01,\"B\",*)|
| 5624 | ^ORD(100.01,\"AMASTERVUID\")|
| 5625 | ^ORD(100.01,\"AMASTERVUID\",)|
| 5626 | ^ORD(100.01,\"AMASTERVUID\",,)|
| 5627 | ^ORD(100.01,\"AMASTERVUID\",*)|
 

#### Traverse globals >>> Range test

| Number | Description |
| :---:| ---      |
| 5700 | ^ORD(100.01,\"AN\":\"AZ\",*) |
| 5701 | ^ORD(100.01,\"AN\":\"AZ\",4501091:4501096,*) |
| 5702 | ^ORD(100.01,:\"AZ\",*) |
| 5703 | ^ORD(100.01,\"AN\":,*) |
| 5704 | ^ORD(100.01,\"AN\":\"AZ\",4501091:,*) |
| 5705 | ^ORD(100.01,\"AN\":\"AZ\",:4501096,*) |
 

#### Traverse globals >>> With forward offset

| Number | Description |
| :---:| ---      |
| 5750 | ^ORD(100.01,*) 15 recs|

#### Traverse globals >>> With backward offset

| Number | Description |
| :---:| ---      |
| 5800 | ^ORD(100.01,*) 100 recs then the first 10|

#### Traverse globals >>> Test with different dataSize param value

| Number | Description |
| :---:| ---      |
| 5825 | Set dataSize to 10 and verify|
| 5826 | Set dataSize to 5 and verify|
| 5827 | do NOT set dataSize and verify|
| 5828 | Jump to end|
 

#### routines/find

| Number | Description |
| :---:| ---      |
| 5900 | submit with no mask, expect error|
| 5901 | Submit with mask=a, expect error|
| 5902 | Submit with mask=a*, expect error|
| 5903 | Submit with mask=%*, expect error|
| 5904 | Submit with mask=%?, expect error|
| 5905 | Submit with mask=a?, expect error|
| 5906 | Submit with mask=%ydb*, expect ok|
| 5907 | Submit with mask=%ydb*, verify payload|

#### routines/{routine}}

| Number | Description |
| :---:| ---      |
| 5925 | submit no routine, expect error|
| 5926 | submit bad routine, expect error|
| 5927 | submit good routine, expect ok|
| 5928 | submit good routine with no object, verify payload|
| 5929 | submit good routine, verify payload|
 

#### Maintenance: REORG

| Number | Description |
| :---:| ---      |
| 6000 | call /api/regions/maintenance/defrag with no body|
| 6001 | call /api/regions/maintenance/defrag with no fillFactor|
| 6002 | call /api/regions/maintenance/defrag with no indexFillFactor|
| 6003 | call /api/regions/maintenance/defrag with no regions|
| 6005 | call /api/regions/maintenance/defrag with valid body and YDBAIM and YDBJNLF, should return no globals found on both|
| 6006 | create globals on DEFAULT, call /api/regions/maintenance/defrag with valid body and DEFAULT and YDBJNLF, should return no globals found on YDBOCTO and valid data on 'DEFAULT'|
| 6007 | call /api/regions/maintenance/defrag with valid body and DEFAULT, verify that globals list is returned as array in response payloadRegions|
| 6008 | call /api/regions/maintenance/defrag in RO mode, expect HTTP 403|

#### Maintenance: INTEG

| Number | Description |
| :---:| ---      |
| 6025 | call /api/regions/maintenance/integ with no body|
| 6026 | call /api/regions/maintenance/integ with no regions|
| 6027 | call /api/regions/maintenance/integ with no bad type|
| 6028 | call /api/regions/maintenance/integ with no bad reporting|
| 6029 | call /api/regions/maintenance/integ with keyRange different than boolean|
| 6030 | call /api/regions/maintenance/integ with stat different than boolean|
| 6031 | call /api/regions/maintenance/integ with mapErrors with negative number|
| 6032 | call /api/regions/maintenance/integ with mapErrors with number > 1,000,000|
| 6033 | call /api/regions/maintenance/integ with keySizeErrors with negative number|
| 6034 | call /api/regions/maintenance/integ with keySizeErrors with number > 1,000,000|
| 6035 | call /api/regions/maintenance/integ with transactionErrors with negative number|
| 6036 | call /api/regions/maintenance/integ with transactionErrors with number > 1,000,000|
| 6037 | call /api/regions/maintenance/integ with reporting = full, verify|
| 6038 | call /api/regions/maintenance/integ with reporting = summary, verify|
| 6039 | call /api/regions/maintenance/integ in RO mode, expect HTTP 403|

#### Maintenance: BACKUP

| Number | Description |
| :---:| ---      |
| 6050 | call /api/regions/maintenance/backup with no body|
| 6051 | call /api/regions/maintenance/backup with no regions|
| 6052 | call /api/regions/maintenance/backup with no targetPath|
| 6053 | call /api/regions/maintenance/backup with no replace|
| 6054 | call /api/regions/maintenance/backup with no disableJournaling|
| 6055 | call /api/regions/maintenance/backup with no record|
| 6056 | call /api/regions/maintenance/backup with no createNewJournalFiles|
| 6057 | call /api/regions/maintenance/backup with createNewJournalFiles = 'link'|
| 6058 | call /api/regions/maintenance/backup with no includeReplicatedInstances|
| 6059 | call /api/regions/maintenance/backup with includeReplicatedInstances = true|
| 6060 | call /api/regions/maintenance/backup with includeReplicatedInstances = true, replUseSamePath = false|
| 6061 | call /api/regions/maintenance/backup in RO mode, expect HTTP 403|
| 6062 | perform a backup and confirm response|
 

#### get globals size

| Number | Description |
| :---:| ---      |
| 6120 | execute globals/size with no region, verify that error is returned|
| 6121 | execute globals/size with no type, verify that error is returned|
| 6122 | execute globals/size with no param, verify that error is returned|
| 6123 | execute globals/size with arsample, 1000, DEFAULT, verify response|
| 6124 | execute globals/size with arsample, 10000, DEFAULT, verify response|
| 6125 | execute globals/size with arsample, 10, DEFAULT, verify response|
| 6126 | execute globals/size with impsample, 1000, DEFAULT, verify response|
| 6127 | execute globals/size with impsample, 10000, DEFAULT, verify response|
| 6128 | execute globals/size with impsample, 10, DEFAULT, verify response|
| 6129 | execute globals/size with scan, 0, DEFAULT, verify response|
| 6130 | execute globals/size with scan, -1, DEFAULT, verify response|
| 6131 | execute globals/size with scan, 1, DEFAULT, verify response|
| 6132 | execute globals/size with scan, 2, DEFAULT, verify response|
| 6133 | execute globals/size with scan, 3, DEFAULT, verify response|

#### get globals by region

| Number | Description |
| :---:| ---      |
| 6150 | execute globals/by-region/{region} on DEFAULT and verify that there are globals|
| 6151 | execute globals/by-region/{region} on YDBOCTO and verify that there are NO globals|
| 6152 | generate a zombie, execute globals/by-region/{region} on DEFAULT and verify that there are zombies|
| 6153 | execute globals/by-region/{region} on an invalid region, verify that error is returned|
 

#### validate Environment

| Number | Description |
| :---:| ---      |
| 6300 | REST api/regions/gld/validate: supply no path, expect error|
| 6301 | REST api/regions/gld/validate: supply invalid path, expect error|
| 6302 | REST api/regions/gld/validate: supply valid file, valid gld, expect OK|
| 6303 | REST api/regions/gld/validate: supply valid file, bad gld, expect error|
| 6304 | REST api/regions/gld/validate: supply all-good.gld, cwd as /YDBGUI, expect ok|
| 6305 | REST api/regions/gld/validate: supply cwd.gld, expept relativeFlag + pathsInError|
| 6306 | REST api/regions/gld/validate: supply cwd.gld and cwd=\"/YDBGUI\", expept relativeFlag + pathIsnError|
| 6307 | REST api/regions/gld/validate: supply cwd.gld and cwd=\"/data/r1.38_x86_64/g/\", except relativeFlag, no pathsInError|
| 6308 | REST api/regions/gld/validate: supply envVars.gld, expect envVarsFlag, pathsInError and envVars to be set|
| 6309 | REST api/regions/gld/validate: supply envVars.gld and envVars (good ones), expect envVarsFlag, envVars to be set, no pathsInError|
| 6310 | REST api/regions/gld/validate: supply envVars.gld and envVars (bad ones), expect envVarsFlag, envVars and pathsInError to be set|

#### ygblstats enumerate PIDS

| Number | Description |
| :---:| ---      |
| 6320 | REST api/ygbl/get-pids: supply no parameter|
| 6321 | REST api/ygbl/get-pids: supply region param with no value|
| 6322 | REST api/ygbl/get-pids: supply region param with *|
| 6323 | REST api/ygbl/get-pids: supply region=* and run the test program|
| 6324 | REST api/ygbl/get-pids: supply region=DEFAULT and run the test program|
| 6325 | REST api/ygbl/get-pids: supply region=NOEXIST, should return error|
 
# **LOGIN**


#### CLIENT: login

| Number | Description |
| :---:| ---      |
| 10000 | Dummy|
| 10000 | Verify that the login form is displayed|
| 10001 | supply user name, no password, should get error|
| 10002 | supply password, no username, should get error|
| 10003 | supply bad user name, correct password, should get error|
| 10004 | supply bad password, correct username, should get error|
| 10005 | supply admin credentials, should be logged in as RW|
| 10006 | supply user credentials, should be logged in as RO|
| 10007 | Logout: should display inputbox|
| 10008 | Logout: select Yes, should restart|
 

#### SERVER: login

| Number | Description |
| :---:| ---      |
| 11000 | execute the auth mode, verify response|
| 11001 | execute login with wrong credentials, expect 401|
| 11002 | execute login with admin credentials, expect RW mode|
| 11003 | execute login with user credentials, expect RO mode|
| 11004 | execute login, then logout, check response|
 
# **OCTO**


#### Octo / Rocto detection

| Number | Description |
| :---:| ---      |
| 2000 | Dummy|
| 2000 | Ensure menus are enabled when octo is detected|
| 2001 | Ensure menus are disabled when octo is NOT detected|
| 2003 | Ensure tab menu hasn't Octo enabled when octo is detected|
| 2004 | No Octo, ensure no frame is displayed|
| 2005 | Rocto stopped, ensure correct pill text and color are displayed|
| 2006 | Rocto running, ensure correct pill text and color are displayed|
| 2007 | Rocto running, ensure IP #, port and params are displayed|
 

#### Octo: editor

| Number | Description |
| :---:| ---      |
| 2020 | Open the tab, ensure tab caption is New query|
| 2021 | Type \\d and click on 'Run', verify that query returns data in table and tab is selected|
| 2022 | Type '\\d suppliers' and click on 'Run', verify that query returns data in CLI and tab is selected|
| 2024 | Type \\d and click on 'Run', verify that tab name changed into \\d|
| 2026 | Type select * from suppliers, verify that tab name changed into suppliers|
| 2030 | Type  `create table testnew (id integer)', verify response|
| 2032 | Type text, UNDO, ensure text is removed|
| 2033 | Type text, UNDO, ensure text is removed, REDO, ensure text appears again|
| 2034 | Type text, empty line, text, move cursor on empty line and try to submit, should do nothing|
| 2035 | Type: \\q, it should return Empty response|
| 2036 | type \\d, use CTRL-Enter to submit, verify|
| 2037 | RO mode: type \\d, it should NOT execute|
| 2038 | RO mode: type \\q, it should NOT execute|
| 2039 | RO mode: type INSERT, should display msgbox|
| 2040 | RO mode: type UPDATE, should display msgbox|
| 2041 | RO mode: type SELECT, should display msgbox|
| 2042 | RO mode: type DROP, should display msgbox|
| 2044 | execute one create, one insert, one update, one delete, one drop, verify CLI response|
| 2045 | execute 1 select, one error (select * from notexist), one select, verify|
| 2046 | execute select * from notexist, then \\d notexist, verify both are trapped correctly|
| 2047 | click on edit icon on the tab, should display the tab edit dialog|
| 2048 | pop the tab edit dialog, change the text and submit, verify|
| 2049 | type \\d, verify|
| 2050 | type \\d v1, verify|
| 2051 | type \\dv, verify|
 

#### Tree

| Number | Description |
| :---:| ---      |
| 2100 | pop the tree, wait and check tables population|
| 2101 | pop the tree, wait and check functions population|
| 2102 | pop the tree, check population, hide it|
| 2103 | pop the tree, check population, hide it, show it again, it shouldn't refresh|
| 2104 | select first table, double click, ensure the node gets populated|
| 2105 | select first table, double click, double click again, it shouldn't fetch data again|
| 2106 | Pop the tree, wait and check views population|
| 2107 | Select first view, double click, ensure it gets populated|
| 2108 | Select first view, double click, double click again, it shouldn't fetch again|
 

#### CLI view

| Number | Description |
| :---:| ---      |
| 2125 | generate an error with select, verify error gets colored|
| 2126 | generate an error with \\d idontexist, verify error is returned|
| 2127 | enter \\d suppliers, verify response|
 

#### Data view

| Number | Description |
| :---:| ---      |
| 2150 | type: select * from test, verify that table has one column|
| 2151 | type: select * from orderdetails, verify that table has 4 columns|
| 2152 | execute a multiple (8) chained select statements, verify all tabs are there, with correct table displayed|
| 2153 | execute two select, verify tabs, change limit, rerun and verify that new tabs are created|
| 2154 | execute two select, verify tabs, close one tab, rerun and verify that only one tab is created|
| 2155 | execute a SELECT, insert a record, re-execute the select, verify that the same tab is used, but the new record appears|
 

#### Octo / Rocto detection

| Number | Description |
| :---:| ---      |
| 6200 | Check Octo detected, Rocto stopped|
| 6201 | Run Rocto, check Octo detected, Rocto running, stop Rocto|
| 6202 | Run Rocto with params, check Octo detected, Rocto running, stop Rocto|
| 6203 | Run Rocto on another port, with params, check Octo detected, Rocto running, stop Rocto|
| 6204 | Move .../plugin/octo directory, refresh, check Octo missing, move back|
| 6205 | Move %ydbocto.dat as *.old, detect should fail, move *.old file back|

#### execute Query

| Number | Description |
| :---:| ---      |
| 6220 | submit with no query specified|
| 6221 | submit with no timeout specified|
| 6222 | submit with \\d;, verify response|
| 6223 | submit with \\d, timeout 1, verify timeout gets triggered|
| 6224 | submit query with double-quotes, verify it gets processed alright|
| 6225 | submit query without ;, verify that it gets executed|
| 6226 | RO: try to execute an SELECT, verify error is returned|
| 6227 | RO: try to execute an INSERT, verify error is returned|
| 6228 | RO: try to execute an UPDATE, verify error is returned|
| 6229 | RO: try to execute an DROP, verify error is returned|
| 6230 | RO: try to execute a \\d, verify it doesn't executed|
| 6231 | RO: try to execute a \\q, verify it doesn't executed|

#### get-objects

| Number | Description |
| :---:| ---      |
| 6250 | submit, verify response|

#### get-objects

| Number | Description |
| :---:| ---      |
| 6251 | submit table with injection (suppliers;drop), verify response|

#### get-objects

| Number | Description |
| :---:| ---      |
| 6252 | submit, verify response includes the view `v1`|

#### get-struct

| Number | Description |
| :---:| ---      |
| 6275 | submit with bad table, verify error|
| 6276 | submit with valid table, verify response|
| 6277 | submit with bad view, verify error|
| 6278 | submit with valid view, verify response|
 

Number of tests: 1576
