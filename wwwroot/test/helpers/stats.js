/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.stats.storage.test = {
    importTestFile: async function (fileNumber) {
        return new Promise(async function (resolve) {
            const file = 'test/statsTestFiles/test-' + fileNumber + '.ysr'

            $.ajax({
                url: file,
                success: function (data) {
                    app.ui.stats.storage.data.push(JSON.parse(data))
                    app.ui.storage.save('statsSources', app.ui.stats.storage.data)
                    resolve()
                }
            });
        })
    },

    loadTest: async function (fileNumber) {
        return new Promise(async function (resolve) {
            const file = 'test-' + fileNumber

            resolve(app.ui.stats.storage.load.okPressed(undefined, file))
        })
    },

    saveTest: function () {
        app.ui.stats.storage.data.push(app.ui.stats.entry)
        app.ui.storage.save('statsSources', app.ui.stats.storage.data)

        // update tab header
        $('#lblStatsReportName').text('[' + app.ui.stats.entry.name + ']')
        app.ui.stats.entryDirty = false
    }
}
