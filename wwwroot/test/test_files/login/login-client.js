/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe(": CLIENT: login", async () => {
    // this helps prevent errors in the job due to ERR:NETWORK:CHANGED returned (for some mysterious reason) by some jobs
    it("Test # 10000: Dummy", async () => {
        try {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

        } catch (err) {
            await libs.delay(500)
        }
    });

    it("Test # 10000: Verify that the login form is displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');
    });

    it("Test # 10001: supply user name, no password, should get error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');

        await page.keyboard.type('admin');

        btnClick = await page.$("#btnLoginLogin");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox')

    });

    it("Test # 10002: supply password, no username, should get error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');

        await page.keyboard.press('Tab');
        await page.keyboard.type('admin');

        btnClick = await page.$("#btnLoginLogin");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox')

    });

    it("Test # 10003: supply bad user name, correct password, should get error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');

        await page.keyboard.type('badAdmin');
        await page.keyboard.press('Tab');
        await page.keyboard.type('admin');

        btnClick = await page.$("#btnLoginLogin");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox')

    });

    it("Test # 10004: supply bad password, correct username, should get error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');

        await page.keyboard.type('admin');
        await page.keyboard.press('Tab');
        await page.keyboard.type('badpwd');

        btnClick = await page.$("#btnLoginLogin");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox')

    });

    it("Test # 10005: supply admin credentials, should be logged in as RW", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');

        await page.keyboard.type('admin');
        await page.keyboard.press('Tab');
        await page.keyboard.type('admin');

        btnClick = await page.$("#btnLoginLogin");
        await btnClick.click();

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const text = await page.evaluate(() => $('#lblUserMode').text());
        expect(text).to.have.string('RW')
    });

    it("Test # 10006: supply user credentials, should be logged in as RO", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');

        await page.keyboard.type('user');
        await page.keyboard.press('Tab');
        await page.keyboard.type('user');

        btnClick = await page.$("#btnLoginLogin");
        await btnClick.click();

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const text = await page.evaluate(() => $('#lblUserMode').text());
        expect(text).to.have.string('RO')
    });

    it("Test # 10007: Logout: should display inputbox", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');

        await page.keyboard.type('admin');
        await page.keyboard.press('Tab');
        await page.keyboard.type('admin');

        btnClick = await page.$("#btnLoginLogin");
        await btnClick.click();

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const text = await page.evaluate(() => $('#lblUserMode').text());
        expect(text).to.have.string('RW')

        await page.evaluate(() => app.ui.login.logout());

        await libs.waitForDialog('modalInputbox');
    });

    it("Test # 10008: Logout: select Yes, should restart", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');

        await page.keyboard.type('admin');
        await page.keyboard.press('Tab');
        await page.keyboard.type('admin');

        btnClick = await page.$("#btnLoginLogin");
        await btnClick.click();

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const text = await page.evaluate(() => $('#lblUserMode').text());
        expect(text).to.have.string('RW')

        await page.evaluate(() => app.ui.login.logout());

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalLogin');
    });
});

