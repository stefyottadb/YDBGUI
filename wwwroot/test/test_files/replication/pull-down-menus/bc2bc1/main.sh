#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

echo '***************************'
echo '---Pull-down menus > bc2bc1'
echo '***************************'
echo

$PWD/replication/repl run bc2bc1
while
	! docker logs --tail 1 melbourne | grep -q "Starting Server at port" ||
	! docker logs --tail 1 paris | grep -q "Starting Server at port" ||
	! docker logs --tail 1 santiago | grep -q "Starting Server at port" ||
	! docker logs --tail 1 rome | grep -q "Starting Server at port";
do sleep 1
done

docker exec -e ydb_in_repl=paris paris npm test -- wwwroot/test/test_files/replication/pull-down-menus/bc2bc1/paris.js
script1=$?

docker exec -e ydb_in_repl=santiago santiago npm test -- wwwroot/test/test_files/replication/pull-down-menus/bc2bc1/santiago.js
script2=$?

$PWD/replication/repl down

exitCode=0
 if [ $script1 != 0 ] || [ $script2 != 0 ]; then
 	exitCode=1
 fi
 exit $exitCode
