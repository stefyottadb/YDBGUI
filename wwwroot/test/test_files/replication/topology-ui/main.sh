#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

echo '*****************************'
echo 'Topology ui'
echo '*****************************'
echo

$PWD/wwwroot/test/test_files/replication/topology-ui/bc1/main.sh
script1=$?
$PWD/wwwroot/test/test_files/replication/topology-ui/bc2bc1/main.sh
script2=$?

exitCode=0
if [ $script1 != 0 ] || [ $script2 != 0 ]; then
	exitCode=1
fi
exit $exitCode
