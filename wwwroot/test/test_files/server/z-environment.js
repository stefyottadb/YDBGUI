/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");
const {env} = require('process')
const {exec} = require('child_process');

describe("SERVER: validate Environment", async () => {
    it("Test # 6300: REST api/regions/gld/validate: supply no path, expect error", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {}).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'ERROR').to.be.true;

        // and verify that the fields are there
        expect(res.error.description).to.have.string('No path has been specified')
    });

    it("Test # 6301: REST api/regions/gld/validate: supply invalid path, expect error", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {path: '/this/does/not/exist.gld'}).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'ERROR').to.be.true;

        // and verify that the fields are there
        expect(res.error.description).to.have.string('The specified gld file could not be found')
    });

    it("Test # 6302: REST api/regions/gld/validate: supply valid file, valid gld, expect OK", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {path: '/YDBGUI/wwwroot/test/test-gld/all-good.gld'}).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.envVarsFlag).to.be.false
        expect(res.data.relativeFlag).to.be.false
        expect(typeof res.data.envVars === 'object').to.be.true
    });

    it("Test # 6303: REST api/regions/gld/validate: supply valid file, bad gld, expect error", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {path: '/YDBGUI/wwwroot/test/test-gld/bad.gld'}).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 6304: REST api/regions/gld/validate: supply all-good.gld, cwd as /YDBGUI, expect ok", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {
            path: '/YDBGUI/wwwroot/test/test-gld/all-good.gld',
            cwd: '/YDBGUI'
        }).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'OK').to.be.true;

        expect(res.data.envVarsFlag).to.be.false
        expect(res.data.relativeFlag).to.be.false
        expect(typeof res.data.envVars === 'object').to.be.true
        expect(typeof res.data.pathsInError === 'object').to.be.false
    });

    it("Test # 6305: REST api/regions/gld/validate: supply cwd.gld, expept relativeFlag + pathsInError", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {
            path: '/YDBGUI/wwwroot/test/test-gld/cwd.gld',
        }).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'OK').to.be.true;

        expect(res.data.envVarsFlag).to.be.false
        expect(res.data.relativeFlag).to.be.true
        expect(typeof res.data.envVars === 'object').to.be.true
        expect(typeof res.data.pathsInError === 'object').to.be.true
    });

    it("Test # 6306: REST api/regions/gld/validate: supply cwd.gld and cwd=\"/YDBGUI\", expept relativeFlag + pathIsnError", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {
            path: '/YDBGUI/wwwroot/test/test-gld/cwd.gld',
            cwd: '/YDBGUI'
        }).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'OK').to.be.true;

        expect(res.data.envVarsFlag).to.be.false
        expect(res.data.relativeFlag).to.be.true
        expect(typeof res.data.envVars === 'object').to.be.true
        expect(typeof res.data.pathsInError === 'object').to.be.true
    });

    it("Test # 6307: REST api/regions/gld/validate: supply cwd.gld and cwd=\"/data/r1.38_x86_64/g/\", except relativeFlag, no pathsInError", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {
            path: '/YDBGUI/wwwroot/test/test-gld/cwd.gld',
            cwd: '/data/' + env.ydb_rel + '/g/'
        }).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'OK').to.be.true;

        expect(res.data.envVarsFlag).to.be.false
        expect(res.data.relativeFlag).to.be.true
        expect(typeof res.data.envVars === 'object').to.be.true
        expect(typeof res.data.pathsInError === 'object').to.be.false
    });

    it("Test # 6308: REST api/regions/gld/validate: supply envVars.gld, expect envVarsFlag, pathsInError and envVars to be set", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {
            path: '/YDBGUI/wwwroot/test/test-gld/envvars.gld',
        }).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'OK').to.be.true;
        expect(res.data.envVarsFlag).to.be.false
        expect(res.data.relativeFlag).to.be.false
        expect(typeof res.data.envVars === 'object').to.be.true
        expect(typeof res.data.pathsInError === 'object').to.be.true
    });

    it("Test # 6309: REST api/regions/gld/validate: supply envVars.gld and envVars (good ones), expect envVarsFlag, envVars to be set, no pathsInError", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {
            path: '/YDBGUI/wwwroot/test/test-gld/envvars.gld',
            envVars: [{'$ydb_dir': '/data'}, {'$ydb_rel': env.ydb_rel}, {'$ydb_rel2': env.ydb_rel}]
        }).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'OK').to.be.true;
        expect(res.data.envVarsFlag).to.be.true
        expect(res.data.relativeFlag).to.be.false
        expect(typeof res.data.envVars === 'object').to.be.true
        expect(typeof res.data.pathsInError === 'object').to.be.false
    });

    it("Test # 6310: REST api/regions/gld/validate: supply envVars.gld and envVars (bad ones), expect envVarsFlag, envVars and pathsInError to be set", async () => {
        // execute the call
        let res = await libs._RESTpost('regions/gld/validate', {
            path: '/YDBGUI/wwwroot/test/test-gld/envvars.gld',
            envVars: [{'$ydb_dir': '/data'}, {'$ydb_rel': env.ydb_rel}, {'$ydb_rel2': env.ydb_rel + '-bad'}]
        }).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'OK').to.be.true;
        expect(res.data.envVarsFlag).to.be.true
        expect(res.data.relativeFlag).to.be.false
        expect(typeof res.data.envVars === 'object').to.be.true
        expect(typeof res.data.pathsInError === 'object').to.be.true
    });
})

describe("SERVER: ygblstats enumerate PIDS", async () => {
    it("Test # 6320: REST api/ygbl/get-pids: supply no parameter", async () => {
        // execute the call
        const res = await libs._REST('ygbl/get-pids').catch(() => {});

        expect(res.error.description).to.have.string('No region parameter have been specified')
    })

    it("Test # 6321: REST api/ygbl/get-pids: supply region param with no value", async () => {
        // execute the call
        const res = await libs._REST('ygbl/get-pids?region=').catch(() => {});

        expect(res.error.description).to.have.string('No region have been specified')
    })

    it("Test # 6322: REST api/ygbl/get-pids: supply region param with *", async () => {
        // execute the call
        const res = await libs._REST('ygbl/get-pids?region=*').catch(() => {});

        expect(res.data === undefined).to.be.true
    })

    it("Test # 6323: REST api/ygbl/get-pids: supply region=* and run the test program", async () => {
        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');
        await libs.delay(1000)

        // execute the call
        const res = await libs._REST('ygbl/get-pids?region=*').catch(() => {});

        expect(res.data.processes.length === 1).to.be.true

        await libs.delay(2000)
    })

    it("Test # 6324: REST api/ygbl/get-pids: supply region=DEFAULT and run the test program", async () => {
        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,3)"');
        await libs.delay(1000)

        // execute the call
        const res = await libs._REST('ygbl/get-pids?region=DEFAULT').catch(() => {});

        expect(res.data.processes.length === 1).to.be.true

        await libs.delay(2000)
    })

    it("Test # 6325: REST api/ygbl/get-pids: supply region=NOEXIST, should return error", async () => {
        // run the background random generator program
        // execute the call
        const res = await libs._REST('ygbl/get-pids?region=NOEXIST').catch(() => {});

        expect(res.error.description).to.have.string('The region: NOEXIST was not found on the server')
    })
})

