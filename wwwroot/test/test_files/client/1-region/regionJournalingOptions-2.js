/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');


describe("CLIENT: Journal files", async () => {
    it("Test # 1700: Display the dialog, it should list only the active journal file", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let btnClick = await page.$("#btnDashRegionView0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionView');

            await page.evaluate(() => app.ui.regionJournalFiles.show('DEFAULT'))

            // wait for regionView to be set by the async call
            //await libs.waitForDialog('#modalRegionJournalFiles');
            await libs.delay(3000)

            // should have 1 row only
            const rows = await page.evaluate(() => $('#tblRegionJournalFiles >tbody').children())
            expect(rows.length === 1).to.be.true

            // and marked as active
            const cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(1) >td:nth-child(2)');
            const text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'Active').to.be.true
        }
    })

    it("Test # 1701: Switch journal files, it should list the active and the extra files in the chain", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // switch journal
            execSync('. /opt/yottadb/current/ydb_env_set && mupip set -journal -region default');

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let btnClick = await page.$("#btnDashRegionView0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionView');

            await page.evaluate(() => app.ui.regionJournalFiles.show('DEFAULT'))

            // wait for regionView to be set by the async call
            //await libs.waitForDialog('#modalRegionJournalFiles');
            await libs.delay(3000)


            // should have 2 rows
            const rows = await page.evaluate(() => $('#tblRegionJournalFiles >tbody').children())
            expect(rows.length === 2).to.be.true

            // first marked as active
            let cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(1) >td:nth-child(2)');
            let text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'Active').to.be.true

            // second marked as orphan
            cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(2) >td:nth-child(2)');
            text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'In chain').to.be.true
        }
    })

    it("Test # 1702: Break the journal chain, it should display the active and the orphans", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // break the chain
            execSync('. /opt/yottadb/current/ydb_env_set && mupip backup -replace -newjnlfiles=noprevlink  DEFAULT /');

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let btnClick = await page.$("#btnDashRegionView0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionView');

            await page.evaluate(() => app.ui.regionJournalFiles.show('DEFAULT'))

            // wait for regionView to be set by the async call
            //await libs.waitForDialog('#modalRegionJournalFiles');
            await libs.delay(3000)

            // should have 3 rows
            const rows = await page.evaluate(() => $('#tblRegionJournalFiles >tbody').children())
            expect(rows.length === 3).to.be.true

            // The first marked as active
            let cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(1) >td:nth-child(2)');
            let text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'Active').to.be.true

            // and the other two marked as Orphans
            cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(2) >td:nth-child(2)');
            text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'Orphan').to.be.true

            cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(3) >td:nth-child(2)');
            text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'Orphan').to.be.true
        }
    })

    it("Test # 1703: Fill up the journal to trigger a switch, it should display the active file, the extra journal in the chain and the orphans", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // switch the journal by filling the active up
            execSync('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'for x=1:1:70000000 set ^y(x)=x  \'');

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let btnClick = await page.$("#btnDashRegionView0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionView');

            await page.evaluate(() => app.ui.regionJournalFiles.show('DEFAULT'))

            // wait for regionView to be set by the async call
            //await libs.waitForDialog('#modalRegionJournalFiles');
            await libs.delay(3000)

            // should have 4 rows
            const rows = await page.evaluate(() => $('#tblRegionJournalFiles >tbody').children())
            expect(rows.length === 4).to.be.true

            // The first marked as active
            let cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(1) >td:nth-child(2)');
            let text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'Active').to.be.true

            // The second marked as in chain
            cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(2) >td:nth-child(2)');
            text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'In chain').to.be.true

            // and the other two marked as Orphans
            cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(3) >td:nth-child(2)');
            text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'Orphan').to.be.true

            cell = await page.$('#tblRegionJournalFiles >tbody >tr:nth-child(4) >td:nth-child(2)');
            text = await page.evaluate(el => el.textContent, cell);
            expect(text === 'Orphan').to.be.true
        }
    })

    it("Test # 1704: Click on View details..., it should display the journal details dialog", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let btnClick = await page.$("#btnDashRegionView0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionView');

            await page.evaluate(() => app.ui.regionJournalFiles.show('DEFAULT'))

            // wait for regionView to be set by the async call
            //await libs.waitForDialog('#modalRegionJournalFiles');
            await libs.delay(3000)

            btnClick = await page.$("#btnJournalFilesDetails0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionJournalFilesDetails');
        }
    })

    it("Test # 1705: Click on View details..., ensure all entries are correct", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let btnClick = await page.$("#btnDashRegionView0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionView');

            await page.evaluate(() => app.ui.regionJournalFiles.show('DEFAULT'))

            // wait for regionView to be set by the async call
            //await libs.waitForDialog('#modalRegionJournalFiles');
            await libs.delay(3000)

            btnClick = await page.$("#btnJournalFilesDetails0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionJournalFilesDetails');

            // should have 4 rows
            const rows = await page.evaluate(() => $('#tblRegionJournalFilesDetails >tbody').children())
            expect(rows.length === 41).to.be.true

            // The last should have /n in the cell text
            let cell = await page.$('#tblRegionJournalFilesDetails >tbody >tr:nth-child(41) >td:nth-child(2)');
            let text = await page.evaluate(el => el.textContent, cell);
            expect(text).to.have.string('Node')
        }
    })

    it("Test # 1706: Ensure total disk space is displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashRegionView0");
        await btnClick.click();

        // wait for regionView to be set by the async call
        await libs.waitForDialog('#modalRegionView');

        await page.evaluate(() => app.ui.regionJournalFiles.show('DEFAULT'))

        // wait for regionView to be set by the async call
        //await libs.waitForDialog('#modalRegionJournalFiles');
        await libs.delay(3000)

        // check if disk space is populated
        const text = await page.evaluate(() => $('#lblRegionJournalFilesTotalSpace').text())
        expect(text !== '').to.be.true
    })
})

describe("CLIENT: Switch Journal File", async () => {
    it("Test # 1720: Click on the `Switch Journal` button, ensure inputbox is displayed", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let btnClick = await page.$("#btnDashRegionView0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionView');

            await page.evaluate(() => app.ui.regionJournalSwitch.switchFiles())

            await libs.waitForDialog('modalInputbox')
        }
    })

    it("Test # 1721: Click on the Switch Journal button, cancel should close it", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let btnClick = await page.$("#btnDashRegionView0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionView');

            await page.evaluate(() => app.ui.regionJournalSwitch.switchFiles())

            await libs.waitForDialog('modalInputbox')

            btnClick = await page.$("#btnInputboxNo");
            await btnClick.click();

            // wait for inputbox to be set by the async call
            await libs.waitForDialog('#modalInputbox', 'close');
        }
    })

    it("Test # 1722: Click on the Switch Journal button, select Yes, it should close it", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let btnClick = await page.$("#btnDashRegionView0");
            await btnClick.click();

            // wait for regionView to be set by the async call
            await libs.waitForDialog('#modalRegionView');

            await page.evaluate(() => app.ui.regionJournalSwitch.switchFiles())

            await libs.waitForDialog('modalInputbox')

            btnClick = await page.$("#btnInputboxYes");
            await btnClick.click();

            // wait for inputbox to be set by the async call
            await libs.waitForDialog('#modalInputbox', 'close');
        }
    })
})
