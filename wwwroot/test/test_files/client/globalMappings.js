/*
#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe("CLIENT: Global mappings", async () => {
    it("Test # 1820: display dialog, ensure list is correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // click menu
        await page.evaluate(() => app.ui.globalMappings.show());

        await libs.waitForDialog('#modalGlobalMappings');

        let caption = await page.evaluate(() => $('#tblGlobalMappings > tbody > tr:first-child td:first-child').text());
        expect(caption === '%ydbAIM*').to.be.true

        caption = await page.evaluate(() => $('#tblGlobalMappings > tbody > tr:first-child td:nth-child(2)').text());
        expect(caption === 'YDBAIM').to.be.true

        caption = await page.evaluate(() => $('#tblGlobalMappings > tbody > tr:nth-child(7) td:first-child').text());
        expect(caption === '%ydbJNlF*').to.be.true

        caption = await page.evaluate(() => $('#tblGlobalMappings > tbody > tr:nth-child(7) td:nth-child(2)').text());
        expect(caption === 'YDBJNLF').to.be.true
    });

    it("Test # 1821: display dialog, ensure list contains *", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // click menu
        await page.evaluate(() => app.ui.globalMappings.show());

        await libs.waitForDialog('#modalGlobalMappings');

        let caption = await page.evaluate(() => $('#tblGlobalMappings > tbody > tr:last-child td:first-child').text());
        expect(caption === '*').to.be.true

        caption = await page.evaluate(() => $('#tblGlobalMappings > tbody > tr:last-child td:nth-child(2)').text());
        expect(caption === 'DEFAULT').to.be.true
    });
});

