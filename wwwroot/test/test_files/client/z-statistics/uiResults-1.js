/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");
const {exec} = require('child_process');

describe("Statistics: Stats: Results: General tab and filters population", async () => {
    it("Test # 2650: Load test-1, run it without bgnd program running, stop it, it should display msgbox: There is no data to display in the result tab.", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(4000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('There is no data to display in the result tab')
    })

    it("Test # 2651: Load test-3, run it without bgnd program running, stop it, it should display msgbox: There is no data to display in the result tab.", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(3))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(3))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(4000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('There is no data to display in the result tab')
    })

    it("Test # 2652: Load test-8, run it without bgnd program running, stop it, it should display msgbox: There is no data to display in the result tab.", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(4000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('There is no data to display in the result tab')
    })

    it("Test # 2653: Load test-1, run bgnd program, acquire 5 secs of samples, stop, tab should be enabled", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        const disabled = await page.evaluate(() => $('#tabStatsResult').prop('disabled'))

        expect(disabled).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2654: Load test-3, run bgnd program, acquire 5 secs of samples, stop, tab should be enabled", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(3))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(3))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        const disabled = await page.evaluate(() => $('#tabStatsResult').prop('disabled'))

        expect(disabled).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2655: Load test-1, run bgnd program, acquire 5 secs of samples, stop, select results tab, source, region and processes should be disabled, samples and values should be populated", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        let disabled = await page.evaluate(() => $('#tabStatsResult').prop('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#selStatsResultsSource').prop('disabled'))
        expect(disabled).to.be.true

        let hasClass = await page.evaluate(() => $('#ddStatsResultsRegions').hasClass('disabled'))
        expect(hasClass).to.be.true

        hasClass = await page.evaluate(() => $('#ddStatsResultsProcesses').hasClass('disabled'))
        expect(hasClass).to.be.true

        hasClass = await page.evaluate(() => $('#ddStatsResultsSamples').hasClass('disabled'))
        expect(hasClass).to.be.false

        hasClass = await page.evaluate(() => $('#ddStatsResultsSampleTypes').hasClass('disabled'))
        expect(hasClass).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2656: Load test-3, run bgnd program, acquire 5 secs of samples, stop, select results tab, source, and region should be disabled, processes, samples and values should be populated", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(3))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(3))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        let disabled = await page.evaluate(() => $('#tabStatsResult').prop('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#selStatsResultsSource').prop('disabled'))
        expect(disabled).to.be.true

        let hasClass = await page.evaluate(() => $('#ddStatsResultsRegions').hasClass('disabled'))
        expect(hasClass).to.be.true

        hasClass = await page.evaluate(() => $('#ddStatsResultsProcesses').hasClass('disabled'))
        expect(hasClass).to.be.false

        hasClass = await page.evaluate(() => $('#ddStatsResultsSamples').hasClass('disabled'))
        expect(hasClass).to.be.false

        hasClass = await page.evaluate(() => $('#ddStatsResultsSampleTypes').hasClass('disabled'))
        expect(hasClass).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2657: Load test-8, run bgnd program, acquire 5 secs of samples, stop, select results tab, region and processes should be disabled, source, samples and values should be populated", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        let disabled = await page.evaluate(() => $('#tabStatsResult').prop('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#selStatsResultsSource').prop('disabled'))
        expect(disabled).to.be.false

        let hasClass = await page.evaluate(() => $('#ddStatsResultsRegions').hasClass('disabled'))
        expect(hasClass).to.be.true

        hasClass = await page.evaluate(() => $('#ddStatsResultsProcesses').hasClass('disabled'))
        expect(hasClass).to.be.true

        hasClass = await page.evaluate(() => $('#ddStatsResultsSamples').hasClass('disabled'))
        expect(hasClass).to.be.false

        hasClass = await page.evaluate(() => $('#ddStatsResultsSampleTypes').hasClass('disabled'))
        expect(hasClass).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2658: Load test-8, run 2 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 2 entries", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the second source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_32558280-6b78-487f-9d29-d2d141d6bd53"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        //count the processes found in the list
        const children = await page.evaluate(() => $('#selStatsResultToolbarProcesses').children())
        expect(children.length === 2).to.be.true

        await libs.delay(3000)
    })

    it("Test # 2659: Load test-8, run 3 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 3 entries", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the second source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_32558280-6b78-487f-9d29-d2d141d6bd53"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        //count the processes found in the list
        const children = await page.evaluate(() => $('#selStatsResultToolbarProcesses').children())
        expect(children.length === 3).to.be.true

        await libs.delay(3000)
    })

    it("Test # 2660: Load test-8, run 4 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 4 entries", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the second source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_32558280-6b78-487f-9d29-d2d141d6bd53"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        //count the processes found in the list
        const children = await page.evaluate(() => $('#selStatsResultToolbarProcesses').children())
        expect(children.length === 4).to.be.true

        await libs.delay(3000)
    })

    it("Test # 2661: Load test-8, run 5 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 5 entries", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the second source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_32558280-6b78-487f-9d29-d2d141d6bd53"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        //count the processes found in the list
        const children = await page.evaluate(() => $('#selStatsResultToolbarProcesses').children())
        expect(children.length === 5).to.be.true

        await libs.delay(3000)
    })
    it("Test # 2662: Load test-8, run 6 x bgnd program, acquire 5 secs of samples, stop, select results tab, processes combo should have 5 entries", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(500)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the second source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_32558280-6b78-487f-9d29-d2d141d6bd53"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        //count the processes found in the list
        const children = await page.evaluate(() => $('#selStatsResultToolbarProcesses').children())
        expect(children.length === 5).to.be.true

        await libs.delay(3000)
    })
})

describe("Statistics: Stats: Results: Headers and filters + source switching", async () => {
    it("Test # 2665: Load test-1, run bgnd program, acquire 5 secs of samples, stop, verify result headers", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        let text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr > th:first').text())
        expect(text).to.have.string('Timestamp')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr > th:nth-child(2)').text())
        expect(text).to.have.string('Region: DEFAULT')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr:nth-child(2) > th').text())
        expect(text).to.have.string('All processes')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr:nth-child(3) ').text())
        expect(text).to.have.string('SETKILGETORDDRDDWTJFLJFSJBBJFBJFW')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr:nth-child(4) ').text())
        expect(text).to.have.string('AbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ Max')

        await libs.delay(3000)
    })

    it("Test # 2666: Load test-1, run bgnd program, acquire 5 secs of samples, stop, remove samples, verify result headers", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // remove samples
        btnClick = await page.$("#ddStatsResultsSamples");
        await btnClick.click();

        btnClick = await page.$("#chkStatsResultSample-0-JFW");
        await btnClick.click();

        btnClick = await page.$("#chkStatsResultSample-0-JFB");
        await btnClick.click();

        btnClick = await page.$("#chkStatsResultSample-0-JBB");
        await btnClick.click();


        let text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr > th:first').text())
        expect(text).to.have.string('Timestamp')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr > th:nth-child(2)').text())
        expect(text).to.have.string('Region: DEFAULT')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr:nth-child(2) > th').text())
        expect(text).to.have.string('All processes')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr:nth-child(3) ').text())
        expect(text).to.have.string('SETKILGETORDDRDDWTJFLJFS')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr:nth-child(4) ').text())
        expect(text).to.have.string('AbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ Max')

        await libs.delay(5000)
    })


    it("Test # 2667: Load test-1, run bgnd program, acquire 5 secs of samples, stop, remove data, verify result headers", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // remove samples
        btnClick = await page.$("#ddStatsResultsSampleTypes");
        await btnClick.click();

        btnClick = await page.$("#chkStatsResultValue-0-Abs");
        await btnClick.click();

        let text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr > th:first').text())
        expect(text).to.have.string('Timestamp')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr > th:nth-child(2)').text())
        expect(text).to.have.string('Region: DEFAULT')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr:nth-child(2) > th').text())
        expect(text).to.have.string('All processes')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr:nth-child(3) ').text())
        expect(text).to.have.string('SETKILGETORDDRDDWTJFLJFS')

        text = await page.evaluate(() => $('#tblStatsResults-0 > thead > tr:nth-child(4) ').text())
        expect(text).to.have.string('ΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ MaxΔΔ AvgΔ MinΔ Max')

        await libs.delay(5000)
    })

    it("Test # 2668: Load test-8, run bgnd program, acquire 5 secs of samples, stop, verify result headers on second source", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        const hProcess = exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the second source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_32558280-6b78-487f-9d29-d2d141d6bd53"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        let text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr > th:first').text())
        expect(text).to.have.string('Timestamp')

        text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr > th:nth-child(2)').text())
        expect(text).to.have.string('All regions')

        text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr:nth-child(2) > th').text())
        expect(text).to.have.string('P-')

        text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr:nth-child(3) ').text())
        expect(text).to.have.string('SETKILGET')

        text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr:nth-child(4) ').text())
        expect(text).to.have.string('AbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ Max')

        await libs.delay(5000)
    })

    it("Test # 2669: Load test-8, run bgnd program, acquire 5 secs of samples, stop, switch to second source, remove one process, verify result headers", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the second source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_32558280-6b78-487f-9d29-d2d141d6bd53"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        // get the process list
        const oldProcesses = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr:nth-child(2) ').text())

        // remove process
        btnClick = await page.$("#ddStatsResultsProcesses");
        await btnClick.click();

        // get the processId
        let processId = await page.evaluate(() => document.getElementById('selStatsResultToolbarProcesses').children[0].children[0].children[0].id)

        // remove it
        btnClick = await page.$("#" + processId);
        await btnClick.click();

        // and compare
        text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr:nth-child(2) ').text())
        expect(text !== oldProcesses).to.be.true

        await libs.delay(5000)
    })

    it("Test # 2670: Load test-6, run bgnd program, acquire 5 secs of samples, stop, select 2nd source, verify result headers", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the second source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_5fd15ec9-c8af-48e1-8692-389e0b0f9fc8"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        let text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr > th:first').text())
        expect(text).to.have.string('Timestamp')

        text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr > th:nth-child(2)').text())
        expect(text).to.have.string('Region: DEFAULT')

        text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr:nth-child(2) > th').text())
        expect(text).to.have.string('P-')

        text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr:nth-child(3) ').text())
        expect(text).to.have.string('KIL')

        text = await page.evaluate(() => $('#tblStatsResults-1 > thead > tr:nth-child(4) ').text())
        expect(text).to.have.string('AbsΔΔ AvgΔ MinΔ Max')

        await libs.delay(5000)
    })

    it("Test # 2671: Load test-6, run bgnd program, acquire 5 secs of samples, stop, select 4th source, verify result headers", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the 4th source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_47891c28-6b71-4b8c-bc69-671d9840aac6"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        let text = await page.evaluate(() => $('#tblStatsResults-3 > thead > tr > th:first').text())
        expect(text).to.have.string('Timestamp')

        text = await page.evaluate(() => $('#tblStatsResults-3 > thead > tr > th:nth-child(2)').text())
        expect(text).to.have.string('Region: DEFAULT')

        text = await page.evaluate(() => $('#tblStatsResults-3 > thead > tr th:nth-child(3)').text())
        expect(text).to.have.string('Region: YDBAIM')

        text = await page.evaluate(() => $('#tblStatsResults-3 > thead > tr:nth-child(3) ').text())
        expect(text).to.have.string('SETSET')

        text = await page.evaluate(() => $('#tblStatsResults-3 > thead > tr:nth-child(4) ').text())
        expect(text).to.have.string('AbsΔΔ AvgΔ MinΔ MaxAbsΔΔ AvgΔ MinΔ Max')

        await libs.delay(3000)
    })
})

