/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");

describe("Statistics: Stats: Sources: PEEKBYNAME  Add", async () => {
    it("Test # 2770: Click ok, should display msgbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        let btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2771: select 1 sample,click ok, should create source, verify in table", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.ref_cnt"]').attr('selected', 'selected'))

        let btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let rowData = await page.evaluate(() => $('#tblStatsSources > tbody > tr ').text())
        expect(rowData).to.have.string('PEEKBYNAMEnode_local.ref_cntDEFAULT1.000')
    })

    it("Test # 2772: select 3 samples, change region, change sample rate, click ok, verify in table", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_set"]').attr('selected', 'selected'))
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_get"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))

        let btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let rowData = await page.evaluate(() => $('#tblStatsSources > tbody > tr ').text())
        expect(rowData).to.have.string('PEEKBYNAMEnode_local.gvstats_rec.n_set, node_local.gvstats_rec.n_getYDBAIM3.000')
    })
})

describe("Statistics: Stats: Sources: PEEKBYNAME Edit", async () => {
    it("Test # 2780: Create source, enter edit mode, change sample rate, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2781: Create source, enter edit mode, change sample rate, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('PEEKBYNAMEnode_local.gvstats_rec.n_setDEFAULT3.000')
    })

    it("Test # 2782: Create source, enter edit mode, change region, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2783: Create source, enter edit mode, change region, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');


        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('PEEKBYNAMEnode_local.gvstats_rec.n_setYDBAIM1.000')
    })

    it("Test # 2784: Create source, enter edit mode, change region and sample rate, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2785: Create source, enter edit mode, change region and sample rate, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('PEEKBYNAMEnode_local.gvstats_rec.n_setYDBAIM3.000')
    })

    it("Test # 2786: Create source, enter edit mode, change samples, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2787: Create source, enter edit mode, change samples, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('PEEKBYNAMEnode_local.gvstats_rec.n_set, node_local.gvstats_rec.n_lock_failDEFAULT1.000')
    })

    it("Test # 2788: Create source, enter edit mode, change samples and sample rate, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test # 2789: Create source, enter edit mode, change samples and sample rate, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('PEEKBYNAMEnode_local.gvstats_rec.n_set, node_local.gvstats_rec.n_lock_failDEFAULT3.000')
    })

    it("Test # 2790: Create source, enter edit mode, change samples and region, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test #  2791: Create source, enter edit mode, change samples and region, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('PEEKBYNAMEnode_local.gvstats_rec.n_set, node_local.gvstats_rec.n_lock_failYDBAIM1.000')
    })

    it("Test # 2792: Create source, enter edit mode, change samples, sample rate and region, submit, expect inputbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');
    })

    it("Test #  2793: Create source, enter edit mode, change samples, sample rate and region, submit, expect inputbox, answer YES, verify", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourcePeekByNameAdd()

        await utils.createSourceFheadPeekByName('peekByName')

        // select and edit
        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector');

        await libs.delay(2000)

        // set samples
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorSamples option[value="node_local.gvstats_rec.n_lock_fail"]').attr('selected', 'selected'))

        // set region
        await page.evaluate(() => $('#optStatsFheadPeekByNameSelectorRegions option[value="YDBAIM"]').attr('selected', 'selected'))

        // set sample rate
        await page.evaluate(() => $('#inpStatsFheadPeekByNameSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.fheadPeekByName.sampleRateUpdated())

        btnClick = await page.$("#btnStatsFheadPeekByNameSelectorOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalStatsFheadPeekByNameSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr').text())
        expect(colData).to.have.string('PEEKBYNAMEnode_local.gvstats_rec.n_set, node_local.gvstats_rec.n_lock_failYDBAIM3.000')
    })
})
