/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");
const {env} = require('process')

describe("CLIENT: Global Directory", async () => {
    it("Test # 2909: Select envvars.gld, change the empty env var to a bad one, verify again", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('$ydb_rel2')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the env vars to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

        // pop up the env var dialog
        btnClick = await page.$("#btnEnvSettingsEnvVars");
        await btnClick.click();

        // and wait for the dialog to display
        await libs.waitForDialog('#modalEnvEnvVars');

        // focus on control for env var
        await page.evaluate(() => $('#txtEnvVarList_ydb_rel2').focus())
        // and type it in
        await page.keyboard.type('r1.36');

        // close the dialog
        btnClick = await page.$("#btnEnvEnvVarsOk");
        await btnClick.click();

        // and wait for the dialog to close
        await libs.waitForDialog('#modalEnvEnvVars', 'close');

        // verify env settings gets updated
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables')

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('/data/r1.36/g/%ydbocto.dat')
    })

    it("Test # 2910: Select envvars.gld, change the empty env var to a good one, verify, hit Set and verify, open again, hit Reset and verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('$ydb_rel2')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the env vars to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

        // pop up the env var dialog
        btnClick = await page.$("#btnEnvSettingsEnvVars");
        await btnClick.click();

        // and wait for the dialog to display
        await libs.waitForDialog('#modalEnvEnvVars');

        // focus on control for env var
        await page.evaluate(() => $('#txtEnvVarList_ydb_rel2').focus())
        // and type it in
        await page.keyboard.type(env.ydb_rel);

        // close the dialog
        btnClick = await page.$("#btnEnvEnvVarsOk");
        await btnClick.click();

        // and wait for the dialog to close
        await libs.waitForDialog('#modalEnvEnvVars', 'close');

        // verify env settings gets updated
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('The environment variables are set')

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('You can now press "Set" to apply the changes...')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set button is enabled, Reset is disabled
        status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // press Set
        btnClick = await page.$("#btnEnvSettingsSet");
        await btnClick.click();

        // wait for envSettings to be closed
        await libs.waitForDialog('#modalEnvSettings', 'close');

        await libs.delay(500)

        // count the regions
        let regions = await page.evaluate(() => app.system.regions)
        let regionsCount = 0

        for (const region in regions) {
            regionsCount++
        }
        expect(regionsCount === 3).to.be.true

        // verify text in alerts
        const file = await page.evaluate(() => $('#txtGldAltFilename').text())
        expect(file).to.have.string('YDBGUI/wwwroot/test/test-gld/envvars.gld')

        const extra = await page.evaluate(() => $('#txtGldAltExtra').text())
        expect(extra).to.have.string('Environment variables: ydb_dir=/data; ydb_rel=' + env.ydb_rel + '; ydb_rel2=' + env.ydb_rel)

        // press Change / Reset
        btnClick = await page.$(".dash-gld-link");
        await btnClick.click();

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // press Reset
        btnClick = await page.$("#btnEnvSettingsReset");
        await btnClick.click();

        // wait for envSettings to be closed
        await libs.waitForDialog('#modalEnvSettings', 'close');

        await libs.delay(500)

        // count the regions
        regions = await page.evaluate(() => app.system.regions)
        regionsCount = 0

        for (const region in regions) {
            regionsCount++
        }
        expect(regionsCount === 4).to.be.true
    })

    it("Test # 2911: Select envvars.gld, change the empty env var to a good one, the existing to a bad one, verify again", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('$ydb_rel2')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the env vars to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

        // pop up the env var dialog
        btnClick = await page.$("#btnEnvSettingsEnvVars");
        await btnClick.click();

        // and wait for the dialog to display
        await libs.waitForDialog('#modalEnvEnvVars');

        // focus on control for env var
        await page.evaluate(() => $('#txtEnvVarList_ydb_rel2').focus())
        // and type it in
        await page.keyboard.type('r1.38_x86_64');

        // empty text box
        await page.evaluate(() => $('#txtEnvVarList_ydb_rel').val(''))

        // focus on control for env var
        await page.evaluate(() => $('#txtEnvVarList_ydb_rel').focus())
        // and type it in
        await page.keyboard.type('r1.36_x86_64');

        // close the dialog
        btnClick = await page.$("#btnEnvEnvVarsOk");
        await btnClick.click();

        // and wait for the dialog to close
        await libs.waitForDialog('#modalEnvEnvVars', 'close');

        // verify env settings gets updated
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables')

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('/data/r1.36_x86_64/g/%ydbjnlf.dat')
        expect(validationText).to.have.string('/data/r1.36_x86_64/g/yottadb.dat')
    })

    it("Test # 2912: Select cwd-envvars.gld, change the cwd to a bad one, verify again", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('$ydb_rel2')
        expect(msg).to.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the cwd to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

        // change cwd

        // press Set cwd to submit it
        btnClick = await page.$("#btnEnvSettingsCwd");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalCwdSelector');

        // set focus on path input field
        await page.evaluate(() => $('#lblCwdSelectorPath').focus())
        // and type it in
        await page.evaluate(() => $('#lblCwdSelectorPath').val(''))
        await page.keyboard.type('/YDBGUI');

        await libs.delay(100)

        // press Validate
        btnClick = await page.$("#btnCwdSelectorValidate");
        await btnClick.click();

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('The path has been validated...')

        // close msg box
        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'close');

        // close CWD selector
        btnClick = await page.$("#btnCwdSelectorOk");
        await btnClick.click();

        // wait for dialog to close
        await libs.waitForDialog('#modalCwdSelector', 'close');

        // verify that path has been set
        const cwd = await page.evaluate(() => $('#lblEnvSettingsCwd').val())
        expect(cwd).to.have.string('/YDBGUI')

        // it has invalid class
        const invalidClass = await page.evaluate(() => $('#lblEnvSettingsCwd').hasClass('is-invalid'))
        expect(invalidClass).to.be.true

        // and help text appears
        const invalidText = await page.evaluate(() => $('#valEnvSettingsCwdInvalid').text())
        expect(invalidText).to.have.string('You need to set up the current working directory')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set button is enabled, Reset is disabled
        status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false
    })

    it("Test # 2913: Select cwd-envvars.gld, change the cwd to a good one, verify again", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('$ydb_rel2')
        expect(msg).to.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the cwd to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

        // change cwd

        // press Set cwd to submit it
        btnClick = await page.$("#btnEnvSettingsCwd");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalCwdSelector');

        // set focus on path input field
        await page.evaluate(() => $('#lblCwdSelectorPath').focus())
        // and type it in
        await page.evaluate(() => $('#lblCwdSelectorPath').val(''))
        await page.keyboard.type('/data/' + env.ydb_rel + '/g');

        await libs.delay(100)

        // press Validate
        btnClick = await page.$("#btnCwdSelectorValidate");
        await btnClick.click();

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('The path has been validated...')

        // close msg box
        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'close');

        // close CWD selector
        btnClick = await page.$("#btnCwdSelectorOk");
        await btnClick.click();

        // wait for dialog to close
        await libs.waitForDialog('#modalCwdSelector', 'close');

        // verify that path has been set
        const cwd = await page.evaluate(() => $('#lblEnvSettingsCwd').val())
        expect(cwd).to.have.string('/data/' + env.ydb_rel + '/g')

        // it has invalid class
        const invalidClass = await page.evaluate(() => $('#lblEnvSettingsCwd').hasClass('is-invalid'))
        expect(invalidClass).to.be.true

        // and help text appears
        const invalidText = await page.evaluate(() => $('#valEnvSettingsCwdInvalid').text())
        expect(invalidText).to.have.string('You changed the current working directory')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.not.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set button is enabled, Reset is disabled
        status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false
    })

    it("Test # 2914: Select cwd-envvars.gld, change the empty env var to a bad one, verify again", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('ydb_rel2')
        expect(msg).to.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the env vars to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

        // pop up the env var dialog
        btnClick = await page.$("#btnEnvSettingsEnvVars");
        await btnClick.click();

        // and wait for the dialog to display
        await libs.waitForDialog('#modalEnvEnvVars');

        // focus on control for env var
        await page.evaluate(() => $('#txtEnvVarList_ydb_rel2').focus())
        // and type it in
        await page.keyboard.type('r1.36');

        // close the dialog
        btnClick = await page.$("#btnEnvEnvVarsOk");
        await btnClick.click();

        // and wait for the dialog to close
        await libs.waitForDialog('#modalEnvEnvVars', 'close');

        // verify env settings gets updated
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables')

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('/data/r1.36/g/%ydbocto.dat')
    })

    it("Test # 2915: Select cwd-envvars.gld, change the empty env var to a good one, verify again", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('ydb_rel2')
        expect(msg).to.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the env vars to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

        // pop up the env var dialog
        btnClick = await page.$("#btnEnvSettingsEnvVars");
        await btnClick.click();

        // and wait for the dialog to display
        await libs.waitForDialog('#modalEnvEnvVars');

        // focus on control for env var
        await page.evaluate(() => $('#txtEnvVarList_ydb_rel2').focus())
        // and type it in
        await page.keyboard.type('r1.38_x86_64');

        // close the dialog
        btnClick = await page.$("#btnEnvEnvVarsOk");
        await btnClick.click();

        // and wait for the dialog to close
        await libs.waitForDialog('#modalEnvEnvVars', 'close');

        // verify env settings gets updated
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables')

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('yottadb.dat')
    })

    it("Test # 2916: Select cwd-envvars.gld, change the empty env var to a bad one, the cwd to a bad one, verify again", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('ydb_rel2')
        expect(msg).to.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the env vars to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

        // pop up the env var dialog
        btnClick = await page.$("#btnEnvSettingsEnvVars");
        await btnClick.click();

        // and wait for the dialog to display
        await libs.waitForDialog('#modalEnvEnvVars');

        // focus on control for env var
        await page.evaluate(() => $('#txtEnvVarList_ydb_rel2').focus())
        // and type it in
        await page.keyboard.type('r1.36');

        // close the dialog
        btnClick = await page.$("#btnEnvEnvVarsOk");
        await btnClick.click();

        // and wait for the dialog to close
        await libs.waitForDialog('#modalEnvEnvVars', 'close');

        // verify env settings gets updated
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables')

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        let validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('/data/r1.36/g/%ydbocto.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // change cwd

        // press Set cwd to submit it
        btnClick = await page.$("#btnEnvSettingsCwd");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalCwdSelector');

        // set focus on path input field
        await page.evaluate(() => $('#lblCwdSelectorPath').focus())
        // and type it in
        await page.evaluate(() => $('#lblCwdSelectorPath').val(''))
        await page.keyboard.type('/data/' + env.ydb_rel + '/r/');

        await libs.delay(100)

        // press Validate
        btnClick = await page.$("#btnCwdSelectorValidate");
        await btnClick.click();

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('The path has been validated...')

        // close msg box
        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'close');

        // close CWD selector
        btnClick = await page.$("#btnCwdSelectorOk");
        await btnClick.click();

        // wait for dialog to close
        await libs.waitForDialog('#modalCwdSelector', 'close');

        // verify that path has been set
        const cwd = await page.evaluate(() => $('#lblEnvSettingsCwd').val())
        expect(cwd).to.have.string('/data/' + env.ydb_rel + '/r/')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string(':/data/r1.36/g/%ydbocto.dat')
        expect(msg).to.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');
    })
})
