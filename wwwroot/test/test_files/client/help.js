/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const {expect} = require("chai");

describe("CLIENT: Help: load", async () => {
    it("Test # 34: Try to load the help", async () => {
        // perform the test
        await page.goto(`https://localhost:${MDevPort}/help/index.html?stats/index`, {
            waitUntil: "domcontentloaded"
        });

        const div = await page.evaluate(() => $('#divHelpContent'))

        expect(div !== undefined).to.be.true
    });
});
