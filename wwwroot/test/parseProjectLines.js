/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

const fs = require('fs');
const {walkSync} = require('./libs');
const paths = [
    {
        type: 'm',
        path: 'routines/'
    },
    {
        type: 'css',
        path: 'wwwroot/css/'
    },
    {
        type: 'js',
        path: 'wwwroot/js/'
    },
    {
        type: 'html',
        path: 'wwwroot/html/'
    },
    {
        type: 'test',
        path: 'wwwroot/test/'
    },
];

const filesHistory = 'wwwroot/test/filesHistory.csv';
let filesList;
const timestamp = new Date;
const result = {
    grand: 0,
    grandCode: 0
};

try {
    paths.forEach(path => {
        filesList = [];
        result[path.type] = 0;

        walkSync(path.path, filesList);
        filesList.forEach(file => {
            if (file.indexOf('.mp4') === -1) {
                result[path.type] += fs.readFileSync(file).toString().split('\n').length
            }
        });

        if (path.type !== 'test') result.grandCode += result[path.type];

        result.grand += result[path.type];
    });

    const newData =
        timestamp.toISOString().split('.')[0] + ',' +
        result.grand + ',' +
        result.grandCode + ',' +
        result.m + ',' +
        result.css + ',' +
        result.js + ',' +
        result.html + ',' +
        result.test + '\n';

    fs.appendFileSync(filesHistory, newData);

    console.log('Data appended to: ' + filesHistory)

} catch (err) {
    console.log(err)
}
