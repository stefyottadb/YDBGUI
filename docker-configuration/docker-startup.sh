#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property	      #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################
# $1 = test or shell/bash or server/server-tls/[none] [none] = server
# $2 = if $1 = test, test file (optional); otherwise, entire test suite is run
# $3 = if $1 = test, readwrite, readonly, octo-readwrite, octo-readonly, login
# $2 = if $1 = shell/bash, $2 is ignored.
# $2 = if $1 = server*, it's the rest of the arguments to the %ydbwebreq (like --userpass xxx, --readwrite, log [0-3], etc.)
source /YDBGUI/dev
export ydb_tls_passwd_ydbgui="$(echo ydbgui | /opt/yottadb/current/plugin/gtmcrypt/maskpass | cut -d ":" -f2 | tr -d '[:space:]')"

# This is normally done by CMake, but needed as we don't have this file in the wwwroot mounted-from-host directory
if [ -f /YDBGUI/wwwroot/index.html ]; then
	if ! [ -f /YDBGUI/wwwroot/_ydbgui.manifest.json ]; then
		cp $ydb_dist/plugin/etc/ydbgui/_ydbgui.manifest.json /YDBGUI/wwwroot/_ydbgui.manifest.json
	fi
fi

echo "Host name is: "$HOSTNAME

# replication port setting, melbourne is also 8089
server_port=8089
if [ "$HOSTNAME" = "paris" ]; then
    server_port=9089
elif [ "$HOSTNAME" = "santiago" ]; then
    server_port=10089
elif [ "$HOSTNAME" = "rome" ]; then
    server_port=11089
elif [ "$HOSTNAME" = "amsterdam" ]; then
    server_port=12089
elif [ "$HOSTNAME" = "london" ]; then
    server_port=13089
elif [ "$HOSTNAME" = "tokio" ]; then
    server_port=14089
elif [ "$HOSTNAME" = "madrid" ]; then
    server_port=15089
fi

if [ "$1" = "test" ]; then
	if [ ! -f /YDBGUI/wwwroot/index.html ]; then
		echo "You must mount wwwroot to /YDBGUI/wwwroot to run tests"
		exit -1
	fi
	cd /YDBGUI/wwwroot
	if [ "$3" = "readwrite" ]; then
		yottadb -run start^%ydbgui --port $server_port --tlsconfig ydbgui --log 0 --readwrite 1> /dev/null &
	elif [ "$3" = "octo-readwrite" ]; then
		yottadb -run start^%ydbgui --port $server_port --tlsconfig ydbgui --log 0 --readwrite 1> /dev/null &
	elif [ "$3" = "octo-readonly" ]; then
		yottadb -run start^%ydbgui --port $server_port --tlsconfig ydbgui --log 0 1> /dev/null &
	elif [ "$3" = "login" ]; then
		yottadb -run start^%ydbgui --port $server_port --tlsconfig ydbgui --log 2 --auth-file /YDBGUI/wwwroot/test/users.json 1> /dev/null &
	elif [ "$3" = "remote-config" ]; then
		yottadb -run start^%ydbgui --port $server_port --tlsconfig ydbgui --client-config test/ydbgui-range-settings.json --log 1 --readwrite  1> /dev/null &
	else
		yottadb -run start^%ydbgui --port $server_port --tlsconfig ydbgui --log 0 1> /dev/null &
	fi
	echo "Running tests..."
	echo "Ctrl-C twice to stop..."
	if [ -n "$2" ] && [ "$2" != "--" ]; then
		exec npm test -- $2
	else
		if [ -n "$2" ] && [ "$2" != "--" ]; then
			exec npm test -- $2
		else
			if [ "$3" = "login" ] ; then
				exec npm test -- "wwwroot/test/test_files/login/"
			elif [ "$3" = "octo-readwrite" ] || [ "$3" = "octo-readonly" ] ; then
				exec npm test -- "wwwroot/test/test_files/octo/"
			elif [ "$3" = "remote-config" ] ; then
				exec npm test -- "wwwroot/test/test_files/remoteconfig/"
			else
				exec npm test -- "wwwroot/test/test_files/client" "wwwroot/test/test_files/server"
			fi
		fi
	fi
elif [ "$1" = "shell" ] || [ "$1" = "bash" ]; then
	echo "Starting shell..."
	exec /bin/bash
else
	if [ -f /YDBGUI/wwwroot/index.html ]; then
		cd /YDBGUI/wwwroot
	else
		cd $ydb_dist/plugin/etc/ydbgui
	fi

	tls=$1
	shift 1

	echo "Starting the Server..."
	echo "Ctrl-\ to stop"
	mupip rundown -relinkctl
	if [ "$tls" = "server-tls" ] || [ "$tls" = "tls-server" ]; then
		exec yottadb -run start^%ydbgui --port $server_port --tlsconfig ydbgui $@
	else
		# if replicating, bring the replication up
		if [ "$HOSTNAME" = "paris" ] ||  [ "$HOSTNAME" = "santiago" ] ||  [ "$HOSTNAME" = "melbourne" ] ||  [ "$HOSTNAME" = "rome" ] ||  [ "$HOSTNAME" = "amsterdam" ] ||  [ "$HOSTNAME" = "london" ] ||  [ "$HOSTNAME" = "tokio" ] ||  [ "$HOSTNAME" = "madrid" ]; then
			# trap SIGTERM and execute shutdown
			trap "/repl/shutdown.sh" SIGTERM SIGKILL
			# replication startup
			. /repl/repl-startup.sh
			# run the m-web-server in RW mode
			yottadb -run start^%ydbgui --port $server_port --readwrite --tlsconfig ydbgui &
			#  infinite loop
			wait $!
		else
			# run the m-web-server
			exec yottadb -run start^%ydbgui --port $server_port $@
		fi
	fi
fi
