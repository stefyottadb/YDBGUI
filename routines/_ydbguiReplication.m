%ydbguiReplication ; YottaDB Replication; 03-29-2023
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	quit
	;
getInstanceFile()
	new res,ret,cmd,shellResult,cnt,line,replInstanceFile
	new prevChar,param1,param2,charCnt,char,paramCnt,found,array
	new headerFound,slotsFound,historyFound,typeNode,name,value
	new slotNumber,isPrimaryRoot,isSupplementary,isSecondaryReadWrite
	new level
	;
	set level=$zlevel
	;
	; mount error handler
	new $etrap
	set $etrap="set $ecode="""",res(""result"")=""INTERNAL_ERROR"" zgoto level:getInstanceHeaderQuit"
	;
	set (headerFound,slotsFound,historyFound,slotNumber,isPrimaryRoot,isSupplementary,isSecondaryReadWrite)=0
	set typeNode=""
	;
	; check for instance file existance
	set replInstanceFile=$ztrnlnm("ydb_repl_instance")
	if replInstanceFile="" set replInstanceFile=$ztrnlnm("gtm_repl_instance")
	if replInstanceFile="" goto getInstanceHeaderQuit
	if $zsearch(replInstanceFile,-1)="" goto getInstanceHeaderQuit
	;
	; execute the call
	set cmd="$ydb_dist/mupip replicate -editinstance -show "_replInstanceFile
	set ret=$$runShell^%ydbguiUtils(cmd,.shellResult)
	if ret'=0 do  goto getInstanceHeaderQuit
	. set res("status")="ERROR"
	. set res("error","description")="The following error was returned: "_ret
	. merge res("error","dump")=shellResult
	;
	; parse the response and extract the values
	set cnt="" for  set cnt=$order(shellResult(cnt)) quit:cnt=""  do
	. set line=shellResult(cnt)
	. ;remove extra spaces
	. ;
	. quit:$zfind(line,"YDB-I-MUREPLSHOW")
	. quit:line=""
	. quit:$zfind(line,"---")
	. if $zfind(line,"File Header") set typeNode="header" quit
	. if $zfind(line,"Source Server Slots") set typeNode="slots" quit
	. if $zfind(line,"History Records") set typeNode="history" quit
	. ;
	. set (prevChar,param1,param2)="",found=0
	. for charCnt=1:1:$zlength(line) do
	. . set char=$zextract(line,charCnt)
	. . if char=" ",prevChar=" " do
	. . . set param1=$zextract(line,1,charCnt-1)
	. . . for paramCnt=charCnt:1:$zlength(line) do  quit:found
	. . . . if $zextract(line,paramCnt,paramCnt)'=" " set param2=$zextract(line,paramCnt,$zlength(line)),found=1
	. . else  set prevChar=char
	. ;
	. if param1="" set array(typeNode,cnt,"name")=$zpiece(line," ",1,4),array(typeNode,cnt,"value")=$zpiece(line," ",5)
	. else  set array(typeNode,cnt,"name")=$$R^%TRIM(param1),array(typeNode,cnt,"value")=param2
	. ;
	. ; adjust names and values
	. if typeNode="header" do
	. . set name=array(typeNode,cnt,"name")
	. . set name=$zextract(name,5,$zlength(name))
	. . set array(typeNode,cnt,"name")=$translate(name," ","")
	. . ;
	. . set value=array(typeNode,cnt,"value")
	. . if $zfind(value,"[") set array(typeNode,cnt,"value")=$zpiece(value," ",1)
	. . if value="INVALID" set array(typeNode,cnt,"value")="null"
	. . if value="TRUE"!(value="FALSE") set array(typeNode,cnt,"value")=$$FUNC^%LCASE(value)
	. . if array(typeNode,cnt,"name")="RootPrimary",value="TRUE" set isPrimaryRoot=1
	. . if array(typeNode,cnt,"name")="SupplementaryInstance",value="TRUE" set isSupplementary=1
	. . ;
	. if typeNode="slots" do
	. . set name=array(typeNode,cnt,"name")
	. . set value=array(typeNode,cnt,"value")
	. . set slotNumber=$zpiece(name," ",3)+1
	. . kill array(typeNode,cnt)
	. . if $zfind(value,"[") set value=$zpiece(value," ",1)
	. . set array(typeNode,slotNumber,$zpiece(name," ",5,99))=value
	. ;
	. if typeNode="history" do
	. . set name=$$^%MPIECE(array(typeNode,cnt,"name"))
	. . set value=array(typeNode,cnt,"value")
	. . set historyNumber=+$zpiece(name," ",3)+1
	. . kill array(typeNode,cnt)
	. . set array("history",historyNumber,cnt)=value
	;
	; update slots with extra peek-by-name info
	for cnt=0:1:15 do
	. set instName=$$^%PEEKBYNAME("gtmsource_local_struct.secondary_instname",cnt,"S")
	. if instName'="" do
	. . set ix="" for  set ix=$order(array("slots",ix)) quit:ix=""  do
	. . . if array("slots",ix,"Secondary Instance Name")=instName do
	. . . . set array("slots",ix,"host")=$$^%PEEKBYNAME("gtmsource_local_struct.secondary_host",cnt,"S")
	. . . . set array("slots",ix,"port")=$$^%PEEKBYNAME("gtmsource_local_struct.secondary_port",cnt)
	. . . . set array("slots",ix,"state")=$$^%PEEKBYNAME("gtmsource_local_struct.mode",cnt)
	;
	; create flags leg
	set array("flags","isPrimaryRoot")=isPrimaryRoot
	set array("flags","isSupplementary")=isSupplementary
	set array("flags","instanceName")=$$^%PEEKBYNAME("repl_inst_hdr.inst_info.this_instname",,"S")
	if isSupplementary set isSecondaryReadWrite='$$FUNC^%HD($$^%PEEKBYNAME("jnlpool_ctl_struct.upd_disabled"))
	set array("flags","isReadWrite")=isPrimaryRoot!(isSupplementary&(isSecondaryReadWrite))
	;
	set res("result")="OK"
	;
getInstanceHeaderQuit
	merge res("data")=array
	quit *res
	;
	;
getHealth()
	new res,ix,pid,isAlive,mode,instName,allAlive
	new level
	;
	set level=$zlevel
	;
	; mount error handler
	new $etrap
	set $etrap="set $ecode="""",res=""INTERNAL_ERROR"" zgoto level:getHealthQuit"
	;
	; sources
	set allAlive=1
	for ix=0:1:15 do
	. set pid=$$^%PEEKBYNAME("gtmsource_local_struct.gtmsource_pid",ix)
	. if pid=0 quit
	. set isAlive=$zgetjpi(pid,"isprocalive")
	. if isAlive=0 set allAlive=0
	. set mode=$$^%PEEKBYNAME("gtmsource_local_struct.mode",ix)
	. set instName=$$^%PEEKBYNAME("gtmsource_local_struct.secondary_instname",ix)
	. set res("sources",ix+1,"pid")=pid
	. set res("sources",ix+1,"isAlive")=$select(isAlive=1:"true",1:"false")
	. set res("sources",ix+1,"mode")=$select(mode:"active",1:"passive")
	. set res("sources",ix+1,"instanceName")=instName
	;
	set res("sourcesGlobal","allAlive")=$select(allAlive:"true",1:"false")
	;
	; update process
	do
	. new $etrap
	. set $etrap="set $ecode="""",res(""updateProcess"",""isAlive"")=""null"" quit"
	. ;
	. set isAlive=0
	. set res("updateProcess","isAlive")="false"
	. set pid=$$^%PEEKBYNAME("upd_proc_local_struct.upd_proc_pid")
	. if pid set isAlive=$zgetjpi(pid,"isprocalive")
	. set res("updateProcess","pid")=pid
	. set res("updateProcess","isAlive")=$select(isAlive:"true",1:"false")
	;
	; receiver server
	do
	. new $etrap
	. set $etrap="set $ecode="""",res(""receiverServer"",""isAlive"")=""null"" quit"
	. ;
	. set isAlive=0
	. set res("receiverServer","isAlive")="false"
	. set pid=$$^%PEEKBYNAME("gtmrecv_local_struct.recv_serv_pid")
	. if pid set isAlive=$zgetjpi(pid,"isprocalive")
	. set res("receiverServer","pid")=pid
	. set res("receiverServer","isAlive")=$select(isAlive:"true",1:"false")
	;
getHealthQuit
	quit *res
	;
	;
getBacklog(type)
	new res,ret,ix,tot,streamValues,level
	;
	set level=$zlevel
	new $etrap
	set $etrap="set $ecode="""" zgoto level:getBacklogQuit"
	;
	; Source
	if type="S" do  goto getBacklogQuit
	. set tot=$$FUNC^%HD($$^%PEEKBYNAME("jnlpool_ctl_struct.jnl_seqno"))
	. for ix=0:1:15 quit:$$^%PEEKBYNAME("gtmsource_local_struct.gtmsource_pid",ix)=0  do
	. . set res("data","streams",ix+1)=-$$FUNC^%HD($$^%PEEKBYNAME("gtmsource_local_struct.read_jnl_seqno",ix))+tot
	;
	; Receiver
	if type="R" set res("data","total")=-$$FUNC^%HD($$^%PEEKBYNAME("upd_proc_local_struct.read_jnl_seqno"))+$$FUNC^%HD($$^%PEEKBYNAME("recvpool_ctl_struct.jnl_seqno"))
	;
getBacklogQuit
	quit *res
	;
	;
getLogFileList()
	new res,ix
	new level
	;
	set level=$zlevel
	;
	; mount error handler
	new $etrap
	set $etrap="set $ecode="""",res=""INTERNAL_ERROR"" zgoto level:getLogFileListQuit"
	;
	;
	do
	. new $etrap
	. set $etrap="set $ecode="""",res(""receiver"")=""null"",res(""updater"")=""null"" quit"
	. set res("receiver","file")=$$^%PEEKBYNAME("gtmrecv_local_struct.log_file",,"S")
	. set res("updater","file")=$$^%PEEKBYNAME("upd_proc_local_struct.log_file",,"S")
	;
	for ix=0:1:15 quit:$$^%PEEKBYNAME("gtmsource_local_struct.gtmsource_pid",ix)=0  do
	. set res("sources",ix+1,"file")=$$^%PEEKBYNAME("gtmsource_local_struct.log_file",ix,"S")
	. set res("sources",ix+1,"source")=$$^%PEEKBYNAME("gtmsource_local_struct.secondary_instname",ix,"S")
	;
getLogFileListQuit
	quit *res
	;
	;
fullInfo()
	new res,replData
	new level
	;
	set level=$zlevel
	;
	; mount error handler
	new $etrap
	set $etrap="set $ecode="""",res=""NO_REPL"" zgoto level:fullInfoQuit"
	;
	; instance file
	set *replData=$$getInstanceFile()
	if $data(replData)=0 goto fullInfoQuit
	kill replData("data","result")
	if $get(replData("result"))="OK" merge res("instanceFile")=replData("data")
	;
	if $data(res("instanceFile","header")) do
	. ; health
	. kill replData
	. set *replData=$$getHealth()
	. merge res("health")=replData
	. ;
	. ; backlog source
	. kill replData
	. set *replData=$$getBacklog("S")
	. merge res("backlog","source")=replData
	. ;
	. ; backlog receiver: is this RootPrimary
	. kill replData
	. set *replData=$$getBacklog("R")
	. merge res("backlog","receiver")=replData
	;
	; Log files
	set *logData=$$getLogFileList()
	merge res("logFiles")=logData
	;
fullInfoQuit
	quit *res
	;
	;
crawler(body)
	new res,instanceData,ix,instanceName,serverInfo,iy,iz,secondaryFound
	new curlHttpCode,curlResponse,curlPath,curlPayload,curlRet,jsonErr,error,socket
	new logFiles,logInstanceName,file,fileList,level,line
	new shellResult,ret,tail,cmd,useFileList,quit,logData
	;
	set level=$zlevel
	;
	set instanceName=$$^%PEEKBYNAME("repl_inst_hdr.inst_info.this_instname",,"S")
	;
	; always return the log files
	set *logData=$$getLogFileList()
	merge res(instanceName,"logFiles")=logData
	;
	; Get local data first
	if body("responseType")="F" do
	. ;*********************
	. ;FULL: all data
	. ;*********************
	. set *instanceData=$$fullInfo
	. merge res(instanceName)=instanceData
	;
	if $zfind(body("responseType"),"B") do
	. ;*********************
	. ; BACKLOG
	. ;*********************
	. ; Source
	. set *instanceData=$$getBacklog("S")
	. merge res(instanceName,"backlog","source")=instanceData
	. ;
	. ; Receiver
	. set *instanceData=$$getBacklog("R")
	. merge res(instanceName,"backlog","receiver")=instanceData
	;
	if $zfind(body("responseType"),"H") do
	. ;*********************
	. ; HEALTH
	. ;*********************
	. set *instanceData=$$getHealth
	. merge res(instanceName,"health")=instanceData
	;
	if $zfind(body("responseType"),"L:") do
	. ;*********************
	. ; LOG FILES
	. ;*********************
	. set logInstanceName=$zpiece(body("responseType"),":",2)
	. set tail=+$zpiece(body("responseType"),":",3)
	. ; check if instance is the correct one
	. ; if ok, fetch and quit (skip crawl)
	. if instanceName=logInstanceName do  quit
	. . ;
	. . set *fileList=$$getLogFileList()
	. . set tail=$get(body("logFiles","tail"),0)
	. . set cmd=$select(tail=0:"cat ",1:"tail "_tail_" ")
	. . set useFileList=($get(body("logFiles","files",1))'="")
	. . ; sources
	. . set ix="" for  set ix=$order(fileList("sources",ix)) quit:ix=""  do
	. . . set file=fileList("sources",ix,"file")
	. . . if useFileList,$$isFileInList($name(body("logFiles","files")),file)=0 quit
	. . . kill shellResult
	. . . set ret=$$runShell^%ydbguiUtils(cmd_file,.shellResult)
	. . . merge fileList("sourcesData",fileList("sources",ix,"source"))=shellResult
	. . ;
	. . ; receiver
	. . if $get(fileList("receiver","file"))'="",fileList("receiver","file")'="null" do
	. . . set file=fileList("receiver","file")
	. . . if useFileList,$$isFileInList($name(body("logFiles","files")),file)=0 quit
	. . . kill shellResult
	. . . set ret=$$runShell^%ydbguiUtils(cmd_file,.shellResult)
	. . . merge fileList("receiverData","file")=shellResult
	. . ;
	. . ; updater
	. . if $get(fileList("updater","file"))'="",fileList("updater","file")'="null" do
	. . . set file=fileList("updater","file")
	. . . if useFileList,$$isFileInList($name(body("logFiles","files")),file)=0 quit
	. . . kill shellResult
	. . . set ret=$$runShell^%ydbguiUtils(cmd_file,.shellResult)
	. . . merge fileList("updaterData","file")=shellResult
	. . ;
	. . kill fileList("sources"),fileList("receiver"),fileList("updater")
	. . merge res(instanceName,"logFiles")=fileList
	;
	;*************************
	; Start crawling if needed
	;*************************
	if $$^%PEEKBYNAME("gtmsource_local_struct.secondary_instname",0,"S")="" goto crawlQuit
	;
	for ix=0:1:15 set secondary=$$^%PEEKBYNAME("gtmsource_local_struct.secondary_instname",ix,"S") quit:secondary=""  do
	. if $$^%PEEKBYNAME("gtmsource_local_struct.mode",ix)=0 quit
	. ; make remote calls
	. ;
	. set secondaryFound=0
	. set iy="" for  set iy=$order(body("servers",iy)) quit:iy=""  do
	. . if body("servers",iy,"instance")=secondary do
	. . . set secondaryFound=1
	. . . set curlPath=body("servers",iy,"protocol")_"://"_body("servers",iy,"host")_":"_body("servers",iy,"port")_"/api/replication/topology"
	. . . ;
	. . . ; process the body for forwarding
	. . . do encode^%ydbwebjson("body","curlPayload","jsonErr")
	. . . if $data(jsonErr) do  quit
	. . . . set error="Error encoding CURL payload: "_jsonErr(1)
	. . . . set res(instanceName,"children",secondary,"ERROR")=error
	. . . ;
	. . . set (iz,curlPayload)=""
	. . . for  set iz=$order(curlPayload(iz)) quit:iz=""  set curlPayload=curlPayload_curlPayload(iz)
	. . . ;
	. . . ; mount error handler
	. . . new $etrap
	. . . set $etrap="set $ecode="""",res(instanceName,""children"",secondary,""ERROR"")=""curl:""_curlHttpCode,res(instanceName,""children"",secondary,""ERROR_DATA"")=curlResponse quit"
	. . . ;
	. . . ; init the curl library
	. . . do &libcurl.init
	. . . do &libcurl.TLSVerifyPeer($select(body("servers",iy,"verifyCertificates")="false":0,1:1))
	. . . if body("servers",iy,"tlsFileLocation")'="" do  quit:ret=""
	. . . . set ret=$zsearch(body("servers",iy,"tlsFileLocation"),-1)
	. . . . if ret="" set res(instanceName,"children",secondary,"ERROR")="tls_file_not_found",res(instanceName,"children",secondary,"ERROR_DATA")=body("servers",iy,"tlsFileLocation") quit
	. . . . do &libcurl.serverCA(body("servers",iy,"tlsFileLocation"))
	. . . do &libcurl.conTimeoutMS(body("timeouts","onConnect"))
 	. . . ;
	. . . ; execute the CURL call
	. . . set curlRet=$&libcurl.do(.curlHttpCode,.curlResponse,"POST",curlPath,curlPayload,"application/json",body("timeouts","onResponse")/1000)
	. . . ;
	. . . ; curl library cleanup
	. . . do &libcurl.cleanup
	. . . ;
	. . . ; check curl return values
	. . . if curlHttpCode=500 do  quit
	. . . . set error="status_500"
	. . . . set res(instanceName,"children",secondary,"ERROR")=error
	. . . . set res(instanceName,"children",secondary,"ERROR_data")=curlResponse
	. . . ;
	. . . if curlHttpCode'=200 do  quit
	. . . . set error="status_"_curlHttpCode
	. . . . set res(instanceName,"children",secondary,"ERROR")=error
	. . . . set res(instanceName,"children",secondary,"ERROR_data")=$get(curlResponse)
	. . . ;
	. . . ; parse reponse
	. . . do decode^%ydbwebjson("curlResponse","response","jsonErr")
	. . . if $data(jsonErr) do  quit
	. . . . set error="Error parsing CURL response: "_jsonErr(1)
	. . . . set res(instanceName,"children",secondary,"ERROR")=error
	. . . ;
	. . . kill response(secondary,"result")
	. . . ;
	. . . merge response(secondary,"net")=body("servers",iy)
	. . . merge res(instanceName,"children")=response
	;
crawlQuit
	quit *res
	;
	;
isFileInList(list,file)
	new ret,ix
	;
	set ret=0,ix=""
	for  set ix=$order(@list@(ix)) quit:ix=""!(ret)  do
	. if file=@list@(ix) set ret=1
	;
	quit ret
	;
	;
lBacklog()
	new res,instanceName,instanceData
	;
	set instanceName=$$^%PEEKBYNAME("repl_inst_hdr.inst_info.this_instname",,"S")
	;
	; Source
	set *instanceData=$$getBacklog("S")
	merge res("backlog","source")=instanceData
	;
	; Receiver
	set *instanceData=$$getBacklog("R")
	merge res("backlog","receiver")=instanceData
	;
	quit *res
	;
