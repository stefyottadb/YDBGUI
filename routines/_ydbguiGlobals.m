%ydbguiGlobals ; Globals core; 05-07-2021
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	quit
	;
; ****************************************************************
; find(mask)
;
; PARAMS:
; mask (string) mask without ^
; size (number) number of records to return, 0 will return all
;
; RETURNS
; JDOM res array with globals list
; ****************************************************************
find(mask,size)
	new res,gname,cnt,gnameStart,gnameStop
	;
	set cnt=0
	set gname="^"_mask
	if $zlength(mask)<2 do
	. set gnameStart="^%"
	. set gnameEnd="^zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
	. ;
	. if $data(^%) set res("data",$increment(cnt))="%"
	. ;
	else  do
	. set gnameStart=$zextract(gname,1,$zlength(gname)-1)
	. set gnameEnd=$zextract(gname,1,$zlength(gname)-1)_"zzzzzzzzzz"
	;
	for  set gnameStart=$order(@gnameStart) quit:gnameStart=""!(gnameStart]gnameEnd)  do  quit:(size>0)&(cnt>=size)
	. if $zlength(mask)<2 set res("data",$increment(cnt))=gnameStart
	. else  set:$zextract(gnameStart,2,$zlength(mask)+1)=mask res("data",$increment(cnt))=$zextract(gnameStart,2,99)
	;
	set res("result")="OK"
	;
	quit *res
	;
	;
; ****************************************************************
; traverse(params)
;
; PARAMS:
; params			byRef
;
; RETURNS
; JDOM res array with globals list
; ****************************************************************
traverse(params)
	new subs,cnt,hasStar,returnAllNodes,pathSubs,recordCount,maxRecords,quitNow,res,subsCount
	new bashPath,subStr,found,subNoQuotes,offsetSubs,offsetPath,continue,gname,direction,skipCounter,upOffset
	new bufferSize
	;
	; init variables
	set (basePath,gname)=params("namespace","global")
	set offsetPath=$get(params("offset"))
	set subStr=""
	set maxRecords=params("size")
	set (recordCount,quitNow,continue,skipCounter,bufferSize)=0
	set returnAllNodes=$select($get(params("returnAll"))="true":1,1:0)
	merge subs=params("namespace","subscripts")
	set subsCount=$order(subs(""),-1)
	set hasStar=$select($get(subs($order(subs(""),-1),"type"))="*":1,1:0)
	set direction=params("direction")
	set upOffset=+$get(params("upOffset"))
	;
	; disable badchar to avoid errors from binary data
	view "NOBADCHAR"
	; detect comma or range
	set (found,level)=0
	set cnt="" for  set cnt=$order(subs(cnt)) quit:cnt=""!(found)  do
	. if subs(cnt,"type")=","!(subs(cnt,"type")=":") set found=1
	;
	; if reverse, goto reverse routine
	if params("mode")="tail" goto traverseWalkerR
	;
	; adjust subs for $order
	do adjustValueByRange(.subs)
	;
	; Creates the base path
	if $zlength(subStr) set basePath=basePath_"("_$zextract(subStr,1,$zlength(subStr)-1)_")"
	;
	; Check the root node
	if (returnAllNodes=0&($data(@basePath)=1!($data(@basePath)=11)))!(returnAllNodes=1) do
	. set skipCounter=skipCounter+1
	. if ((offsetPath=""!(continue)!((direction=-1))&(skipCounter>upOffset))&(level>=subsCount))!((subsCount=1)&(subs(1,"type")="*")) do
	. . ; log the record
	. . do logRecord(basePath)
	;
	; start walking
	do traverseWalker(basePath)
	;
	; adjust record count for continue mode
	if offsetPath'="" do
	. set recordCount=recordCount-1
	. kill res("data","list",$order(res("data","list","")))
	;
	; update counter
	set res("data","recordCount")=recordCount
	;
	quit *res
	;
traverseWalker(path)
	new ix,quitAll
	;
	set quitAll=0
	set level=level+1
	;
	; if fixed value, append to path and continue traversing
	if $get(subs(level,"value"))'="",$get(subs(level,"type"))="V" do  goto traverseWalkerQuit
	. set path=$name(@path@(subs(level,"value")))
	. if (offsetPath'=""&(offsetPath=path)) set continue=1			; do we need to continue
	. do traverseWalker(path)
	. ;
	. if (returnAllNodes=0&($data(@path)=1!($data(@path)=11)))!(returnAllNodes=1) do  quit:quitNow
	. . set skipCounter=skipCounter+1
	. . if ((offsetPath=""!(continue)!(direction=-1)&(skipCounter>upOffset))!(direction=0))&(level>=subsCount) do
	. . . ; log the record
	. . . do logRecord(path)
	. . . ; Check record limits and quit if done
	. . . if direction=0 quit
	. . . if recordCount=$select(continue:maxRecords+1,1:maxRecords) set quitNow=1
	;
	if $get(subs(level,"value"))'="" set ix=subs(level,"value")
	else  set ix=""
	;
	for  set ix=$order(@path@(ix)) quit:ix=""!(quitAll)!(quitNow)  do
	. if $get(subs(level,"type"))="V",$get(subs(level,"value"))'="",(ix>subs(level,"value")!(ix]subs(level,"value"))) set quitAll=1 quit
	. if $get(subs(level,"type"))=":",$get(subs(level,"valueEnd"))'="",(+ix>+subs(level,"valueEnd")!(ix]subs(level,"valueEnd"))) set quitAll=1 quit
	. if $data(subs(level))=0,hasStar=0 set quitAll=1 quit
	. ;
	. set newPath=$name(@path@(ix))
	. if (offsetPath'=""&(offsetPath=newPath)) set continue=1
	. if (returnAllNodes=0&($data(@newPath)=1!($data(@newPath)=11)))!(returnAllNodes=1) do  quit:quitNow
	. . set skipCounter=skipCounter+1
	. . if ((offsetPath=""!(continue)!(direction=-1)&(skipCounter>upOffset))!(direction=0))&(level>=subsCount) do
	. . . ; log the record
	. . . do logRecord(newPath)
	. . ;
	. . ; Check record limits and quit if done
	. . if direction=0 quit
	. . if recordCount=$select(continue:maxRecords+1,1:maxRecords) set quitNow=1
	. ;
	. ; continue walking if ( ( * or comma or next level has expression ) && $data>9 )
	. if ($get(subs(level,"type"))=","!(hasStar)!($get(subs(level+1,"type"))'="")),$data(@newPath)>9 do traverseWalker(newPath)
	;
traverseWalkerQuit
	set level=level-1
	;
	quit
	;
	;
logRecord(path)
	; log the record
	set recordCount=recordCount+1
	set res("data","list",recordCount,"path")=path
	set res("data","list",recordCount,"$data")=$data(@path)
	set res("data","list",recordCount,"global")=gname
	; append the subscripts
	do extractSubs(path,$name(res("data","list",recordCount,"subscripts")))
	; and the $data and value
	if $data(@path)=1!($data(@path)=11) do
	. if $length($get(@path))>params("dataSize") set res("data","list",recordCount,"value")=$extract($get(@path),1,params("dataSize")),res("data","list",recordCount,"hasMore")="true"
	. else  set res("data","list",recordCount,"value")=$get(@path),res("data","list",recordCount,"hasMore")="false"
	else  set res("data","list",recordCount,"value")="null",res("data","list",recordCount,"hasMore")="false"
	;
	if direction=0 do
	. if bufferSize<maxRecords set bufferSize=bufferSize+1
	. else  kill res("data","list",recordCount-bufferSize)
	;
	quit
	;
	;
extractSubs(path,subs)
	; Creates the subs node by parsing the path
	quit:$qlength(path)=0
	;
	new ix
	;
	for ix=1:1:$qlength(path) set @subs@(ix)=$qsubscript(path,ix)
	;
	quit
	;
	;
adjustValueByRange(subs)
	new ix,lastChar
	;
	set ix="" for  set ix=$order(subs(ix)) quit:ix=""  do
	. quit:subs(ix,"type")'=":"
	. ;
	. if $get(subs(ix,"type1"))="N"!(($get(subs(ix,"type1"))="")&($get(subs(ix,"type2"))="N")) do
	. . if subs(ix,"value")="" set subs(ix,"value")=0 quit
	. . set subs(ix,"value")=subs(ix,"value")-0.000001
	. else  do
	. . if subs(ix,"value")="" set subs(ix,"value")=" " quit
	. . set lastChar=$zextract(subs(ix,"value"),$zlength(subs(ix,"value")))
	. . set lastChar=$zchar($zascii(lastChar)-1)
	. . set subs(ix,"value")=$zextract(subs(ix,"value"),1,$zlength(subs(ix,"value"))-1)_lastChar_"}"
	;
	quit
	;
	;
;*******************************	
; traverseWalkerR (reverse)
;*******************************	
traverseWalkerR
	new val,quitAll,glvn,result,glvnSubs
	;
	;start by jumping at the end 
	set glvn=params("upOffset")
	set quitAll=0
	;
	; quit if nothing found (empty global)
	set glvn=$query(@glvn,-1)
	if glvn="" goto traverseWalkerRquit
	;
	if $$compareSubs(.subs,glvn,.glvnSubs)=1 do
	. ; include record in list
	. do logRecordR(glvn,.glvnSubs)
	;
	for  quit:glvn=""  set glvn=$query(@glvn,-1) do  quit:quitAll
	. if glvn="" set quitAll=1 quit
	. set result=$$compareSubs(.subs,glvn,.glvnSubs)
	. if result do
	. . ; include record in list
	. . do logRecordR(glvn,.glvnSubs)
	. . if recordCount>maxRecords set quitAll=1
	. else  set quitAll=1
	;
traverseWalkerRquit
	set res("data","recordCount")=recordCount
	;
	quit *res
	;
	;
;*******************************	
; logRecordR(subs,glvn)
;*******************************	
logRecordR(glvn,glvnSubs)
	new ix
	;
	set recordCount=recordCount+1
	set ix=maxRecords-recordCount+1
	set res("data","list",ix,"path")=glvn
	set res("data","list",ix,"$data")=$data(@glvn)
	set res("data","list",ix,"global")=gname
	; and the $data and value
	if $data(@glvn)=1!($data(@glvn)=11) do
	. if $length($get(@glvn))>params("dataSize") set res("data","list",ix,"value")=$extract($get(@glvn),1,params("dataSize")),res("data","list",ix,"hasMore")="true"
	. else  set res("data","list",ix,"value")=$get(@glvn),res("data","list",ix,"hasMore")="false"
	else  set res("data","list",ix,"value")="null",res("data","list",ix,"hasMore")="false"
	set res("data","list",ix,"value")=@glvn
	;
	merge res("data","list",ix,"subscripts")=glvnSubs
	;		
	quit
	;
	;		
;*******************************	
; compareSubs(subs,glvn)	
;*******************************	
compareSubs(subs,glvn,glvbSubs)	
	new result,ix,sub,glvnSub,quitAll,tmp
	;
	set *glvnSubs=$$splitSubs(glvn)
	set (result,quitAll)=0
	;
	for ix=1:1:$order(glvnSubs(""),-1) do  if quitAll goto compareSubsQ
	. kill sub
	. merge sub=subs(ix)
	. set glvnSub=glvnSubs(ix)
	. ;
	. ; if there is no sub, check the hasStar flag
	. if $data(sub)=0 set (result,quitAll)=hasStar quit
	. ;
	. ; if there is a star, go to next subscript
	. if sub("type")="*" set result=1 quit
	. ;
	. ; Value, check if it matches
	. if sub("type")="V" do
	. . set result=(sub("value")=glvnSub),quitAll='result quit
	. ;
	. ; Range
	. if sub("type")=":" do
	. . set result=0
	. . ;
	. . ; lower bound
	. . if $get(sub("type1"))=""!(sub("type1")="S") set result=(glvnSub]sub("value")) if result=0 set quitAll=1 quit
	. . else  set result=(glvnSub>=sub("value")) if result=0 set quitAll=1 quit
	. . ;
	. . ; upper bound
	. . if sub("type2")="S" do
	. . . set tmp=sub("valueEnd")_"}"
	. . . set result=(tmp]glvnSub) if result=0 set quitAll=1
	. . else  set result=(glvnSub<=sub("valueEnd")) if result=0 set quitAll=1 
	;
compareSubsQ
	quit result	
	;
	;
;*******************************	
; splitSubs(glvn)	
;*******************************	
splitSubs(glvn)
	new subs,ix,sub
	;
	if $zfind(glvn,"(")=0 goto splitSubsQ
	set glvn=$extract(glvn,$zfind(glvn,"("),$zfind(glvn,")")-2)
	set *subs=$$SPLIT^%MPIECE(glvn,",")
	;
	; adjust strings
	for ix=1:1:$order(subs(""),-1) do
	. set sub=subs(ix)
	. if $zextract(sub,1)="""" set sub=$zextract(sub,2,$zlength(sub)-1),subs(ix)=sub
	;
splitSubsQ	
	quit *subs
	;
	;
; ****************************************************************
; globalsData(globalName)
;
; PARAMS:
; globalName (string) global name, including the ^
;
; RETURNS
; {data: $data} OR {data: -1) if error
; ****************************************************************
globalData(globalName)
	new res
	;
	set res("data")=-1
	set globalName=$ztranslate(globalName,"_","%")
	new $etrap
	set $etrap="goto globalDataQuit"
	;
	set res("data")=$data(@globalName)
	;
globalDataQuit
	set $ecode=""
	set res("result")="OK"
	;
	quit *res
	;
	;
; ****************************************************************
; getGlobals(regionsData)
; ;
; PARAMS:
; regionsData			array
; RETURNS:
; nothing
; ****************************************************************
getGlobals(regionsName)
	new tmp,gldFilename,region,dbFilename,gbl,cnt,global,globalIx,regionData,res,globals,globalByRegion,regionIx,regionFound
	;
	do getRegionStruct^%ydbguiRegions(regionName,.regionData,.warnings)
	;
	; extract the global list
	kill cnt
	set dbFilename=regionData("dbFile","data",1,"FILE_NAME")
	if dbFilename'="",regionData("dbFile","flags","fileExist")="true",regionData("dbFile","flags","fileBad")="false" do
	. set gldFilename=$$getGld(regionName,dbFilename)
	. new $zgbldir
	. set $zgbldir=gldFilename
	. if $data(^%) set globals("data",$increment(cnt),"name")="^%"
	. set gbl="^%" for  set gbl=$order(@gbl) quit:gbl=""  set globals("data",$increment(cnt),"name")=gbl
	;
	; detect and mark zombies
	if $data(globals("data")) do
	. set globalIx="" for  set globalIx=$order(globals("data",globalIx)) quit:globalIx=""  do
	. . set global=globals("data",globalIx,"name")
	. . set globalByRegion=$view("region",global)
	. . set globals("data",globalIx,"status")=$select($find(","_globalByRegion_",",","_regionsName_","):"ok",1:"zombie")
	; 
	; remove the .gld file
	open gldFilename:readonly
	close gldFilename:delete
	;
	set globals("result")="OK"
	;
	quit *globals
	;
	;
getGld(regionName,dbFilename)
	new gldFilename,gdeFilename,tmp
	;
	set tmp=$ZTRNLNM("ydb_tmp")
	set:tmp="" tmp="/tmp/"
	set gldFilename=tmp_regionName_".gld",gdeFilename=tmp_regionName_".gde"
	if $zsearch(gldFilename,-1)="" do
	. open gdeFilename:newversion
	. use gdeFilename
	. write "change -segment DEFAULT -file="""_dbFilename_"""",!
	. write "exit",!
	. close gdeFilename
	. ;
	. open "pipe":(shell="/bin/bash":command="ydb_gbldir="""_gldFilename_""" $ydb_dist/yottadb -r GDE @"_gdeFilename)::"pipe"
	. use "pipe"
	. new x for i=1:1 read x(i) quit:$zeof
	. close "pipe"
	. if $zclose'=0 write "An error occurred... Contact YottaDB Support.",! zwrite x
	. ;
	. ; delete the gld file
	. open gdeFilename
	. close gdeFilename:delete
	. ;
	;
	quit gldFilename
	;
	;
; ****************************************************************
; getGlobalSize(globalName)
; ;
; PARAMS:
; regionsData			array
; RETURNS:
; array
; ****************************************************************
getGlobalSize(type,param,region)
	new res,cmd,shellResult
	new regionData,warnings,dbFilename,gldFilename
	;
	set cmd="$ydb_dist/mupip SIZE -heuristic="_type_","_$select(type="scan":"level",1:"samples")_"="_param_" -region=""DEFAULT"""
	;
	; recreate the .gld file
	do getRegionStruct^%ydbguiRegions(region,.regionData,.warnings)
	set dbFilename=regionData("dbFile","data",1,"FILE_NAME")
	set gldFilename=$$getGld(region,dbFilename)
	;
	; execute the MUPIP SIZE in specific database
	set ret=$$runShell^%ydbguiUtils(cmd,.shellResult,"/bin/sh",gldFilename)
	;
	; MUPIP returns 0 (all ok), all others are errors
	if ret'=0 do  goto getGlobalSizeQuit
	. ; internal errors
	. set res("result")="ERROR"
	. set res("error","description")="An error occured while executing SIZE: "_ret
	. merge res("error","dump")=shellResult
	;
	set res("result")="OK"
	merge res("data","dump")=shellResult
	;
getGlobalSizeQuit	
	; remove the .gld file
	open gldFilename:readonly
	close gldFilename:delete
	;
	quit *res
	;
	;
	;
