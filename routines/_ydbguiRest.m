%ydbguiRest ; REST handlers; 05-07-2021
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	quit
	;
; ****************************************************************
; getAll
;
; Related URL: GET api/dashboard/getAll
;
; ****************************************************************
getDashboard
	;HTTPREADWRITE
	new res,ret,cnt,regionsData,file,files,region,devices
	new lastIndex,mountpoint,ldevices,ldevice,warnings,list,region,regions
	new file,fbuffer,envVars,mountpoint,manifestName,manifest,replData
	new octo,gldInfo,gldData,shellResult,clientConfig
	;
	set *res=httprsp
	;
	set warnings=0
	;
	; get ydb release
	set res("data","ydb_version")=$zpiece($zyrelease," ",2)
	;
	; get zroutines
	set res("data","zroutines")=$zroutines
	;
	if $ztrnlnm("ydb_gbldir")="" do  goto getDashboardQuit
	. set res("result")="ERROR"
	. set res("error","code")=-1
	. set res("error","description")="No GLD file was specified in the environment variable: ydb_gbldir"
	;
	; get system info
	set res("data","systemInfo","zroutines")=$zroutines
	set res("data","systemInfo","gld")=$zgbldir
	set res("data","systemInfo","chset")=$zchset
	; Encryption library
	set res("data","systemInfo","encryptionLibrary")=$select($zsearch("$ydb_dist/plugin/libgtmcrypt.so",-1)="":"false",1:"true")
	;
	; get env vars
	set file="/proc/self/environ"
	open file:readonly use file read fbuffer close file
	set *envVars=$$SPLIT^%MPIECE(fbuffer,$zchar(0))
	;
	set cnt="" for  set cnt=$order(envVars(cnt)) quit:cnt=""  do
	. set res("data","systemInfo","envVars",cnt,"name")=$zpiece(envVars(cnt),"=")
	. set res("data","systemInfo","envVars",cnt,"value")=$zpiece(envVars(cnt),"=",2,99)
	;
	; get plugins information
	set res=$zsearch("",-1)
	for  set file=$zsearch("$ydb_dist/plugin/o/*.so") quit:file=""  do
	. kill manifest
	. set files($increment(files),"name")=$zparse(file,"name")
	. ;
	. ; search for related manifest file
	. set manifestFile=$zparse(file,"DIRECTORY")_$zparse(file,"NAME")_".manifest.json"
	. if $zsearch(manifestFile,-1)'="" set *manifest=$$getManifestFile^%ydbguiUtils(manifestFile)
	. if $data(manifest)<9 do
	. . ; no manifest file
	. . set last=$order(files(""),-1)
	. . set files(last,"description")="Library file: <br>"_file
	. . set files(last,"version")="n/a"
	. . set files(last,"vendor")="n/a"
	. else  do
	. . ; manifest file found
	. . set last=$order(files(""),-1)
	. . set files(last,"description")=$get(manifest("description"))
	. . set files(last,"version")=$get(manifest("version"))
	. . set files(last,"vendor")=$get(manifest("vendor"))
	;
	; check node.js presence
	set ret=$$runShell^%ydbguiUtils("node --version",.shellResult)
	set res("data","node","status")=$select(ret!($get(shellResult(1))=""):"N/A",1:shellResult(1))
	;
	merge res("data","systemInfo","plugins")=files
	zkill res("data","systemInfo","plugins")
	;
	; check gld file existance
	set res("data","gld","exist")=$select($zsearch($zgbldir,-1)="":"false",1:"true")
	if res("data","gld","exist")="false" goto getDashboardQuit
	;
	; collect gld file informations for Environment
	set gldData("path")=$zgbldir
	set *gldInfo=$$validateGld^%ydbguiGde(.gldData)
	merge res("data","environment")=gldInfo("data")
	set res("data","environment","cwd")=$zdirectory
	set res("data","environment","gld")=$zgbldir
	;
	; enumerate regions
	do enumRegions^%ydbguiGde(.regions)
	;
	; reorganize the array so that numbering is correct
	set region="" for  set region=$order(regions(region)) quit:region=""  do
	. kill regionData
	. do getRegionStruct^%ydbguiRegions(region,.regionData,.warnings)
	. merge regionsData(region)=regionData
	;
	merge res("data","regions")=regionsData
	;
	; compute devices
	set (region,lastIndex)=0
	for  set region=$order(regionsData(region)) quit:region=""  do
	. if $get(regionsData(region,"dbFile","flags","mountpoint"))'="" do
	. . set lastIndex=lastIndex+1
	. . set devices(regionsData(region,"dbFile","flags","mountpoint"),lastIndex,"region")=region
	. . set devices(regionsData(region,"dbFile","flags","mountpoint"),lastIndex,"file")=$get(regionsData(region,"dbFile","flags","file"))
	. . set devices(regionsData(region,"dbFile","flags","mountpoint"),lastIndex,"device")=$get(regionsData(region,"dbFile","flags","device"))
	. . set devices(regionsData(region,"dbFile","flags","mountpoint"),lastIndex,"fsBlockSize")=$get(regionsData(region,"dbFile","flags","fsBlockSize"))
	. . set devices(regionsData(region,"dbFile","flags","mountpoint"),lastIndex,"deviceId")=$get(regionsData(region,"dbFile","flags","deviceId"))
	. . set devices(regionsData(region,"dbFile","flags","mountpoint"),lastIndex,"iNodesTotal")=$get(regionsData(region,"dbFile","flags","iNodesTotal"))
	. . set devices(regionsData(region,"dbFile","flags","mountpoint"),lastIndex,"iNodesFree")=$get(regionsData(region,"dbFile","flags","iNodesFree"))
	. . ;
	. if $get(regionsData(region,"journal","flags","mountpoint"))'="" do
	. . set lastIndex=lastIndex+1
	. . set devices(regionsData(region,"journal","flags","mountpoint"),lastIndex,"region")=region
	. . set devices(regionsData(region,"journal","flags","mountpoint"),lastIndex,"file")=$get(regionsData(region,"journal","flags","file"))
	. . set devices(regionsData(region,"journal","flags","mountpoint"),lastIndex,"device")=$get(regionsData(region,"journal","flags","device"))
	. . set devices(regionsData(region,"journal","flags","mountpoint"),lastIndex,"fsBlockSize")=$get(regionsData(region,"journal","flags","fsBlockSize"))
	. . set devices(regionsData(region,"journal","flags","mountpoint"),lastIndex,"deviceId")=$get(regionsData(region,"journal","flags","deviceId"))
	. . set devices(regionsData(region,"journal","flags","mountpoint"),lastIndex,"iNodesTotal")=$get(regionsData(region,"journal","flags","iNodesTotal"))
	. . set devices(regionsData(region,"journal","flags","mountpoint"),lastIndex,"iNodesFree")=$get(regionsData(region,"journal","flags","iNodesFree"))
	;
	; reset the first leg
	set mountpoint=0
	for  set mountpoint=$order(devices(mountpoint)) quit:mountpoint=""  do
	. set cnt=$order(devices(mountpoint,"")) if cnt>1 do
	. . merge devices(mountpoint,1)=devices(mountpoint,cnt)
	. . kill devices(mountpoint,cnt)
	;
	set (lastIndex,cnt)=0
	for  set cnt=$order(devices(cnt)) quit:cnt=""  do
	. set lastIndex=lastIndex+1
	. merge ldevices(lastIndex,"usedBy")=devices(cnt)
	. set ldevice=$get(devices(cnt,1,"device"))
	. ; piecing out the device information returned by the shell
	. ; "type totalBlocks usedBlocks freeBlocks percentUsed mountPoint"
	. set ldevices(lastIndex,"type")=$zpiece(ldevice," ",1)
	. set ldevices(lastIndex,"totalBlocks")=$zpiece(ldevice," ",2)
	. set ldevices(lastIndex,"usedBlocks")=$zpiece(ldevice," ",3)
	. set ldevices(lastIndex,"freeBlocks")=$zpiece(ldevice," ",4)
	. set ldevices(lastIndex,"percentUsed")=$zextract($p(ldevice," ",5),1,$zlength($zpiece(ldevice," ",5))-1)
	. set ldevices(lastIndex,"mountPoint")=$zpiece(ldevice," ",6)
	. set ldevices(lastIndex,"fsBlockSize")=devices(cnt,1,"fsBlockSize")
	. set ldevices(lastIndex,"deviceId")=devices(cnt,1,"deviceId")
	. set ldevices(lastIndex,"iNodesTotal")=devices(cnt,1,"iNodesTotal")
	. set ldevices(lastIndex,"iNodesFree")=devices(cnt,1,"iNodesFree")
	;
	; remove unused data by the client
	set cnt=0 for  set cnt=$order(ldevices(cnt)) quit:cnt=""  do
	. set lastIndex="" for  set lastIndex=$order(ldevices(cnt,"usedBy",lastIndex)) quit:lastIndex=""  do
	. . kill ldevices(cnt,"usedBy",lastIndex,"device")
	;
	merge res("data","devices")=ldevices
	;
	;************************************
	; append replication information
	;************************************
	;
	set *replData=$$fullInfo^%ydbguiReplication
	if $data(replData) merge res("data","replication")=replData
	;
	;************************************
	; octo status
	;************************************
	; enable only if this instance is not replicated or if it is R/W (primary or R/W supplementary)
	if $data(res("data","replication","instanceFile","header"))=0!(res("data","replication","instanceFile","flags","isReadWrite")) do
	. set *octo=$$detect^%ydbguiOcto()
	. merge res("data","octo")=octo
	;
getDashboardQuit
	if $get(httpoptions("client-config"))="" set res("data","webServer","prefsConfigData")=""
	else  do
	. set *clientConfig=$$getConfigurationFile^%ydbguiUtils(httpoptions("client-config"))
	. if $get(clientConfig)="" zkill clientconfig
	. merge res("data","webServer","prefsConfigData")=clientConfig
	. set res("data","webServer","prefsConfigFile")=httpoptions("client-config")
	;
	; all ok, set status
	set res("result")=$select($data(warnings)>1:"WARNING",$get(res("result"))="ERROR":"ERROR",1:"OK")
	if $data(warnings)>1 merge res("data","warnings")=warnings zkill res("data","warnings")
	;
	; append readonly status
	do appendRwStatus
	;
	quit
	;
	;
; ****************************************************************
; getRegion
;
; Related URL: GET api/regions/{region}
;
; ****************************************************************
getRegion
	new res,ret,cnt,regionName,regionData,warnings
	;
	set *res=HTTPRSP
	set warnings=0
	;
	set regionName=$zconvert($get(HTTPARGS("region")),"u")
	;
	do getRegionStruct^%ydbguiRegions(regionName,.regionData,.warnings)
	merge res("data")=regionData
	;
	; all ok, set status
	set res("result")=$select($data(warnings)>1:"WARNING",1:"OK")
	if $data(warnings)>1 merge res("data","warnings")=warnings zkill res("data","warnings")
	;
	quit
	;
	;
; ****************************************************************
; getGlobals
;
; Related URL: GET api/globals/by-regions/{region}
;
; ****************************************************************
getGlobals
	new res,ret,cnt,regionName,regionData,warnings,globals
	;
	set *res=HTTPRSP
	;
	if $get(HTTPARGS("region"))="" do  goto getGlobalsQuit
	. set res("result")="ERROR"
	. set res("error","description")="You need to supply a region"
	;
	set regionName=$zconvert($get(HTTPARGS("region")),"u")
	;
	; check if region exists
	do enumRegions^%ydbguiGde(.regions)
	if $data(regions(regionName))=0 do  goto getGlobalsQuit
	. set res("result")="ERROR"
	. set res("error","description")="The region "_regionName_" doesn't exist"
	;
	set *HTTPRSP=$$getGlobals^%ydbguiGlobals(regionName)
	;
	;
getGlobalsQuit
	quit
	;
	;
; ****************************************************************
; getGlobalSize
;
; Related URL: GET api/globals/size
;
; ****************************************************************
getGlobalSize
	new res
	;
	set *res=HTTPRSP
	;
	if $get(HTTPARGS("region"))="" do  goto getGlobalSizeQuit
	. set res("result")="ERROR"
	. set res("error","description")="You need to supply a region"
	;
	if $get(HTTPARGS("type"))="" do  goto getGlobalSizeQuit
	. set res("result")="ERROR"
	. set res("error","description")="You need to supply a type"
	;
	if $get(HTTPARGS("param"))="" do  goto getGlobalSizeQuit
	. set res("result")="ERROR"
	. set res("error","description")="You need to supply a param value"
	;
	set *HTTPRSP=$$getGlobalSize^%ydbguiGlobals(HTTPARGS("type"),HTTPARGS("param"),$get(HTTPARGS("region")))
	;
getGlobalSizeQuit
	quit
	;
	;
; ****************************************************************
; deleteRegion
;
; Related URL: DELETE api/regions/{region}
;
; ****************************************************************
deleteRegion
	;RW only
	quit:$$checkAuthorization=0
	;
	new res,regionName,regions,deleteFiles
	;
	set *res=HTTPRSP
	;
	; check if param exists
	set regionName=$get(HTTPARGS("region"))
	if regionName="" do  goto deleteRegionQuit
	. set res("result")="ERR"
	. set res("error","description")="The parameter ""region"" is missing or empty"
	;
	set deleteFiles=$data(HTTPARGS("deletefiles"))
	;
	set regionName=$zconvert(regionName,"u")
	;
	; check if region exists
	do enumRegions^%ydbguiGde(.regions)
	if $data(regions(regionName))=0 do  goto deleteRegionQuit
	. set res("result")="ERR"
	. set res("error","description")="The region "_regionName_" doesn't exist"
	;
	; perform the delete
	set *HTTPRSP=$$delete^%ydbguiRegions(regionName,deleteFiles)
	;
deleteRegionQuit
	quit
	;
	;
; ****************************************************************
; extendRegion
;
; Related URL: POST api/regions/{region}/extend
;
; ****************************************************************
extendRegion
	;RW only
	quit:$$checkAuthorization=0
	;
	new res,region,blocks,mupipCmd,ret,shellData,regions
	;
	set *res=HTTPRSP
	;
	; check if param region exist
	set region=$get(HTTPARGS("region"))
	if region="" do  goto extendRegionQuit
	. set res("result")="ERR"
	. set res("error","description")="The parameter ""region"" is missing or empty"
	;
	set region=$zconvert(region,"u")
	;
	; check if region exists
	do enumRegions^%ydbguiGde(.regions)
	if $data(regions(region))=0 do  goto extendRegionQuit
	. set res("result")="ERR"
	. set res("error","description")="The region "_region_" doesn't exist"
	;
	; check the blocks parameter
	set blocks=$get(HTTPARGS("blocks"))
	if +blocks=0 do  goto extendRegionQuit
	. set res("result")="ERR"
	. set res("error","description")="The parameter ""blocks"" is missing or not valid"
	;
	set mupipCmd="$ydb_dist/mupip EXTEND "_region_" -BLOCKS="_blocks
	set ret=$$runShell^%ydbguiUtils(mupipCmd,.shellData)
	;
	if ret'=0 do  goto extendRegionQuit
	. set res("result")="ERR"
	. set res("error","description")="The shell returned the following error: "_ret
	;
	set res("result")="OK"
	;
extendRegionQuit
	quit
	;
	;
; ****************************************************************
; journalSwitch
;
; Related URL: POST api/regions/{region}/journalSwitch
;
; ****************************************************************
journalSwitch
	;RW only
	quit:$$checkAuthorization=0
	;
	new res,region,turn,mupipCmd,ret,shellData
	;
	set *res=HTTPRSP
	;
	; check if param region exist
	set region=$get(HTTPARGS("region"))
	if region="" do  goto journalSwitchQuit
	. set res("result")="ERR"
	. set res("error","description")="The parameter ""region"" is missing or empty"
	;
	set region=$zconvert(region,"u")
	;
	; check if region exists
	do enumRegions^%ydbguiGde(.regions)
	if $data(regions(region))=0 do  goto journalSwitchQuit
	. set res("result")="ERR"
	. set res("error","description")="The region "_region_" doesn't exist"
	;
	; check the turn parameter
	set turn=$zconvert($get(HTTPARGS("turn")),"u")
	if turn'="ON",turn'="OFF" do  goto journalSwitchQuit
	. set res("result")="ERR"
	. set res("error","description")="The parameter ""turn"" must be either ""on"" or ""off"""
	;
	set mupipCmd="$ydb_dist/mupip SET -JOURNAL="_turn_" -region "_region
	set ret=$$runShell^%ydbguiUtils(mupipCmd,.shellData)
	;
	if ret'=0 do  goto journalSwitchQuit
	. set res("result")="ERR"
	. set res("error","description")=$select(ret=10:"The journal file couldn't be found",1:"The shell returned the following error: "_ret)
	;
	if $zfind($get(shellData(1)),"%YDB-I-JNLSTATE")!($zfind($get(shellData(4)),"%YDB-I-JNLSTATE"))!($zfind($get(shellData(1)),"%YDB-I-JNLCREATE"))!($zfind($get(shellData(1)),"%YDB-I-FILERENAME"))!($zfind($get(shellData(1)),"%YDB-I-JNLFNF")) do
	. set res("result")="OK"
	else  do
	. set res("result")="ERR"
	. set res("error","description")=$select($get(shellData(1))="":"unknown",1:shellData(1))
	;
journalSwitchQuit
	quit
	;
	;
; ****************************************************************
; journalFileSwitch
;
; Related URL: POST api/regions/{region}/journalFileSwitch
;
; ****************************************************************
journalFileSwitch
	;RW only
	quit:$$checkAuthorization=0
	;
	new res,region,turn,mupipCmd,ret,shellData
	;
	set *res=HTTPRSP
	;
	; check if param region exist
	set region=$get(HTTPARGS("region"))
	if region="" do  goto journalFileSwitchQuit
	. set res("result")="ERR"
	. set res("error","description")="The parameter ""region"" is missing or empty"
	;
	set region=$zconvert(region,"u")
	;
	; check if region exists
	do enumRegions^%ydbguiGde(.regions)
	if $data(regions(region))=0 do  goto journalFileSwitchQuit
	. set res("result")="ERR"
	. set res("error","description")="The region "_region_" doesn't exist"
	;
	; Perform the switch
	set mupipCmd="$ydb_dist/mupip SET -JOURNAL -region "_region
	set ret=$$runShell^%ydbguiUtils(mupipCmd,.shellData)
	;
	; Check for errors
	if ret'=0 do  goto journalFileSwitchQuit
	. set res("result")="ERR"
	. set res("error","description")=$select(ret=10:"The journal file couldn't be found",1:"The shell returned the following error: "_ret)
	;
	; Fill the response with shell data
	set ret="" for  set ret=$order(shellData(ret)) quit:ret=""  set res("data",ret)=shellData(ret)
	set res("result")="OK"
	;
journalFileSwitchQuit
	quit
	;
	;
; ****************************************************************
; createDb
;
; Related URL: POST api/regions/{region}/createDb
;
; ****************************************************************
createDb
	;RW only
	quit:$$checkAuthorization=0
	;
	new res,region,mupipCmd,ret,shellData
	;
	set *res=HTTPRSP
	;
	set region=$get(HTTPARGS("region"))
	if region="" do  goto createDbQuit
	. set res("result")="ERR"
	. set res("error","description")="The parameter ""region"" is missing or empty"
	;
	set mupipCmd="$ydb_dist/mupip CREATE -REGION="_region
	set ret=$$runShell^%ydbguiUtils(mupipCmd,.shellData)
	;
	if ret'=0 do
	. set res("result")="ERR"
	. set res("error","description")="The shell returned the following error: "_ret
	. merge res("error","dump")=shellData
	else  set res("result")="OK"
	;
createDbQuit
	quit
	;
	;
; ****************************************************************
; getTemplates
;
; Related URL: GET api/dashboard/getTemplates
;
; ****************************************************************
getTemplates
	new res,templates,key,value,type
	;
	set *res=HTTPRSP
	;
	; get templates
	set *templates=$$getTemplates^%ydbguiGde()
	;
	; get limits
	do ^GDEINIT
	;
	; map everything
	set key="" for  set key=$order(templates("region",key)) quit:key=""  do
	. quit:key="FILE_NAME"
	. set value=templates("region",key)
	. kill templates("region",key)
	. set templates("region",key,"value")=value
	. set templates("region",key,"min")=minreg(key)
	. set templates("region",key,"max")=maxreg(key)
	;
	for type="BG","MM" do
	. set key="" for  set key=$order(templates("segment",type,key)) quit:key=""  do
	. . set value=templates("segment",type,key)
	. . kill templates("segment",type,key)
	. . set templates("segment",type,key,"value")=value
	. . set templates("segment",type,key,"min")=$g(minseg(type,key))
	. . set templates("segment",type,key,"max")=$g(maxseg(type,key))
	;
	merge res("data")=templates
	set res("result")="OK"
	;
	quit
	;
	;
; ****************************************************************
; parseNamespace
;
; Related URL: POST api/regions/parseNamespace
;
; ****************************************************************
parseNamespace
	new body,gdeCommand,shellResult,map,regions,region,res,cnt
	;
	set *res=HTTPRSP
	;
	; get region list to ensure we have a valid name
	do enumRegions^%ydbguiGde(.regions)
	;
	; and extract the first name
	set region=$order(regions(""))
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	; check params
	if $get(body("namespace"))="" do  goto parseNamespaceQuit
	. set res("result")="ERROR"
	. set res("error","description")="The body parameter: 'namespace' is missing or empty"
	;
	; escape the quotes for the shell
	set map("""")="\"""
	set body("namespace")=$$^%MPIECE(body("namespace"),"""","\""")
	; and execute the GDE
	set gdeCommand="$ydb_dist/yottadb -r GDE  <<< "
	set gdeCommand=gdeCommand_"""add -name "_body("namespace")_" -r="_region_$zchar(10)_"quit"_$zchar(10)_""""
	set ret=$$runShell^%ydbguiUtils(gdeCommand,.shellResult,"/bin/bash")
	if ret<0 do  quit
	. ; FATAL, GDE error
	. do setError^%ydbwebutils("500","Error: "_ret_" while parsing the namespace."_$c(13,10)_"Contact YottaDB to report the error") quit:$quit "" quit
	;
	set res("result")="OK"
	set res("data","parseResult")=$get(shellResult(7))
	set:res("data","parseResult")="GDE> " res("data","parseResult")="OK"
	;
parseNamespaceQuit
	quit
	;
	;
; ****************************************************************
; validatePath
;
; Related URL: POST api/regions/validatePath
;
; ****************************************************************
validatePath
	new body,res,dir,ret,shellResult,command,stat,constDir
	new deviceInfo,path,x
	;
	set *res=HTTPRSP
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	; check params
	if $get(body("path"))="" do  goto validatePathQuit
	. set res("result")="ERROR"
	. set result("error","description")="The body parameter: 'path' is missing or empty"
	;
	; check if path exists at first
	set path=$zparse(body("path"),"DIRECTORY")
	set ret=$zsearch(path,-1)
	if ret="" do  goto validatePathQuit
	. ; error
	. set res("result")="ERROR"
	. set res("error","description")="The path doesn't exists..."
	;
	; then check if file exists
	set ret=$zsearch(body("path"),-1)
	if ret'="" do  goto validatePathQuit
	. ; error
	. set res("result")="ERROR"
	. set res("error","description")="File aready exists..."
	. set x=$&ydbposix.filemodeconst("S_IFDIR",.constDir)
	. do statfile^%ydbposix(body("path"),.stat)
	. set res("data","fileType")=$select(stat("mode")\constDir#2:"dir",1:"file")
	;
	set dir=$zparse(body("path"),"directory")
	; check permissions
	set ret=$$tryCreateFile^%ydbguiUtils(dir)
	if ret=0 do  goto validatePathQuit
	. ; error
	. set res("result")="ERROR"
	. set res("error","description")="Couldn't access the path..."
	;
	set res("data","validation")=$zsearch(dir,-1)
	set res("data","fileExist")=$zsearch(body("path"))
	;
	; get the device info to extract the block size of the device needed for ASYNCIO validation
	if res("data","validation")'="" do
	. set ret=$$runShell^%ydbguiUtils("stat -fc %s "_dir,.deviceInfo)
	. set res("data","deviceBlockSize")=$get(deviceInfo(1),0)
	else  set res("data","deviceBlockSize")=0
	;
	set res("result")="OK"
	;
validatePathQuit
	quit
	;
	;
; ****************************************************************
; addRegion
;
; Related URL: POST api/regions/add
;
; ****************************************************************
addRegion
	;RW only
	quit:$$checkAuthorization=0
	;
	new body
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	; Perform the creation
	set *HTTPRSP=$$create^%ydbguiRegions(.body)
	;
	quit
	;
	;
; ****************************************************************
; editRegion
;
; ****************************************************************
editRegion
	;RW only
	quit:$$checkAuthorization=0
	;
	new body
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	; Perform the edit
	set *HTTPRSP=$$edit^%ydbguiRegions(.body)
	;
	quit
	;
	;
; ****************************************************************
; error
;
; Related URL: 	GET api/test/error/
;				DELETE api/test/error
;
; ****************************************************************
error
	s a=1/0
	;
	quit
	;
	;
; ****************************************************************
; errorPost
;
; Related URL: POST api/test/error
;
; ****************************************************************
errorPost
	s a=1/0
	;
	quit
	;
	;
; ****************************************************************
; getAllLocks
;
; Related URL: 	GET api/regions/locks/getAll
;
; ****************************************************************
getAllLocks
	set *HTTPRSP=$$getAll^%ydbguiLocks()
	;
	quit
	;
	;
; ****************************************************************
; clearLock
;
; Related URL: POST api/regions/locks/clear
;
; ****************************************************************
clearLock
	;RW only
	quit:$$checkAuthorization=0
	;
	new res,namespace
	;
	set *res=HTTPRSP
	;
	if $get(HTTPARGS("namespace"))="" do  goto clearLockQuit
	. set res("result")="ERROR"
	. set res("error","description")="No parameter: namespace was passed"
	;
	set *HTTPRSP=$$clear^%ydbguiLocks(HTTPARGS("namespace"))
	;
clearLockQuit
	;
	quit
	;
	;
; ****************************************************************
; terminateProcess
;
; Related URL: POST api/os/processes/{pid}/terminate
;
; ****************************************************************
terminateProcess
	;RW only
	quit:$$checkAuthorization=0
	;
	new res,pid,ret,shellResult
	;
	set *res=HTTPRSP
	;
	; Validate pid at first
	set pid=+$get(HTTPARGS("pid"),0)
	if pid=0 do  goto terminateProcessQuit
	. set res("reuslt")="ERROR"
	. set res("error","description")="Process: 0 is not a valid process id."
	;
	; Check if process exists
	if $zgetjpi(pid,"ISPROCALIVE")=0 do  goto terminateProcessQuit
	. set res("result")="ERROR"
	. set res("error","description")="Process "_pid_" doesn't exist"
	;
	set ret=$$terminateProcess^%ydbguiUtils(pid)
	if ret'=0 do  goto terminateProcessQuit
	. set res("result")="ERROR"
	. if ret=99997 set res("error","description")="The process: "_pid_" could not be terminated" quit
	. if ret=99998 set res("error","description")="The process: "_pid_" is not a YottaDB process" quit
	. if ret=99999 set res("error","description")="The process: "_pid_" is not running anymore"
	. else  set res("error","description")="Error: "_ret
	;
	set res("result")="OK"
	;
terminateProcessQuit
	quit
	;
	;
; ****************************************************************
; globalsFind
;
; Related URL: 	GET api/globals/find globalsFind^%ydbguiRest
;
; ****************************************************************
globalsFind
	set *HTTPRSP=$$find^%ydbguiGlobals($get(HTTPARGS("mask")),$get(params("size"),0))
	;
globalsFindQuit
	quit
	;
	;
; ****************************************************************
; globalsTraverse
;
; Related URL: 	POST api/globals/traverse globalsTraverse^%ydbguiRest
;
; ****************************************************************
; 					namespace:  		{
; 						global:
; 						subscripts: [
; 							{
; 								type: '*', ',', 'V', ':'
; 															* can be only at last position
; 															, is the same as <null>:<null>
; 															V for value, strings must be quoted
; 															: means we have a range
;								type1:						S or N  data type for left param if type is :
;								type2:						S or N  data type for second param if type is :
; 								value:						strings are quoted
; 								valueEnd:					strings are quoted
; 							}
; 						]
; 					}
; 					size:				How many entries are returned
; 					offset:				The offset if doing lazy fetching, empty string to start. ;
; 										If going DOWN, it is the string path of the last record
; 					direction:			'up' OR 'down' OR 'end'. If omitted, is DOWN. ;
; 					upOffset:			If going UP, the offset to the buffer
; 					dataSize:			The maximum size of the returned data (default if missing is 512 chars)
; 					mode:				'head' or 'tail'
;
;Response
;					recordsNum:
;					data: [record]
; where record is:
;					path:
;					gname:
;					subs: []
;					$data:
;
; ****************************************************************
globalsTraverse
	new res,body,res,numSubscripts,cnt
	;
	set *res=HTTPRSP
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	; validate the namepsace node
	if $data(body("namespace"))=0 do  goto globalsTraverseQuit
	. set res("result")="ERROR"
	. set res("error","description")="There is no 'namespace' node in the body"
	;
	; validate the namespace.global node
	if $get(body("namespace","global"))="" do  goto globalsTraverseQuit
	. set res("result")="ERROR"
	. set res("error","description")="There is no 'namespace.global' node in the body"
	;
	; validate the size node
	if $get(body("size"),0)=0 set body("size")=100
	;
	; validate the namespace.subscripts[] node
	if $data(body("namespace","subscripts")) do
	. set numSubscripts=$order(body("namespace","subscripts",""),-1)
	. set cnt=0 for  set cnt=$order(body("namespace","subscripts",cnt)) quit:cnt=""!$data(res)  do
	. . if $get(body("namespace","subscripts",cnt,"type"))="*",cnt'=numSubscripts do  quit
	. . . set res("result")="ERROR"
	. . . set res("error","description")="The * subscript type can only be as last subscript"
	. . ;
	. . if $get(body("namespace","subscripts",cnt,"value"))="",$get(body("namespace","subscripts",cnt,"valueEnd"))="" do  quit
	. . . set res("result")="ERROR"
	. . . set res("error","description")="A subscript must have at least one value"
	. . ;
	. . ; adjust quotes as needed later
	. . if $get(body("namespace","subscripts",cnt,"type"))=":" do
	. . . if $zextract($get(body("namespace","subscripts",cnt,"value")))="""" set body("namespace","subscripts",cnt,"value")=$zextract(body("namespace","subscripts",cnt,"value"),2,$zlength(body("namespace","subscripts",cnt,"value"))-1)
	. . . if $zextract($get(body("namespace","subscripts",cnt,"valueEnd")))="""" set body("namespace","subscripts",cnt,"valueEnd")=$zextract(body("namespace","subscripts",cnt,"valueEnd"),2,$zlength(body("namespace","subscripts",cnt,"valueEnd"))-1)
	;
	if $get(body("size"),0)=0 do  goto globalsTraverseQuit
	. set res("result")="ERROR"
	. set res("error","description")="There is no 'size' node in the body"
	;
	if $data(body("offset"))=0 set body("offset")=""
	;
	set body("dataSize")=$get(body("dataSize"),512)
	;
	if $get(body("mode"))="" set body("mode")="head"
	;
	if body("mode")'="head",body("mode")'="tail" do  goto globalsTraverseQuit
	. set res("result")="ERROR"
	. set res("error","description")="The 'mode' field can only be 'head' or 'tail'"
	;
	set body("direction")=$select($get(body("direction"))="":1,body("direction")="down":1,body("direction")="up":-1,body("direction")="end":0,1:-2)
	if body("direction")=-2 do  goto globalsTraverseQuit
	. set res("result")="ERROR"
	. set res("error","description")="The 'direction' field can only be 'up', 'down', 'end' or missing (default: down)"
	;
	set *HTTPRSP=$$traverse^%ydbguiGlobals(.body)
	;
	set HTTPRSP("result")="OK"
	;
globalsTraverseQuit
	quit
	;
; ****************************************************************
; globalData
;
; Related URL: 	GET api/globals/dollarData
;
; ****************************************************************
globalData
	new res
	;
	set *res=HTTPRSP
	;
	if $get(HTTPARGS("globalname"))="" do  goto globalDataQuit
	. set res("result")="ERROR"
	. set res("error","description")="The parameter 'globalName' is missing or empty"
	;
	set *HTTPRSP=$$globalData^%ydbguiGlobals(HTTPARGS("globalname"))
	;
globalDataQuit
	quit
	;
	;
; ****************************************************************
; getData
;
; Related URL: 	POST api/globals/dollarData
;
; ****************************************************************
getData
	new res,body
	;
	set *res=HTTPRSP
	;
	; disable badchar to avoid errors from binary data
	view "NOBADCHAR"
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	if $get(body("path"))="" do  goto getDataQuit
	. set res("result")="ERROR"
	. set res("error","description")="The parameter 'path' is missing or empty"
	;
	if $data(@body("path"))'=1&($data(@body("path"))'=11) do  goto getDataQuit
	. set res("result")="ERROR"
	. set res("error","description")="The specified node doesn't have associated data"
	;
	set res("data")=@body("path")
	;
	set res("result")="OK"
	;
getDataQuit
	quit
	;
	;
; ****************************************************************
; findRoutines
;
; Related URL: 	GET api/routines/find
;
; ****************************************************************
findRoutines
	new res
	;
	set *res=HTTPRSP
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	if $get(body("mask"))="" do  goto findRoutinesQuit
	. set res("result")="ERROR"
	. set res("error","description")="The parameter 'mask' is missing or empty"
	;
	if $zlength($ztranslate($ztranslate(body("mask"),"*",""),"?",""))<2 do  goto findRoutinesQuit
	. set res("result")="ERROR"
	. set res("error","description")="The parameter 'mask' must contain at least two character"
	;
	set body("mask")=$ztranslate(body("mask"),"_","%")
	set *HTTPRSP=$$find^%ydbguiRoutines(.body)
	;
findRoutinesQuit
	;
	quit
	;
; ****************************************************************
; getRoutine
;
; Related URL: 	GET api/routines/find
;
; ****************************************************************
getRoutine
	new res
	;
	set *res=HTTPRSP
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	if $get(body("routine"))="" do  goto getRoutineQuit
	. set res("result")="ERROR"
	. set res("error","description")="The parameter 'routine' is missing or empty"
	;
	set *HTTPRSP=$$get^%ydbguiRoutines(.body)
	;
getRoutineQuit
	quit
	;
	;
; ****************************************************************
; defrag
;
; Related URL: 	POST api/regions/maintenance/defrag
;
; ****************************************************************
defrag
	;RW only
	quit:$$checkAuthorization=0
	;
	new body,res
	;
	set *res=HTTPRSP
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	if $get(body("fillFactor"))="" do  goto defragQuit
	. set res("result")="ERROR"
	. set res("error","description")="The parameter 'fillFactor' is missing or empty"
	;
	if $get(body("indexFillFactor"))="" do  goto defragQuit
	. set res("result")="ERROR"
	. set res("error","description")="The parameter 'indexFillFactor' is missing or empty"
	;
	if $data(body("regions"))<10 do  goto defragQuit
	. set res("result")="ERROR"
	. set res("error","description")="The array 'regions' is either missing, empty or the wrong data type"
	;
	; default values
	set body("recover")=$get(body("recover"),"false")
	set body("resume")=$get(body("resume"),"false")
	;
	set *HTTPRSP=$$defrag^%ydbguiMaintenance(.body)
	;
defragQuit
	quit
	;
	;
; ****************************************************************
; integ
;
; Related URL: 	POST api/regions/maintenance/integ
;
; ****************************************************************
integ
	quit:$$checkAuthorization=0
	;
	new body,res
	;
	set *res=HTTPRSP
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	if $data(body("regions"))<10 do  goto integQuit
	. set res("result")="ERROR"
	. set res("error","description")="The array 'regions' is either missing, empty or the wrong data type"
	;
	if $get(body("type"))'="",$get(body("type"))'="complete",$get(body("type"))'="fast" do  goto integQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'type' must be either 'complete' or 'fast'"
	;
	if $get(body("reporting"))'="",$get(body("reporting"))'="summary",$get(body("reporting"))'="full" do  goto integQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'reporting' must be either 'summary' or 'full'"
	;
	if $get(body("keyRange"))'="",$get(body("keyRange"))'="true",$get(body("keyRange"))'="false" do  goto integQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'keyRange' must be either true or false"
	;
	if $get(body("stat"))'="",$get(body("stat"))'="true",$get(body("stat"))'="false" do  goto integQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'stat' must be either true or false"
	;
	if $get(body("mapErrors"))'="",+body("mapErrors")<0!(+body("mapErrors")>1000000) do  goto integQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'mapErrors' must be a numeric value between 0 and 1,000,000"
	;
	if $get(body("keySizeErrors"))'="",+body("keySizeErrors")<0!(+body("keySizeErrors")>1000000) do  goto integQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'keySizeErrors' must be a numeric value between 0 and 1,000,000"
	;
	if $get(body("transactionErrors"))'="",+body("transactionErrors")<0!(+body("transactionErrors")>1000000) do  goto integQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'transactionErrors' must be a numeric value between 0 and 1,000,000"
	;
	; default values
	set body("type")=$get(body("type"),"complete")
	set body("reporting")=$get(body("reporting"),"summary")
	;
	set body("keyRange")=$get(body("keyRange"),"true")
	set body("stat")=$get(body("stat"),"false")
	set body("mapErrors")=$get(body("mapErrors"),10)
	set body("keySizeErrors")=$get(body("keySizeErrors"),10)
	set body("transactionErrors")=$get(body("transactionErrors"),10)
	;
	set *HTTPRSP=$$integ^%ydbguiMaintenance(.body)
	;
integQuit
	quit
	;
	;
; ****************************************************************
; ls
;
; Related URL: 	POST api/os/fs/ls
;
; ****************************************************************
;
;	type: 			string (F: files D dirs)
;	path:			string
;	mask:			string
;
; ****************************************************************
ls
	new body,res
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	set body("path")=$get(body("path"),"/")
	if $zextract(body("path"),$zlength(body("path")),$zlength(body("path")))'="/" set body("path")=body("path")_"/"
	set body("type")=$get(body("type"),"FD")
	set body("mask")=$get(body("mask"),"*")
	;
	set *HTTPRSP=$$ls^%ydbguiUtils(.body)
	;
lsQuit
	quit
	;
	;
; ****************************************************************
; pathExpand
;
; Related URL: 	POST api/os/fs/pathExpand
;
; ****************************************************************
;
;	path:			string
;	dirsOnly		boolean
;
; ****************************************************************
pathExpand
	new body,res,newPath
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	set body("dirsOnly")=$get(body("dirsOnly"),"false")
	;
	set *HTTPRSP=$$pathExpand^%ydbguiUtils(.body)
	;
pathExpandQuit
	quit
	;
	;
; ****************************************************************
; backup
;
; Related URL: 	POST api/regions/maintenance/backup
;
;	regions							array of strings
;	targetPath						string
;	replace							boolean
;	disableJournaling				boolean
;	record							boolean
;	createNewJournalFiles			string (link nolink no)
;	syncIo							boolean (can be omitted if createNewJournalFiles = no, if present will be ignored)
;	includeReplicatedInstances		boolean
;	replUseSamePath					boolean
;	replTargetPath					string (mandatory if includeReplicatedInstances = true)
;
; ****************************************************************
backup
	quit:$$checkAuthorization=0
	;
	new body,res
	;
	set *res=HTTPRSP
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	if $data(body("regions"))<10 do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The array 'regions' is either missing, empty or the wrong data type"
	;
	if $get(body("targetPath"))="" do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'targetPath' is empty"
	;
	if $get(body("replace"))'="true",$get(body("replace"))'="false" do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'replace' must be either true or false"
	;
	if $get(body("disableJournaling"))'="true",$get(body("disableJournaling"))'="false" do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'disableJournaling' must be either true or false"
	;
	if $get(body("record"))'="true",$get(body("record"))'="false" do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'record' must be either true or false"
	;
	if $get(body("createNewJournalFiles"))'="link",$get(body("createNewJournalFiles"))'="nolink",$get(body("createNewJournalFiles"))'="no" do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'createNewJournalFiles' must be either link, nolink or no"
	;
	if body("createNewJournalFiles")'="no",$get(body("syncIo"))'="true",$get(body("syncIo"))'="false" do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'syncIo' must be either true or false"
	;
	if $get(body("includeReplicatedInstances"))'="true",$get(body("includeReplicatedInstances"))'="false" do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'includeReplicatedInstances' must be either true or false"
	;
	if $get(body("includeReplicatedInstances"))="true",$get(body("replUseSamePath"))="" do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'replUseSamePath' is empty"
	;
	if $get(body("includeReplicatedInstances"))="true",$get(body("replUseSamePath"))="false",$get(body("replTargetPath"))="" do  goto backupQuit
	. set res("result")="ERROR"
	. set res("error","description")="The field 'replTargetPath' is empty"
	;
	set *HTTPRSP=$$backup^%ydbguiMaintenance(.body)
	;
backupQuit
	quit
	;
	;
; ****************************************************************
; getJournalInfo
;
; Related URL: 	GET api/regions/{region}/getJournalInfo
;
; ****************************************************************
getJournalInfo
	new body,res,regionName
	;
	set *res=HTTPRSP
	;
	; extract the region name
	set regionName=$zconvert($get(HTTPARGS("region")),"u")
	;
	; check if region exists
	do enumRegions^%ydbguiGde(.regions)
	if $data(regions(regionName))=0 do  goto getJournalInfoQuit
	. set res("result")="ERR"
	. set res("error","description")="The region "_regionName_" doesn't exist"
	;
	set *HTTPRSP=$$getJournalInfo^%ydbguiRegions(regionName)
	;
getJournalInfoQuit
	quit
	;
	;
; ****************************************************************
; zroutines
; ****************************************************************
zroutines
	new res
	;
	set *res=HTTPRSP
	;
	set res("data")=$zroutines
	set res("result")="OK"
	;
	quit
	;
	;
; ****************************************************************
; executeOcto
;
; Related URL: 	POST api/octo/execute
;
;	body					JSON with command:
;
; ****************************************************************
executeOcto
	new res,sqlCommand,timeout,ix,key,sqlLower,quitRo
	;
	set *res=HTTPRSP
	;
	; process the body
	merge sqlCommand=HTTPREQ("json","query")
	set timeout=$get(HTTPREQ("json","timeout"))
	;
	if $get(sqlCommand)=""&($get(sqlCommand("\",1))="") do  goto executeOctoQuit
	. set res("result")="ERROR"
	. set res("error","description")="No query has been specified"
	;
	if timeout="" do  goto executeOctoQuit
	. set res("result")="ERROR"
	. set res("error","description")="No timeout has been specified"
	;
	; merge text if under the "\" subscript"
	if $get(sqlCommand)="" do
	. set sqlCommand=""
	. set ix=""  for  set ix=$order(sqlCommand("\",ix)) quit:ix=""  set sqlCommand=sqlCommand_sqlCommand("\",ix)
	;
	; check permissions
	if HTTPREADWRITE=0 do setError^%ydbwebutils(403,"Operation not supported in the current server mode") goto executeOctoQuit
	;
	; execute
	set *HTTPRSP=$$execute^%ydbguiOcto(sqlCommand,timeout)
	;
executeOctoQuit
		quit
		;
		;
; ****************************************************************
; getOctoTables
;
; Related URL: 	POST api/octo/get-tables
;
; ****************************************************************
getOctoObjects
	set *HTTPRSP=$$getObjects^%ydbguiOcto()
	;
	quit
	;
	;
; ****************************************************************
; getOctoTable
;
; Related URL: 	POST api/octo/tables/{table}/get-struct
;
; ****************************************************************
getOctoTable
	new res
	;
	set *res=HTTPRSP
	;
	set tableName=$get(HTTPARGS("table"))
	;
	if tableName="" do  goto getOctoTableQuit
	. set res("result")="ERROR"
	. set res("error","description")="No table has been specified"
	;
	if $find(tableName,";") do  goto getOctoTableQuit
	. set res("result")="ERROR"
	. set res("error","description")="The table name can not contain the ; character"
	;
	set *HTTPRSP=$$getTable^%ydbguiOcto(tableName)
	;
getOctoTableQuit
	quit
	;
	;
; ****************************************************************
; getOctoView
;
; Related URL: 	POST api/octo/views/{view}/get-struct
;
; ****************************************************************
getOctoView
	new res
	;
	set *res=HTTPRSP
	;
	set viewName=$get(HTTPARGS("view"))
	;
	if viewName="" do  goto getOctoTableQuit
	. set res("result")="ERROR"
	. set res("error","description")="No table has been specified"
	;
	if $find(viewName,";") do  goto getOctoTableQuit
	. set res("result")="ERROR"
	. set res("error","description")="The table name can not contain the ; character"
	;
	set *HTTPRSP=$$getView^%ydbguiOcto(viewName)
	;
getOctoViewQuit
	quit
	;
	;
; ****************************************************************
; validateGld
;
; Related URL: 	POST api/regions/gld/validate
; Body:
; path		required	the path of the gld file
; envVars	optional	an array with all the env vars to be set
; cwd		optional	the new working directory path
; ****************************************************************
validateGld
	new body,res,newPath
	;
	set *res=HTTPRSP
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	if $get(body("path"))="" do  goto validateGldQuit
	. set res("result")="ERROR"
	. set res("error","description")="No path has been specified"
	;
	if $zsearch(body("path"),-1)="" do  goto validateGldQuit
	. set res("result")="ERROR"
	. set res("error","description")="The specified gld file could not be found"
	;
	if $data(body("envVars"))=1 do  goto validateGldQuit
	. set res("result")="ERROR"
	. set res("error","description")="The env var list must be an array"
	;
	if $get(body("cwd"))="" set body("cwd")=""
	;
	set *HTTPRSP=$$validateGld^%ydbguiGde(.body)
	;
validateGldQuit
	quit
	;
	;
; ****************************************************************
; wsStart
;
; Related URL: 	POST api/ws/start
;
; ****************************************************************
wsStart
	new res
	;
	set *HTTPRSP=$$start^%ydbguiWsServer($get(HTTPREQ("query"))="logging=true")
		;
		quit
		;
		;
; ****************************************************************
; ygblGetPids
;
; Related URL: 	POST api/ygbl/get-pids
;
; ****************************************************************
ygblGetPids
	new res,regions,selRegions,ix,iy,found,notFound,region
	;
	set *res=HTTPRSP
	;
	; check if region parameter is passed
	if $find(HTTPREQ("query"),"region")=0 do  goto ygblGetPidsQuit
	. set res("result")="ERROR"
	. set res("error","description")="No region parameter have been specified"
	;
	; check if param region has a value
	if $piece(HTTPREQ("query"),"=",2)="" do  goto ygblGetPidsQuit
	. set res("result")="ERROR"
	. set res("error","description")="No region have been specified"
	;
	set *selRegions=$$SPLIT^%MPIECE($$FUNC^%UCASE($zpiece(HTTPREQ("query"),"=",2)),",")
	;
	; validate regions if needed
	set notFound=""
	if selRegions(1)'="*" do
	. do enumRegions^%ydbguiGde(.regions)
	. set ix="" for  set ix=$order(selRegions(ix)) quit:ix=""!(notFound'="")  do
	. . set found=0,region=""
	. . for  set region=$order(regions(region)) quit:region=""!(found)  do
	. . . if region=selRegions(ix) set found=1
	. . if found=0 set notFound=selRegions(ix)
	;
	if notFound'="" do  goto ygblGetPidsQuit
	. set res("result")="ERROR"
	. set res("error","description")="The region: "_notFound_" was not found on the server."
	;
	; fetch the PIDs
	set *HTTPRSP=$$ygblGetPids^%ydbguiStatsServer(.selRegions)
	;
ygblGetPidsQuit
	quit
	;
	;
; ****************************************************************
; replFullInfo
;
; Related URL: 	GET api/replication/full-info
;
; ****************************************************************
replFullInfo
	new res
	;
	set *res=$$fullInfo^%ydbguiReplication
	;
	merge HTTPRSP("data")=res
	set HTTPRSP("result")="OK"
	;
	quit
	;
	;
; ****************************************************************
; replBacklog
;
; Related URL: 	GET api/replication/backlog
;
; ****************************************************************
replBacklog
	new res
	;
	set *res=$$lBacklog^%ydbguiReplication
	;
	merge HTTPRSP("data")=res
	set HTTPRSP("result")="OK"
	;
	quit
	;
	;
; ****************************************************************
; replTopology
;
; Related URL: 	POST api/replication/topology
;
; Body:		servers[{
;				instance: "",
; 				host: ""
;				port: nnn
;				}...]
;			timeout:
; ****************************************************************
replTopology
	new res,body,ONCONNECT,ONRESPONSE
	;
	; default timeouts
	set ONCONNECT=3000
	set ONRESPONSE=5000
	;
	; process the body
	merge body=HTTPREQ("json")
	;
	; perform body validation
	if $get(body("responseType"))="" do  goto replTopologyQuit
	. set res("result")="ERROR"
	. set res("error","description")="No responseType found"
	;
	; bad responseType
	if '($zfind(body("responseType"),"F")!($zfind(body("responseType"),"H"))!($zfind(body("responseType"),"B"))!($zfind(body("responseType"),"L:"))) do  goto replTopologyQuit
	. set res("result")="ERROR"
	. set res("error","description")="responseType not valid"
	;
	; no tail supplied
	if $zfind(body("responseType"),"L:"),$get(body("logFiles","tail")),body("logFiles","tail")="" do  goto replTopologyQuit
	. set res("result")="ERROR"
	. set res("error","description")="Tail not supplied"
	;
	; force eventual positive tail to negative
	if $zfind(body("responseType"),"L:"),$get(body("logFiles","tail")),body("logFiles","tail")>0 do
	. set body("logFiles","tail")=-body("logFiles","tail")
	;
	; no servers list found
	if $data(body("servers",1))=0 do  goto replTopologyQuit
	. set res("result")="ERROR"
	. set res("error","description")="No servers array found"
	;
	; set defaults
	if +$get(body("timeouts","onConnect"))=0 set body("timeouts","onConnect")=ONCONNECT
	if +$get(body("timeouts","onResponse"))=0 set body("timeouts","onResponse")=ONRESPONSE
	;
	set *res=$$crawler^%ydbguiReplication(.body)
	;
replTopologyQuit
	merge HTTPRSP=res
	;
	quit
	;
	;
; ****************************************************************
; debugMode
;
; Related URL: 	POST api/dashboard/debug-mode
;
; ****************************************************************
debugMode
	set HTTPRSP("debugMode")=$select($zsearch(httpoptions("directory")_"js/debug/userSettings.debug.js",-1)="":"false",1:"true")
	;
	quit
; ----------------------------------------------------------------
; ----------------------------------------------------------------
; Local calls
; ----------------------------------------------------------------
; ----------------------------------------------------------------
;
;
; ****************************************************************
; appendRwStatus
; ****************************************************************
appendRwStatus
	set res("serverMode")=$select(HTTPREADWRITE:"RW",1:"RO")
	;
	quit
	;
	;
; ****************************************************************
; checkRwStatus
; ****************************************************************
checkAuthorization()
	if HTTPREADWRITE=0 do setError^%ydbwebutils(403,"Operation not supported in the current server mode") quit 0
	;
	quit 1
	;
