%weburl ;YottaDB/CJE -- URL Matching routine;2019-11-14  11:14 AM
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	quit
	;
	; This routine is used to map URLs to entry points under
	; the URLMAP entry point. ;
	;
URLMAP ;
	;;GET api/server/mode serverMode^%ydbguiUtils
	;;GET api/server/zroutines zroutines^%ydbguiRest
	;;GET api/test/error error^%ydbguiRest
	;;DELETE api/test/error error^%ydbguiRest
	;;POST api/test/error errorPost^%ydbguiRest
	;;GET api/regions/{region} getRegion^%ydbguiRest
	;;GET api/dashboard/get-all getDashboard^%ydbguiRest
	;;DELETE api/regions/{region} deleteRegion^%ydbguiRest
	;;POST api/regions/{region}/create-db createDb^%ydbguiRest
	;;POST api/regions/{region}/extend extendRegion^%ydbguiRest
	;;POST api/regions/{region}/journal-switch journalSwitch^%ydbguiRest
	;;POST api/regions/{region}/journal-file-switch journalFileSwitch^%ydbguiRest
	;;GET api/dashboard/get-templates getTemplates^%ydbguiRest
	;;POST api/regions/parse-namespace parseNamespace^%ydbguiRest
	;;POST api/regions/validate-path validatePath^%ydbguiRest
	;;POST api/regions/add addRegion^%ydbguiRest
	;;POST api/regions/{region}/edit editRegion^%ydbguiRest
	;;GET api/regions/locks/get-all getAllLocks^%ydbguiRest
	;;POST api/regions/locks/clear clearLock^%ydbguiRest
	;;POST api/os/processes/{pid}/terminate terminateProcess^%ydbguiRest
	;;GET api/globals/find globalsFind^%ydbguiRest
	;;POST api/globals/traverse globalsTraverse^%ydbguiRest
	;;GET api/globals/dollar-data globalData^%ydbguiRest
	;;POST api/globals/get-data getData^%ydbguiRest
	;;GET api/globals/by-region/{region} getGlobals^%ydbguiRest
	;;GET api/globals/size getGlobalSize^%ydbguiRest
	;;POST api/routines/find findRoutines^%ydbguiRest
	;;POST api/routines/{routine} getRoutine^%ydbguiRest
	;;POST api/regions/maintenance/defrag defrag^%ydbguiRest
	;;POST api/regions/maintenance/integ integ^%ydbguiRest
	;;POST api/os/fs/ls ls^%ydbguiRest
	;;POST api/os/fs/path-expand pathExpand^%ydbguiRest
	;;POST api/regions/maintenance/backup backup^%ydbguiRest
	;;GET api/regions/{region}/get-journal-info getJournalInfo^%ydbguiRest
	;;POST api/octo/execute executeOcto^%ydbguiRest
	;;GET api/octo/get-objects getOctoObjects^%ydbguiRest
	;;GET api/octo/tables/{table}/get-struct getOctoTable^%ydbguiRest
	;;GET api/octo/views/{view}/get-struct getOctoView^%ydbguiRest
	;;POST api/regions/gld/validate validateGld^%ydbguiRest
	;;POST api/ws/start wsStart^%ydbguiRest
	;;GET api/ygbl/get-pids ygblGetPids^%ydbguiRest
	;;POST api/replication/topology replTopology^%ydbguiRest
	;;GET api/replication/full-info replFullInfo^%ydbguiRest
	;;GET api/replication/backlog replBacklog^%ydbguiRest
	;;GET api/dashboard/debug-mode debugMode^%ydbguiRest
	;;zzzzz
